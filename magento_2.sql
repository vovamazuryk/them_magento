-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Час створення: Жов 16 2018 р., 16:34
-- Версія сервера: 5.7.18-0ubuntu0.16.04.1
-- Версія PHP: 7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `magento`
--

-- --------------------------------------------------------

--
-- Структура таблиці `review_entity`
--

CREATE TABLE `review_entity` (
  `entity_id` smallint(5) UNSIGNED NOT NULL COMMENT 'Review entity id',
  `entity_code` varchar(32) NOT NULL COMMENT 'Review entity code'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Review entities';

--
-- Дамп даних таблиці `review_entity`
--

INSERT INTO `review_entity` (`entity_id`, `entity_code`) VALUES
(1, 'product'),
(2, 'customer'),
(3, 'category');

-- --------------------------------------------------------

--
-- Структура таблиці `review_entity_summary`
--

CREATE TABLE `review_entity_summary` (
  `primary_id` bigint(20) NOT NULL COMMENT 'Summary review entity id',
  `entity_pk_value` bigint(20) NOT NULL DEFAULT '0' COMMENT 'Product id',
  `entity_type` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Entity type id',
  `reviews_count` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Qty of reviews',
  `rating_summary` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Summarized rating',
  `store_id` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Store id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Review aggregates';

--
-- Дамп даних таблиці `review_entity_summary`
--

INSERT INTO `review_entity_summary` (`primary_id`, `entity_pk_value`, `entity_type`, `reviews_count`, `rating_summary`, `store_id`) VALUES
(1, 1, 1, 2, 50, 0),
(2, 1, 1, 2, 50, 1),
(3, 6, 1, 3, 67, 0),
(4, 6, 1, 3, 67, 1),
(5, 3, 1, 3, 67, 0),
(6, 3, 1, 3, 67, 1),
(7, 2, 1, 2, 90, 0),
(8, 2, 1, 2, 90, 1),
(9, 4, 1, 3, 67, 0),
(10, 4, 1, 3, 67, 1),
(11, 5, 1, 2, 70, 0),
(12, 5, 1, 2, 70, 1),
(13, 37, 1, 3, 87, 0),
(14, 37, 1, 3, 87, 1),
(15, 40, 1, 3, 73, 0),
(16, 40, 1, 3, 73, 1),
(17, 38, 1, 3, 47, 0),
(18, 38, 1, 3, 47, 1),
(19, 36, 1, 2, 80, 0),
(20, 36, 1, 2, 80, 1),
(21, 39, 1, 4, 65, 0),
(22, 39, 1, 4, 65, 1),
(23, 275, 1, 2, 90, 0),
(24, 275, 1, 2, 90, 1),
(25, 291, 1, 2, 90, 0),
(26, 291, 1, 2, 90, 1),
(27, 419, 1, 3, 53, 0),
(28, 419, 1, 3, 53, 1),
(29, 307, 1, 3, 93, 0),
(30, 307, 1, 3, 93, 1),
(31, 403, 1, 3, 80, 0),
(32, 403, 1, 3, 80, 1),
(33, 323, 1, 2, 50, 0),
(34, 323, 1, 2, 50, 1),
(35, 339, 1, 3, 67, 0),
(36, 339, 1, 3, 67, 1),
(37, 355, 1, 2, 70, 0),
(38, 355, 1, 2, 70, 1),
(39, 371, 1, 2, 70, 0),
(40, 371, 1, 2, 70, 1),
(41, 742, 1, 3, 47, 0),
(42, 742, 1, 3, 47, 1),
(43, 755, 1, 3, 47, 0),
(44, 755, 1, 3, 47, 1),
(45, 768, 1, 2, 70, 0),
(46, 768, 1, 2, 70, 1),
(47, 781, 1, 3, 87, 0),
(48, 781, 1, 3, 87, 1),
(49, 794, 1, 3, 73, 0),
(50, 794, 1, 3, 73, 1),
(51, 563, 1, 3, 80, 0),
(52, 563, 1, 3, 80, 1),
(53, 579, 1, 2, 90, 0),
(54, 579, 1, 2, 90, 1),
(55, 531, 1, 2, 60, 0),
(56, 531, 1, 2, 60, 1),
(57, 451, 1, 3, 67, 0),
(58, 451, 1, 3, 67, 1),
(59, 467, 1, 3, 80, 0),
(60, 467, 1, 3, 80, 1),
(61, 547, 1, 2, 70, 0),
(62, 547, 1, 2, 70, 1),
(63, 611, 1, 3, 67, 0),
(64, 611, 1, 3, 67, 1),
(65, 627, 1, 4, 65, 0),
(66, 627, 1, 4, 65, 1),
(67, 483, 1, 3, 73, 0),
(68, 483, 1, 3, 73, 1),
(69, 595, 1, 3, 60, 0),
(70, 595, 1, 3, 60, 1),
(71, 499, 1, 3, 80, 0),
(72, 499, 1, 3, 80, 1),
(73, 515, 1, 3, 53, 0),
(74, 515, 1, 3, 53, 1),
(75, 898, 1, 3, 80, 0),
(76, 898, 1, 3, 80, 1),
(77, 916, 1, 3, 73, 0),
(78, 916, 1, 3, 73, 1),
(79, 929, 1, 3, 60, 0),
(80, 929, 1, 3, 60, 1),
(81, 942, 1, 3, 73, 0),
(82, 942, 1, 3, 73, 1),
(83, 955, 1, 2, 70, 0),
(84, 955, 1, 2, 70, 1),
(85, 968, 1, 3, 87, 0),
(86, 968, 1, 3, 87, 1),
(87, 981, 1, 2, 60, 0),
(88, 981, 1, 2, 60, 1),
(89, 994, 1, 3, 67, 0),
(90, 994, 1, 3, 67, 1),
(91, 643, 1, 4, 55, 0),
(92, 643, 1, 4, 55, 1),
(93, 659, 1, 3, 80, 0),
(94, 659, 1, 3, 80, 1),
(95, 675, 1, 3, 53, 0),
(96, 675, 1, 3, 53, 1),
(97, 681, 1, 4, 70, 0),
(98, 681, 1, 4, 70, 1),
(99, 7, 1, 3, 80, 0),
(100, 7, 1, 3, 80, 1),
(101, 20, 1, 3, 67, 0),
(102, 20, 1, 3, 67, 1),
(103, 18, 1, 2, 60, 0),
(104, 18, 1, 2, 60, 1),
(105, 23, 1, 3, 60, 0),
(106, 23, 1, 3, 60, 1),
(107, 17, 1, 3, 93, 0),
(108, 17, 1, 3, 93, 1),
(109, 19, 1, 3, 87, 0),
(110, 19, 1, 3, 87, 1),
(111, 15, 1, 1, 60, 0),
(112, 15, 1, 1, 60, 1),
(113, 16, 1, 2, 100, 0),
(114, 16, 1, 2, 100, 1),
(115, 8, 1, 3, 67, 0),
(116, 8, 1, 3, 67, 1),
(117, 9, 1, 2, 70, 0),
(118, 9, 1, 2, 70, 1),
(119, 12, 1, 2, 90, 0),
(120, 12, 1, 2, 90, 1),
(121, 14, 1, 3, 67, 0),
(122, 14, 1, 3, 67, 1),
(123, 10, 1, 2, 80, 0),
(124, 10, 1, 2, 80, 1),
(125, 11, 1, 3, 73, 0),
(126, 11, 1, 3, 73, 1),
(127, 13, 1, 3, 60, 0),
(128, 13, 1, 3, 60, 1),
(129, 42, 1, 3, 67, 0),
(130, 42, 1, 3, 67, 1),
(131, 44, 1, 2, 70, 0),
(132, 44, 1, 2, 70, 1),
(133, 43, 1, 3, 53, 0),
(134, 43, 1, 3, 53, 1),
(135, 41, 1, 2, 80, 0),
(136, 41, 1, 2, 80, 1),
(137, 1049, 1, 3, 87, 0),
(138, 1049, 1, 3, 87, 1),
(139, 1065, 1, 3, 67, 0),
(140, 1065, 1, 3, 67, 1),
(141, 1081, 1, 3, 60, 0),
(142, 1081, 1, 3, 60, 1),
(143, 1097, 1, 3, 60, 0),
(144, 1097, 1, 3, 60, 1),
(145, 1113, 1, 3, 80, 0),
(146, 1113, 1, 3, 80, 1),
(147, 1119, 1, 2, 80, 0),
(148, 1119, 1, 2, 80, 1),
(149, 1225, 1, 3, 67, 0),
(150, 1225, 1, 3, 67, 1),
(151, 1241, 1, 4, 70, 0),
(152, 1241, 1, 4, 70, 1),
(153, 1257, 1, 3, 87, 0),
(154, 1257, 1, 3, 87, 1),
(155, 1273, 1, 2, 90, 0),
(156, 1273, 1, 2, 90, 1),
(157, 1289, 1, 3, 87, 0),
(158, 1289, 1, 3, 87, 1),
(159, 1385, 1, 3, 87, 0),
(160, 1385, 1, 3, 87, 1),
(161, 1305, 1, 3, 67, 0),
(162, 1305, 1, 3, 67, 1),
(163, 1321, 1, 2, 60, 0),
(164, 1321, 1, 2, 60, 1),
(165, 1337, 1, 3, 87, 0),
(166, 1337, 1, 3, 87, 1),
(167, 1353, 1, 3, 60, 0),
(168, 1353, 1, 3, 60, 1),
(169, 1369, 1, 3, 87, 0),
(170, 1369, 1, 3, 87, 1),
(171, 1824, 1, 2, 80, 0),
(172, 1824, 1, 2, 80, 1),
(173, 1831, 1, 3, 93, 0),
(174, 1831, 1, 3, 93, 1),
(175, 1838, 1, 3, 67, 0),
(176, 1838, 1, 3, 67, 1),
(177, 1845, 1, 4, 80, 0),
(178, 1845, 1, 4, 80, 1),
(179, 1852, 1, 2, 60, 0),
(180, 1852, 1, 2, 60, 1),
(181, 1859, 1, 3, 67, 0),
(182, 1859, 1, 3, 67, 1),
(183, 1577, 1, 3, 87, 0),
(184, 1577, 1, 3, 87, 1),
(185, 1417, 1, 3, 73, 0),
(186, 1417, 1, 3, 73, 1),
(187, 1433, 1, 4, 60, 0),
(188, 1433, 1, 4, 60, 1),
(189, 1449, 1, 2, 60, 0),
(190, 1449, 1, 2, 60, 1),
(191, 1593, 1, 3, 73, 0),
(192, 1593, 1, 3, 73, 1),
(193, 1481, 1, 2, 60, 0),
(194, 1481, 1, 2, 60, 1),
(195, 1497, 1, 3, 47, 0),
(196, 1497, 1, 3, 47, 1),
(197, 1513, 1, 3, 73, 0),
(198, 1513, 1, 3, 73, 1),
(199, 1529, 1, 3, 80, 0),
(200, 1529, 1, 3, 80, 1),
(201, 1545, 1, 3, 87, 0),
(202, 1545, 1, 3, 87, 1),
(203, 1561, 1, 3, 60, 0),
(204, 1561, 1, 3, 60, 1),
(205, 1924, 1, 3, 60, 0),
(206, 1924, 1, 3, 60, 1),
(207, 1940, 1, 3, 80, 0),
(208, 1940, 1, 3, 80, 1),
(209, 1956, 1, 2, 90, 0),
(210, 1956, 1, 2, 90, 1),
(211, 1972, 1, 2, 80, 0),
(212, 1972, 1, 2, 80, 1),
(213, 1988, 1, 3, 73, 0),
(214, 1988, 1, 3, 73, 1),
(215, 1995, 1, 2, 70, 0),
(216, 1995, 1, 2, 70, 1),
(217, 2002, 1, 3, 67, 0),
(218, 2002, 1, 3, 67, 1),
(219, 2008, 1, 3, 93, 0),
(220, 2008, 1, 3, 93, 1),
(221, 2015, 1, 3, 67, 0),
(222, 2015, 1, 3, 67, 1),
(223, 2022, 1, 3, 80, 0),
(224, 2022, 1, 3, 80, 1),
(225, 2029, 1, 3, 73, 0),
(226, 2029, 1, 3, 73, 1),
(227, 2045, 1, 3, 60, 0),
(228, 2045, 1, 3, 60, 1),
(229, 1609, 1, 4, 75, 0),
(230, 1609, 1, 4, 75, 1),
(231, 1625, 1, 4, 60, 0),
(232, 1625, 1, 4, 60, 1),
(233, 1641, 1, 3, 67, 0),
(234, 1641, 1, 3, 67, 1),
(235, 1657, 1, 3, 60, 0),
(236, 1657, 1, 3, 60, 1),
(237, 1673, 1, 3, 40, 0),
(238, 1673, 1, 3, 40, 1),
(239, 1769, 1, 3, 73, 0),
(240, 1769, 1, 3, 73, 1),
(241, 1785, 1, 1, 80, 0),
(242, 1785, 1, 1, 80, 1),
(243, 1801, 1, 3, 60, 0),
(244, 1801, 1, 3, 60, 1),
(245, 1817, 1, 2, 70, 0),
(246, 1817, 1, 2, 70, 1),
(247, 1689, 1, 2, 80, 0),
(248, 1689, 1, 2, 80, 1),
(249, 1705, 1, 3, 53, 0),
(250, 1705, 1, 3, 53, 1);

-- --------------------------------------------------------

--
-- Структура таблиці `review_status`
--

CREATE TABLE `review_status` (
  `status_id` smallint(5) UNSIGNED NOT NULL COMMENT 'Status id',
  `status_code` varchar(32) NOT NULL COMMENT 'Status code'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Review statuses';

--
-- Дамп даних таблиці `review_status`
--

INSERT INTO `review_status` (`status_id`, `status_code`) VALUES
(1, 'Approved'),
(2, 'Pending'),
(3, 'Not Approved');

-- --------------------------------------------------------

--
-- Структура таблиці `review_store`
--

CREATE TABLE `review_store` (
  `review_id` bigint(20) UNSIGNED NOT NULL COMMENT 'Review Id',
  `store_id` smallint(5) UNSIGNED NOT NULL COMMENT 'Store Id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Review Store';

--
-- Дамп даних таблиці `review_store`
--

INSERT INTO `review_store` (`review_id`, `store_id`) VALUES
(1, 0),
(2, 0),
(3, 0),
(4, 0),
(5, 0),
(6, 0),
(7, 0),
(8, 0),
(9, 0),
(10, 0),
(11, 0),
(12, 0),
(13, 0),
(14, 0),
(15, 0),
(16, 0),
(17, 0),
(18, 0),
(19, 0),
(20, 0),
(21, 0),
(22, 0),
(23, 0),
(24, 0),
(25, 0),
(26, 0),
(27, 0),
(28, 0),
(29, 0),
(30, 0),
(31, 0),
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(37, 0),
(38, 0),
(39, 0),
(40, 0),
(41, 0),
(42, 0),
(43, 0),
(44, 0),
(45, 0),
(46, 0),
(47, 0),
(48, 0),
(49, 0),
(50, 0),
(51, 0),
(52, 0),
(53, 0),
(54, 0),
(55, 0),
(56, 0),
(57, 0),
(58, 0),
(59, 0),
(60, 0),
(61, 0),
(62, 0),
(63, 0),
(64, 0),
(65, 0),
(66, 0),
(67, 0),
(68, 0),
(69, 0),
(70, 0),
(71, 0),
(72, 0),
(73, 0),
(74, 0),
(75, 0),
(76, 0),
(77, 0),
(78, 0),
(79, 0),
(80, 0),
(81, 0),
(82, 0),
(83, 0),
(84, 0),
(85, 0),
(86, 0),
(87, 0),
(88, 0),
(89, 0),
(90, 0),
(91, 0),
(92, 0),
(93, 0),
(94, 0),
(95, 0),
(96, 0),
(97, 0),
(98, 0),
(99, 0),
(100, 0),
(101, 0),
(102, 0),
(103, 0),
(104, 0),
(105, 0),
(106, 0),
(107, 0),
(108, 0),
(109, 0),
(110, 0),
(111, 0),
(112, 0),
(113, 0),
(114, 0),
(115, 0),
(116, 0),
(117, 0),
(118, 0),
(119, 0),
(120, 0),
(121, 0),
(122, 0),
(123, 0),
(124, 0),
(125, 0),
(126, 0),
(127, 0),
(128, 0),
(129, 0),
(130, 0),
(131, 0),
(132, 0),
(133, 0),
(134, 0),
(135, 0),
(136, 0),
(137, 0),
(138, 0),
(139, 0),
(140, 0),
(141, 0),
(142, 0),
(143, 0),
(144, 0),
(145, 0),
(146, 0),
(147, 0),
(148, 0),
(149, 0),
(150, 0),
(151, 0),
(152, 0),
(153, 0),
(154, 0),
(155, 0),
(156, 0),
(157, 0),
(158, 0),
(159, 0),
(160, 0),
(161, 0),
(162, 0),
(163, 0),
(164, 0),
(165, 0),
(166, 0),
(167, 0),
(168, 0),
(169, 0),
(170, 0),
(171, 0),
(172, 0),
(173, 0),
(174, 0),
(175, 0),
(176, 0),
(177, 0),
(178, 0),
(179, 0),
(180, 0),
(181, 0),
(182, 0),
(183, 0),
(184, 0),
(185, 0),
(186, 0),
(187, 0),
(188, 0),
(189, 0),
(190, 0),
(191, 0),
(192, 0),
(193, 0),
(194, 0),
(195, 0),
(196, 0),
(197, 0),
(198, 0),
(199, 0),
(200, 0),
(201, 0),
(202, 0),
(203, 0),
(204, 0),
(205, 0),
(206, 0),
(207, 0),
(208, 0),
(209, 0),
(210, 0),
(211, 0),
(212, 0),
(213, 0),
(214, 0),
(215, 0),
(216, 0),
(217, 0),
(218, 0),
(219, 0),
(220, 0),
(221, 0),
(222, 0),
(223, 0),
(224, 0),
(225, 0),
(226, 0),
(227, 0),
(228, 0),
(229, 0),
(230, 0),
(231, 0),
(232, 0),
(233, 0),
(234, 0),
(235, 0),
(236, 0),
(237, 0),
(238, 0),
(239, 0),
(240, 0),
(241, 0),
(242, 0),
(243, 0),
(244, 0),
(245, 0),
(246, 0),
(247, 0),
(248, 0),
(249, 0),
(250, 0),
(251, 0),
(252, 0),
(253, 0),
(254, 0),
(255, 0),
(256, 0),
(257, 0),
(258, 0),
(259, 0),
(260, 0),
(261, 0),
(262, 0),
(263, 0),
(264, 0),
(265, 0),
(266, 0),
(267, 0),
(268, 0),
(269, 0),
(270, 0),
(271, 0),
(272, 0),
(273, 0),
(274, 0),
(275, 0),
(276, 0),
(277, 0),
(278, 0),
(279, 0),
(280, 0),
(281, 0),
(282, 0),
(283, 0),
(284, 0),
(285, 0),
(286, 0),
(287, 0),
(288, 0),
(289, 0),
(290, 0),
(291, 0),
(292, 0),
(293, 0),
(294, 0),
(295, 0),
(296, 0),
(297, 0),
(298, 0),
(299, 0),
(300, 0),
(301, 0),
(302, 0),
(303, 0),
(304, 0),
(305, 0),
(306, 0),
(307, 0),
(308, 0),
(309, 0),
(310, 0),
(311, 0),
(312, 0),
(313, 0),
(314, 0),
(315, 0),
(316, 0),
(317, 0),
(318, 0),
(319, 0),
(320, 0),
(321, 0),
(322, 0),
(323, 0),
(324, 0),
(325, 0),
(326, 0),
(327, 0),
(328, 0),
(329, 0),
(330, 0),
(331, 0),
(332, 0),
(333, 0),
(334, 0),
(335, 0),
(336, 0),
(337, 0),
(338, 0),
(339, 0),
(340, 0),
(341, 0),
(342, 0),
(343, 0),
(344, 0),
(345, 0),
(346, 0),
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1),
(97, 1),
(98, 1),
(99, 1),
(100, 1),
(101, 1),
(102, 1),
(103, 1),
(104, 1),
(105, 1),
(106, 1),
(107, 1),
(108, 1),
(109, 1),
(110, 1),
(111, 1),
(112, 1),
(113, 1),
(114, 1),
(115, 1),
(116, 1),
(117, 1),
(118, 1),
(119, 1),
(120, 1),
(121, 1),
(122, 1),
(123, 1),
(124, 1),
(125, 1),
(126, 1),
(127, 1),
(128, 1),
(129, 1),
(130, 1),
(131, 1),
(132, 1),
(133, 1),
(134, 1),
(135, 1),
(136, 1),
(137, 1),
(138, 1),
(139, 1),
(140, 1),
(141, 1),
(142, 1),
(143, 1),
(144, 1),
(145, 1),
(146, 1),
(147, 1),
(148, 1),
(149, 1),
(150, 1),
(151, 1),
(152, 1),
(153, 1),
(154, 1),
(155, 1),
(156, 1),
(157, 1),
(158, 1),
(159, 1),
(160, 1),
(161, 1),
(162, 1),
(163, 1),
(164, 1),
(165, 1),
(166, 1),
(167, 1),
(168, 1),
(169, 1),
(170, 1),
(171, 1),
(172, 1),
(173, 1),
(174, 1),
(175, 1),
(176, 1),
(177, 1),
(178, 1),
(179, 1),
(180, 1),
(181, 1),
(182, 1),
(183, 1),
(184, 1),
(185, 1),
(186, 1),
(187, 1),
(188, 1),
(189, 1),
(190, 1),
(191, 1),
(192, 1),
(193, 1),
(194, 1),
(195, 1),
(196, 1),
(197, 1),
(198, 1),
(199, 1),
(200, 1),
(201, 1),
(202, 1),
(203, 1),
(204, 1),
(205, 1),
(206, 1),
(207, 1),
(208, 1),
(209, 1),
(210, 1),
(211, 1),
(212, 1),
(213, 1),
(214, 1),
(215, 1),
(216, 1),
(217, 1),
(218, 1),
(219, 1),
(220, 1),
(221, 1),
(222, 1),
(223, 1),
(224, 1),
(225, 1),
(226, 1),
(227, 1),
(228, 1),
(229, 1),
(230, 1),
(231, 1),
(232, 1),
(233, 1),
(234, 1),
(235, 1),
(236, 1),
(237, 1),
(238, 1),
(239, 1),
(240, 1),
(241, 1),
(242, 1),
(243, 1),
(244, 1),
(245, 1),
(246, 1),
(247, 1),
(248, 1),
(249, 1),
(250, 1),
(251, 1),
(252, 1),
(253, 1),
(254, 1),
(255, 1),
(256, 1),
(257, 1),
(258, 1),
(259, 1),
(260, 1),
(261, 1),
(262, 1),
(263, 1),
(264, 1),
(265, 1),
(266, 1),
(267, 1),
(268, 1),
(269, 1),
(270, 1),
(271, 1),
(272, 1),
(273, 1),
(274, 1),
(275, 1),
(276, 1),
(277, 1),
(278, 1),
(279, 1),
(280, 1),
(281, 1),
(282, 1),
(283, 1),
(284, 1),
(285, 1),
(286, 1),
(287, 1),
(288, 1),
(289, 1),
(290, 1),
(291, 1),
(292, 1),
(293, 1),
(294, 1),
(295, 1),
(296, 1),
(297, 1),
(298, 1),
(299, 1),
(300, 1),
(301, 1),
(302, 1),
(303, 1),
(304, 1),
(305, 1),
(306, 1),
(307, 1),
(308, 1),
(309, 1),
(310, 1),
(311, 1),
(312, 1),
(313, 1),
(314, 1),
(315, 1),
(316, 1),
(317, 1),
(318, 1),
(319, 1),
(320, 1),
(321, 1),
(322, 1),
(323, 1),
(324, 1),
(325, 1),
(326, 1),
(327, 1),
(328, 1),
(329, 1),
(330, 1),
(331, 1),
(332, 1),
(333, 1),
(334, 1),
(335, 1),
(336, 1),
(337, 1),
(338, 1),
(339, 1),
(340, 1),
(341, 1),
(342, 1),
(343, 1),
(344, 1),
(345, 1),
(346, 1);

-- --------------------------------------------------------

--
-- Структура таблиці `salesrule`
--

CREATE TABLE `salesrule` (
  `rule_id` int(10) UNSIGNED NOT NULL COMMENT 'Rule Id',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `description` text COMMENT 'Description',
  `from_date` date DEFAULT NULL COMMENT 'From',
  `to_date` date DEFAULT NULL COMMENT 'To',
  `uses_per_customer` int(11) NOT NULL DEFAULT '0' COMMENT 'Uses Per Customer',
  `is_active` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Is Active',
  `conditions_serialized` mediumtext COMMENT 'Conditions Serialized',
  `actions_serialized` mediumtext COMMENT 'Actions Serialized',
  `stop_rules_processing` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Stop Rules Processing',
  `is_advanced` smallint(5) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Is Advanced',
  `product_ids` text COMMENT 'Product Ids',
  `sort_order` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `simple_action` varchar(32) DEFAULT NULL COMMENT 'Simple Action',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount',
  `discount_qty` decimal(12,4) DEFAULT NULL COMMENT 'Discount Qty',
  `discount_step` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Discount Step',
  `apply_to_shipping` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Apply To Shipping',
  `times_used` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Times Used',
  `is_rss` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Is Rss',
  `coupon_type` smallint(5) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Coupon Type',
  `use_auto_generation` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Use Auto Generation',
  `uses_per_coupon` int(11) NOT NULL DEFAULT '0' COMMENT 'User Per Coupon',
  `simple_free_shipping` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Simple Free Shipping'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule';

--
-- Дамп даних таблиці `salesrule`
--

INSERT INTO `salesrule` (`rule_id`, `name`, `description`, `from_date`, `to_date`, `uses_per_customer`, `is_active`, `conditions_serialized`, `actions_serialized`, `stop_rules_processing`, `is_advanced`, `product_ids`, `sort_order`, `simple_action`, `discount_amount`, `discount_qty`, `discount_step`, `apply_to_shipping`, `times_used`, `is_rss`, `coupon_type`, `use_auto_generation`, `uses_per_coupon`, `simple_free_shipping`) VALUES
(1, 'Buy 3 tee shirts and get the 4th free', 'Buy 3 tee shirts and get the 4th free', NULL, NULL, 0, 1, '{"type":"Magento\\\\SalesRule\\\\Model\\\\Rule\\\\Condition\\\\Combine","attribute":null,"operator":null,"value":"1","is_value_processed":null,"aggregator":"all","conditions":[{"type":"Magento\\\\SalesRule\\\\Model\\\\Rule\\\\Condition\\\\Product\\\\Found","attribute":null,"operator":null,"value":"1","is_value_processed":null,"aggregator":"all","conditions":[{"type":"Magento\\\\SalesRule\\\\Model\\\\Rule\\\\Condition\\\\Product","attribute":"category_ids","operator":"()","value":"25,16","is_value_processed":false}]}]}', '{"type":"Magento\\\\SalesRule\\\\Model\\\\Rule\\\\Condition\\\\Product\\\\Combine","attribute":null,"operator":null,"value":"1","is_value_processed":null,"aggregator":"all"}', 0, 1, NULL, 0, 'buy_x_get_y', '1.0000', NULL, 3, 0, 2, 1, 1, 0, 0, 0),
(2, 'Spend $50 or more - shipping is free!', 'Free shipping on any purchase over $50', NULL, NULL, 0, 1, '{"type":"Magento\\\\SalesRule\\\\Model\\\\Rule\\\\Condition\\\\Combine","attribute":null,"operator":null,"value":"1","is_value_processed":null,"aggregator":"all","conditions":[{"type":"Magento\\\\SalesRule\\\\Model\\\\Rule\\\\Condition\\\\Address","attribute":"base_subtotal","operator":">=","value":"50","is_value_processed":false}]}', '{"type":"Magento\\\\SalesRule\\\\Model\\\\Rule\\\\Condition\\\\Product\\\\Combine","attribute":null,"operator":null,"value":"1","is_value_processed":null,"aggregator":"all"}', 0, 1, NULL, 0, 'by_percent', '0.0000', NULL, 0, 0, 0, 1, 1, 0, 0, 2),
(3, '20% OFF Ever $200-plus purchase!*', 'Shopping cart price rule for the cart over $200. Does not applied on other sales items', NULL, NULL, 0, 1, '{"type":"Magento\\\\SalesRule\\\\Model\\\\Rule\\\\Condition\\\\Combine","attribute":null,"operator":null,"value":"1","is_value_processed":null,"aggregator":"all","conditions":[{"type":"Magento\\\\SalesRule\\\\Model\\\\Rule\\\\Condition\\\\Address","attribute":"base_subtotal","operator":">=","value":"200","is_value_processed":false}]}', '{"type":"Magento\\\\SalesRule\\\\Model\\\\Rule\\\\Condition\\\\Product\\\\Combine","attribute":null,"operator":null,"value":"1","is_value_processed":null,"aggregator":"all","conditions":[{"type":"Magento\\\\SalesRule\\\\Model\\\\Rule\\\\Condition\\\\Product\\\\Combine","attribute":null,"operator":null,"value":"0","is_value_processed":null,"aggregator":"all","conditions":[{"type":"Magento\\\\SalesRule\\\\Model\\\\Rule\\\\Condition\\\\Product","attribute":"category_ids","operator":"==","value":"6","is_value_processed":false},{"type":"Magento\\\\SalesRule\\\\Model\\\\Rule\\\\Condition\\\\Product","attribute":"sale","operator":"==","value":"1","is_value_processed":false}]}]}', 0, 1, NULL, 0, 'by_percent', '20.0000', NULL, 0, 0, 0, 1, 1, 0, 0, 0),
(4, '$4 Luma water bottle (save 70%)', 'Use promo code H20 at checkout', NULL, NULL, 1, 1, '{"type":"Magento\\\\SalesRule\\\\Model\\\\Rule\\\\Condition\\\\Combine","attribute":null,"operator":null,"value":"1","is_value_processed":null,"aggregator":"all","conditions":[{"type":"Magento\\\\SalesRule\\\\Model\\\\Rule\\\\Condition\\\\Product\\\\Found","attribute":null,"operator":null,"value":"1","is_value_processed":null,"aggregator":"all"}]}', '{"type":"Magento\\\\SalesRule\\\\Model\\\\Rule\\\\Condition\\\\Product\\\\Combine","attribute":null,"operator":null,"value":"1","is_value_processed":null,"aggregator":"all","conditions":[{"type":"Magento\\\\SalesRule\\\\Model\\\\Rule\\\\Condition\\\\Product","attribute":"sku","operator":"==","value":"24-UG06","is_value_processed":false}]}', 0, 1, NULL, 0, 'by_percent', '70.0000', NULL, 0, 0, 0, 1, 2, 0, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблиці `salesrule_coupon`
--

CREATE TABLE `salesrule_coupon` (
  `coupon_id` int(10) UNSIGNED NOT NULL COMMENT 'Coupon Id',
  `rule_id` int(10) UNSIGNED NOT NULL COMMENT 'Rule Id',
  `code` varchar(255) DEFAULT NULL COMMENT 'Code',
  `usage_limit` int(10) UNSIGNED DEFAULT NULL COMMENT 'Usage Limit',
  `usage_per_customer` int(10) UNSIGNED DEFAULT NULL COMMENT 'Usage Per Customer',
  `times_used` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Times Used',
  `expiration_date` timestamp NULL DEFAULT NULL COMMENT 'Expiration Date',
  `is_primary` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Is Primary',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Coupon Code Creation Date',
  `type` smallint(6) DEFAULT '0' COMMENT 'Coupon Code Type',
  `generated_by_dotmailer` smallint(6) DEFAULT NULL COMMENT '1 = Generated by dotmailer'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Coupon';

--
-- Дамп даних таблиці `salesrule_coupon`
--

INSERT INTO `salesrule_coupon` (`coupon_id`, `rule_id`, `code`, `usage_limit`, `usage_per_customer`, `times_used`, `expiration_date`, `is_primary`, `created_at`, `type`, `generated_by_dotmailer`) VALUES
(1, 4, 'H20', NULL, 1, 0, NULL, 1, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `salesrule_coupon_aggregated`
--

CREATE TABLE `salesrule_coupon_aggregated` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Id',
  `period` date NOT NULL COMMENT 'Period',
  `store_id` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `coupon_code` varchar(50) DEFAULT NULL COMMENT 'Coupon Code',
  `coupon_uses` int(11) NOT NULL DEFAULT '0' COMMENT 'Coupon Uses',
  `subtotal_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal Amount',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount',
  `total_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Amount',
  `subtotal_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal Amount Actual',
  `discount_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount Actual',
  `total_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Amount Actual',
  `rule_name` varchar(255) DEFAULT NULL COMMENT 'Rule Name'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Coupon Aggregated';

-- --------------------------------------------------------

--
-- Структура таблиці `salesrule_coupon_aggregated_order`
--

CREATE TABLE `salesrule_coupon_aggregated_order` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Id',
  `period` date NOT NULL COMMENT 'Period',
  `store_id` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `coupon_code` varchar(50) DEFAULT NULL COMMENT 'Coupon Code',
  `coupon_uses` int(11) NOT NULL DEFAULT '0' COMMENT 'Coupon Uses',
  `subtotal_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal Amount',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount',
  `total_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Amount',
  `rule_name` varchar(255) DEFAULT NULL COMMENT 'Rule Name'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Coupon Aggregated Order';

-- --------------------------------------------------------

--
-- Структура таблиці `salesrule_coupon_aggregated_updated`
--

CREATE TABLE `salesrule_coupon_aggregated_updated` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Id',
  `period` date NOT NULL COMMENT 'Period',
  `store_id` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `coupon_code` varchar(50) DEFAULT NULL COMMENT 'Coupon Code',
  `coupon_uses` int(11) NOT NULL DEFAULT '0' COMMENT 'Coupon Uses',
  `subtotal_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal Amount',
  `discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount',
  `total_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Amount',
  `subtotal_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Subtotal Amount Actual',
  `discount_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Discount Amount Actual',
  `total_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Amount Actual',
  `rule_name` varchar(255) DEFAULT NULL COMMENT 'Rule Name'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Coupon Aggregated Updated';

-- --------------------------------------------------------

--
-- Структура таблиці `salesrule_coupon_usage`
--

CREATE TABLE `salesrule_coupon_usage` (
  `coupon_id` int(10) UNSIGNED NOT NULL COMMENT 'Coupon Id',
  `customer_id` int(10) UNSIGNED NOT NULL COMMENT 'Customer Id',
  `times_used` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Times Used'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Coupon Usage';

-- --------------------------------------------------------

--
-- Структура таблиці `salesrule_customer`
--

CREATE TABLE `salesrule_customer` (
  `rule_customer_id` int(10) UNSIGNED NOT NULL COMMENT 'Rule Customer Id',
  `rule_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Rule Id',
  `customer_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Customer Id',
  `times_used` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Times Used'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Customer';

--
-- Дамп даних таблиці `salesrule_customer`
--

INSERT INTO `salesrule_customer` (`rule_customer_id`, `rule_id`, `customer_id`, `times_used`) VALUES
(1, 1, 1, 2);

-- --------------------------------------------------------

--
-- Структура таблиці `salesrule_customer_group`
--

CREATE TABLE `salesrule_customer_group` (
  `rule_id` int(10) UNSIGNED NOT NULL COMMENT 'Rule Id',
  `customer_group_id` int(10) UNSIGNED NOT NULL COMMENT 'Customer Group Id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Rules To Customer Groups Relations';

--
-- Дамп даних таблиці `salesrule_customer_group`
--

INSERT INTO `salesrule_customer_group` (`rule_id`, `customer_group_id`) VALUES
(1, 0),
(2, 0),
(3, 0),
(4, 0),
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(1, 2),
(2, 2),
(3, 2),
(4, 2),
(1, 3),
(2, 3),
(3, 3),
(4, 3);

-- --------------------------------------------------------

--
-- Структура таблиці `salesrule_label`
--

CREATE TABLE `salesrule_label` (
  `label_id` int(10) UNSIGNED NOT NULL COMMENT 'Label Id',
  `rule_id` int(10) UNSIGNED NOT NULL COMMENT 'Rule Id',
  `store_id` smallint(5) UNSIGNED NOT NULL COMMENT 'Store Id',
  `label` varchar(255) DEFAULT NULL COMMENT 'Label'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Label';

-- --------------------------------------------------------

--
-- Структура таблиці `salesrule_product_attribute`
--

CREATE TABLE `salesrule_product_attribute` (
  `rule_id` int(10) UNSIGNED NOT NULL COMMENT 'Rule Id',
  `website_id` smallint(5) UNSIGNED NOT NULL COMMENT 'Website Id',
  `customer_group_id` int(10) UNSIGNED NOT NULL COMMENT 'Customer Group Id',
  `attribute_id` smallint(5) UNSIGNED NOT NULL COMMENT 'Attribute Id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Salesrule Product Attribute';

--
-- Дамп даних таблиці `salesrule_product_attribute`
--

INSERT INTO `salesrule_product_attribute` (`rule_id`, `website_id`, `customer_group_id`, `attribute_id`) VALUES
(1, 1, 0, 105),
(1, 1, 1, 105),
(1, 1, 2, 105),
(1, 1, 3, 105),
(3, 1, 0, 105),
(3, 1, 0, 146),
(3, 1, 1, 105),
(3, 1, 1, 146),
(3, 1, 2, 105),
(3, 1, 2, 146),
(3, 1, 3, 105),
(3, 1, 3, 146),
(4, 1, 0, 74),
(4, 1, 1, 74),
(4, 1, 2, 74),
(4, 1, 3, 74);

-- --------------------------------------------------------

--
-- Структура таблиці `salesrule_website`
--

CREATE TABLE `salesrule_website` (
  `rule_id` int(10) UNSIGNED NOT NULL COMMENT 'Rule Id',
  `website_id` smallint(5) UNSIGNED NOT NULL COMMENT 'Website Id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Rules To Websites Relations';

--
-- Дамп даних таблиці `salesrule_website`
--

INSERT INTO `salesrule_website` (`rule_id`, `website_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1);

-- --------------------------------------------------------

--
-- Структура таблиці `sales_bestsellers_aggregated_daily`
--

CREATE TABLE `sales_bestsellers_aggregated_daily` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Store Id',
  `product_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Product Id',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product Name',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Product Price',
  `qty_ordered` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty Ordered',
  `rating_pos` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Rating Pos'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Bestsellers Aggregated Daily';

-- --------------------------------------------------------

--
-- Структура таблиці `sales_bestsellers_aggregated_monthly`
--

CREATE TABLE `sales_bestsellers_aggregated_monthly` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Store Id',
  `product_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Product Id',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product Name',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Product Price',
  `qty_ordered` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty Ordered',
  `rating_pos` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Rating Pos'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Bestsellers Aggregated Monthly';

-- --------------------------------------------------------

--
-- Структура таблиці `sales_bestsellers_aggregated_yearly`
--

CREATE TABLE `sales_bestsellers_aggregated_yearly` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Store Id',
  `product_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Product Id',
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Product Name',
  `product_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Product Price',
  `qty_ordered` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Qty Ordered',
  `rating_pos` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Rating Pos'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Bestsellers Aggregated Yearly';

-- --------------------------------------------------------

--
-- Структура таблиці `sales_creditmemo`
--

CREATE TABLE `sales_creditmemo` (
  `entity_id` int(10) UNSIGNED NOT NULL COMMENT 'Entity Id',
  `store_id` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Store Id',
  `adjustment_positive` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Positive',
  `base_shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Tax Amount',
  `store_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Order Rate',
  `base_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Amount',
  `base_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Order Rate',
  `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
  `base_adjustment_negative` decimal(12,4) DEFAULT NULL COMMENT 'Base Adjustment Negative',
  `base_subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Incl Tax',
  `shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Amount',
  `subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Incl Tax',
  `adjustment_negative` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Negative',
  `base_shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Amount',
  `store_to_base_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Base Rate',
  `base_to_global_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Global Rate',
  `base_adjustment` decimal(12,4) DEFAULT NULL COMMENT 'Base Adjustment',
  `base_subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal',
  `discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Amount',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `adjustment` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `base_adjustment_positive` decimal(12,4) DEFAULT NULL COMMENT 'Base Adjustment Positive',
  `base_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Amount',
  `shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Tax Amount',
  `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
  `order_id` int(10) UNSIGNED NOT NULL COMMENT 'Order Id',
  `email_sent` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Email Sent',
  `send_email` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Send Email',
  `creditmemo_status` int(11) DEFAULT NULL COMMENT 'Creditmemo Status',
  `state` int(11) DEFAULT NULL COMMENT 'State',
  `shipping_address_id` int(11) DEFAULT NULL COMMENT 'Shipping Address Id',
  `billing_address_id` int(11) DEFAULT NULL COMMENT 'Billing Address Id',
  `invoice_id` int(11) DEFAULT NULL COMMENT 'Invoice Id',
  `store_currency_code` varchar(3) DEFAULT NULL COMMENT 'Store Currency Code',
  `order_currency_code` varchar(3) DEFAULT NULL COMMENT 'Order Currency Code',
  `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
  `global_currency_code` varchar(3) DEFAULT NULL COMMENT 'Global Currency Code',
  `transaction_id` varchar(255) DEFAULT NULL COMMENT 'Transaction Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `shipping_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Discount Tax Compensation Amount',
  `base_shipping_discount_tax_compensation_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Discount Tax Compensation Amount',
  `shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Incl Tax',
  `base_shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Incl Tax',
  `discount_description` varchar(255) DEFAULT NULL COMMENT 'Discount Description',
  `customer_note` text COMMENT 'Customer Note',
  `customer_note_notify` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Customer Note Notify'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Creditmemo';

--
-- Дамп даних таблиці `sales_creditmemo`
--

INSERT INTO `sales_creditmemo` (`entity_id`, `store_id`, `adjustment_positive`, `base_shipping_tax_amount`, `store_to_order_rate`, `base_discount_amount`, `base_to_order_rate`, `grand_total`, `base_adjustment_negative`, `base_subtotal_incl_tax`, `shipping_amount`, `subtotal_incl_tax`, `adjustment_negative`, `base_shipping_amount`, `store_to_base_rate`, `base_to_global_rate`, `base_adjustment`, `base_subtotal`, `discount_amount`, `subtotal`, `adjustment`, `base_grand_total`, `base_adjustment_positive`, `base_tax_amount`, `shipping_tax_amount`, `tax_amount`, `order_id`, `email_sent`, `send_email`, `creditmemo_status`, `state`, `shipping_address_id`, `billing_address_id`, `invoice_id`, `store_currency_code`, `order_currency_code`, `base_currency_code`, `global_currency_code`, `transaction_id`, `increment_id`, `created_at`, `updated_at`, `discount_tax_compensation_amount`, `base_discount_tax_compensation_amount`, `shipping_discount_tax_compensation_amount`, `base_shipping_discount_tax_compensation_amnt`, `shipping_incl_tax`, `base_shipping_incl_tax`, `discount_description`, `customer_note`, `customer_note_notify`) VALUES
(1, 1, NULL, '0.0000', '0.0000', '0.0000', '1.0000', '39.6400', NULL, '34.6400', '5.0000', '34.6400', NULL, '5.0000', '0.0000', '1.0000', '0.0000', '32.0000', '0.0000', '32.0000', '0.0000', '39.6400', NULL, '2.6400', '0.0000', '2.6400', 2, NULL, NULL, NULL, 2, 3, 4, NULL, 'USD', 'USD', 'USD', 'USD', NULL, '000000001', '2018-08-14 05:53:44', '2018-08-14 05:53:44', '0.0000', '0.0000', NULL, NULL, '5.0000', '5.0000', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `sales_creditmemo_comment`
--

CREATE TABLE `sales_creditmemo_comment` (
  `entity_id` int(10) UNSIGNED NOT NULL COMMENT 'Entity Id',
  `parent_id` int(10) UNSIGNED NOT NULL COMMENT 'Parent Id',
  `is_customer_notified` int(11) DEFAULT NULL COMMENT 'Is Customer Notified',
  `is_visible_on_front` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Is Visible On Front',
  `comment` text COMMENT 'Comment',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Creditmemo Comment';

-- --------------------------------------------------------

--
-- Структура таблиці `sales_creditmemo_grid`
--

CREATE TABLE `sales_creditmemo_grid` (
  `entity_id` int(10) UNSIGNED NOT NULL COMMENT 'Entity Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  `order_id` int(10) UNSIGNED NOT NULL COMMENT 'Order Id',
  `order_increment_id` varchar(50) DEFAULT NULL COMMENT 'Order Increment Id',
  `order_created_at` timestamp NULL DEFAULT NULL COMMENT 'Order Created At',
  `billing_name` varchar(255) DEFAULT NULL COMMENT 'Billing Name',
  `state` int(11) DEFAULT NULL COMMENT 'Status',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `order_status` varchar(32) DEFAULT NULL COMMENT 'Order Status',
  `store_id` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Store Id',
  `billing_address` varchar(255) DEFAULT NULL COMMENT 'Billing Address',
  `shipping_address` varchar(255) DEFAULT NULL COMMENT 'Shipping Address',
  `customer_name` varchar(128) NOT NULL COMMENT 'Customer Name',
  `customer_email` varchar(128) DEFAULT NULL COMMENT 'Customer Email',
  `customer_group_id` smallint(6) DEFAULT NULL COMMENT 'Customer Group Id',
  `payment_method` varchar(32) DEFAULT NULL COMMENT 'Payment Method',
  `shipping_information` varchar(255) DEFAULT NULL COMMENT 'Shipping Method Name',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `shipping_and_handling` decimal(12,4) DEFAULT NULL COMMENT 'Shipping and handling amount',
  `adjustment_positive` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Positive',
  `adjustment_negative` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Negative',
  `order_base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Order Grand Total'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Creditmemo Grid';

--
-- Дамп даних таблиці `sales_creditmemo_grid`
--

INSERT INTO `sales_creditmemo_grid` (`entity_id`, `increment_id`, `created_at`, `updated_at`, `order_id`, `order_increment_id`, `order_created_at`, `billing_name`, `state`, `base_grand_total`, `order_status`, `store_id`, `billing_address`, `shipping_address`, `customer_name`, `customer_email`, `customer_group_id`, `payment_method`, `shipping_information`, `subtotal`, `shipping_and_handling`, `adjustment_positive`, `adjustment_negative`, `order_base_grand_total`) VALUES
(1, '000000001', '2018-08-14 05:53:44', '2018-08-14 05:53:44', 2, '000000002', '2018-08-14 05:53:41', 'Veronica Costello', 2, '39.6400', 'closed', 1, '6146 Honey Bluff Parkway Calder Michigan 49628-7978', '6146 Honey Bluff Parkway Calder Michigan 49628-7978', 'Veronica Costello', 'roni_cost@example.com', 1, 'checkmo', 'Flat Rate - Fixed', '32.0000', '5.0000', NULL, NULL, '39.6400');

-- --------------------------------------------------------

--
-- Структура таблиці `sales_creditmemo_item`
--

CREATE TABLE `sales_creditmemo_item` (
  `entity_id` int(10) UNSIGNED NOT NULL COMMENT 'Entity Id',
  `parent_id` int(10) UNSIGNED NOT NULL COMMENT 'Parent Id',
  `base_price` decimal(12,4) DEFAULT NULL COMMENT 'Base Price',
  `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
  `base_row_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total',
  `discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Amount',
  `row_total` decimal(12,4) DEFAULT NULL COMMENT 'Row Total',
  `base_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Amount',
  `price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Price Incl Tax',
  `base_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Amount',
  `base_price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Price Incl Tax',
  `qty` decimal(12,4) DEFAULT NULL COMMENT 'Qty',
  `base_cost` decimal(12,4) DEFAULT NULL COMMENT 'Base Cost',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total Incl Tax',
  `row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Row Total Incl Tax',
  `product_id` int(11) DEFAULT NULL COMMENT 'Product Id',
  `order_item_id` int(11) DEFAULT NULL COMMENT 'Order Item Id',
  `additional_data` text COMMENT 'Additional Data',
  `description` text COMMENT 'Description',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `tax_ratio` text COMMENT 'Ratio of tax in the creditmemo item over tax of the order item',
  `weee_tax_applied` text COMMENT 'Weee Tax Applied',
  `weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Amount',
  `weee_tax_applied_row_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Row Amount',
  `weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Disposition',
  `weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Row Disposition',
  `base_weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Amount',
  `base_weee_tax_applied_row_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Row Amnt',
  `base_weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Disposition',
  `base_weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Row Disposition'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Creditmemo Item';

--
-- Дамп даних таблиці `sales_creditmemo_item`
--

INSERT INTO `sales_creditmemo_item` (`entity_id`, `parent_id`, `base_price`, `tax_amount`, `base_row_total`, `discount_amount`, `row_total`, `base_discount_amount`, `price_incl_tax`, `base_tax_amount`, `base_price_incl_tax`, `qty`, `base_cost`, `price`, `base_row_total_incl_tax`, `row_total_incl_tax`, `product_id`, `order_item_id`, `additional_data`, `description`, `sku`, `name`, `discount_tax_compensation_amount`, `base_discount_tax_compensation_amount`, `tax_ratio`, `weee_tax_applied`, `weee_tax_applied_amount`, `weee_tax_applied_row_amount`, `weee_tax_disposition`, `weee_tax_row_disposition`, `base_weee_tax_applied_amount`, `base_weee_tax_applied_row_amnt`, `base_weee_tax_disposition`, `base_weee_tax_row_disposition`) VALUES
(1, 1, '32.0000', '2.6400', '32.0000', NULL, '32.0000', NULL, '34.6400', '2.6400', '34.6400', '1.0000', NULL, '32.0000', '34.6400', '34.6400', 1497, 2, NULL, NULL, 'WS08-XS-Blue', 'Minerva LumaTech&trade; V-Tee', '0.0000', '0.0000', NULL, '[]', NULL, '0.0000', NULL, '0.0000', NULL, NULL, NULL, '0.0000');

-- --------------------------------------------------------

--
-- Структура таблиці `sales_invoice`
--

CREATE TABLE `sales_invoice` (
  `entity_id` int(10) UNSIGNED NOT NULL COMMENT 'Entity Id',
  `store_id` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Store Id',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Tax Amount',
  `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
  `base_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Amount',
  `store_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Order Rate',
  `base_shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Tax Amount',
  `base_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Amount',
  `base_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Order Rate',
  `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
  `shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Amount',
  `subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Incl Tax',
  `base_subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Incl Tax',
  `store_to_base_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Base Rate',
  `base_shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Amount',
  `total_qty` decimal(12,4) DEFAULT NULL COMMENT 'Total Qty',
  `base_to_global_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Global Rate',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `base_subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal',
  `discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Amount',
  `billing_address_id` int(11) DEFAULT NULL COMMENT 'Billing Address Id',
  `is_used_for_refund` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Is Used For Refund',
  `order_id` int(10) UNSIGNED NOT NULL COMMENT 'Order Id',
  `email_sent` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Email Sent',
  `send_email` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Send Email',
  `can_void_flag` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Can Void Flag',
  `state` int(11) DEFAULT NULL COMMENT 'State',
  `shipping_address_id` int(11) DEFAULT NULL COMMENT 'Shipping Address Id',
  `store_currency_code` varchar(3) DEFAULT NULL COMMENT 'Store Currency Code',
  `transaction_id` varchar(255) DEFAULT NULL COMMENT 'Transaction Id',
  `order_currency_code` varchar(3) DEFAULT NULL COMMENT 'Order Currency Code',
  `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
  `global_currency_code` varchar(3) DEFAULT NULL COMMENT 'Global Currency Code',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `shipping_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Discount Tax Compensation Amount',
  `base_shipping_discount_tax_compensation_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Discount Tax Compensation Amount',
  `shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Incl Tax',
  `base_shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Incl Tax',
  `base_total_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Refunded',
  `discount_description` varchar(255) DEFAULT NULL COMMENT 'Discount Description',
  `customer_note` text COMMENT 'Customer Note',
  `customer_note_notify` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Customer Note Notify'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Invoice';

--
-- Дамп даних таблиці `sales_invoice`
--

INSERT INTO `sales_invoice` (`entity_id`, `store_id`, `base_grand_total`, `shipping_tax_amount`, `tax_amount`, `base_tax_amount`, `store_to_order_rate`, `base_shipping_tax_amount`, `base_discount_amount`, `base_to_order_rate`, `grand_total`, `shipping_amount`, `subtotal_incl_tax`, `base_subtotal_incl_tax`, `store_to_base_rate`, `base_shipping_amount`, `total_qty`, `base_to_global_rate`, `subtotal`, `base_subtotal`, `discount_amount`, `billing_address_id`, `is_used_for_refund`, `order_id`, `email_sent`, `send_email`, `can_void_flag`, `state`, `shipping_address_id`, `store_currency_code`, `transaction_id`, `order_currency_code`, `base_currency_code`, `global_currency_code`, `increment_id`, `created_at`, `updated_at`, `discount_tax_compensation_amount`, `base_discount_tax_compensation_amount`, `shipping_discount_tax_compensation_amount`, `base_shipping_discount_tax_compensation_amnt`, `shipping_incl_tax`, `base_shipping_incl_tax`, `base_total_refunded`, `discount_description`, `customer_note`, `customer_note_notify`) VALUES
(1, 1, '36.3900', '0.0000', '2.3900', '2.3900', '0.0000', '0.0000', '0.0000', '1.0000', '36.3900', '5.0000', '31.3900', '31.3900', '0.0000', '5.0000', '1.0000', '1.0000', '29.0000', '29.0000', '0.0000', 2, NULL, 1, NULL, NULL, 0, 2, 1, 'USD', NULL, 'USD', 'USD', 'USD', '000000001', '2018-08-14 05:53:40', '2018-08-14 05:53:40', '0.0000', '0.0000', '0.0000', NULL, '5.0000', '5.0000', NULL, NULL, NULL, NULL),
(2, 1, '39.6400', '0.0000', '2.6400', '2.6400', '0.0000', '0.0000', '0.0000', '1.0000', '39.6400', '5.0000', '34.6400', '34.6400', '0.0000', '5.0000', '1.0000', '1.0000', '32.0000', '32.0000', '0.0000', 4, NULL, 2, NULL, NULL, 0, 2, 3, 'USD', NULL, 'USD', 'USD', 'USD', '000000002', '2018-08-14 05:53:43', '2018-08-14 05:53:43', '0.0000', '0.0000', '0.0000', NULL, '5.0000', '5.0000', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `sales_invoiced_aggregated`
--

CREATE TABLE `sales_invoiced_aggregated` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `orders_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Orders Invoiced',
  `invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced',
  `invoiced_captured` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced Captured',
  `invoiced_not_captured` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced Not Captured'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Invoiced Aggregated';

-- --------------------------------------------------------

--
-- Структура таблиці `sales_invoiced_aggregated_order`
--

CREATE TABLE `sales_invoiced_aggregated_order` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) NOT NULL COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `orders_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Orders Invoiced',
  `invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced',
  `invoiced_captured` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced Captured',
  `invoiced_not_captured` decimal(12,4) DEFAULT NULL COMMENT 'Invoiced Not Captured'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Invoiced Aggregated Order';

-- --------------------------------------------------------

--
-- Структура таблиці `sales_invoice_comment`
--

CREATE TABLE `sales_invoice_comment` (
  `entity_id` int(10) UNSIGNED NOT NULL COMMENT 'Entity Id',
  `parent_id` int(10) UNSIGNED NOT NULL COMMENT 'Parent Id',
  `is_customer_notified` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Is Customer Notified',
  `is_visible_on_front` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Is Visible On Front',
  `comment` text COMMENT 'Comment',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Invoice Comment';

-- --------------------------------------------------------

--
-- Структура таблиці `sales_invoice_grid`
--

CREATE TABLE `sales_invoice_grid` (
  `entity_id` int(10) UNSIGNED NOT NULL COMMENT 'Entity Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `state` int(11) DEFAULT NULL COMMENT 'State',
  `store_id` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Store Id',
  `store_name` varchar(255) DEFAULT NULL COMMENT 'Store Name',
  `order_id` int(10) UNSIGNED NOT NULL COMMENT 'Order Id',
  `order_increment_id` varchar(50) DEFAULT NULL COMMENT 'Order Increment Id',
  `order_created_at` timestamp NULL DEFAULT NULL COMMENT 'Order Created At',
  `customer_name` varchar(255) DEFAULT NULL COMMENT 'Customer Name',
  `customer_email` varchar(255) DEFAULT NULL COMMENT 'Customer Email',
  `customer_group_id` int(11) DEFAULT NULL,
  `payment_method` varchar(128) DEFAULT NULL COMMENT 'Payment Method',
  `store_currency_code` varchar(3) DEFAULT NULL COMMENT 'Store Currency Code',
  `order_currency_code` varchar(3) DEFAULT NULL COMMENT 'Order Currency Code',
  `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
  `global_currency_code` varchar(3) DEFAULT NULL COMMENT 'Global Currency Code',
  `billing_name` varchar(255) DEFAULT NULL COMMENT 'Billing Name',
  `billing_address` varchar(255) DEFAULT NULL COMMENT 'Billing Address',
  `shipping_address` varchar(255) DEFAULT NULL COMMENT 'Shipping Address',
  `shipping_information` varchar(255) DEFAULT NULL COMMENT 'Shipping Method Name',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `shipping_and_handling` decimal(12,4) DEFAULT NULL COMMENT 'Shipping and handling amount',
  `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Invoice Grid';

--
-- Дамп даних таблиці `sales_invoice_grid`
--

INSERT INTO `sales_invoice_grid` (`entity_id`, `increment_id`, `state`, `store_id`, `store_name`, `order_id`, `order_increment_id`, `order_created_at`, `customer_name`, `customer_email`, `customer_group_id`, `payment_method`, `store_currency_code`, `order_currency_code`, `base_currency_code`, `global_currency_code`, `billing_name`, `billing_address`, `shipping_address`, `shipping_information`, `subtotal`, `shipping_and_handling`, `grand_total`, `base_grand_total`, `created_at`, `updated_at`) VALUES
(1, '000000001', 2, 1, 'Main Website\nMain Website Store\n', 1, '000000001', '2018-08-14 05:53:38', 'Veronica Costello', 'roni_cost@example.com', 1, 'checkmo', 'USD', 'USD', 'USD', 'USD', 'Veronica Costello', '6146 Honey Bluff Parkway Calder Michigan 49628-7978', '6146 Honey Bluff Parkway Calder Michigan 49628-7978', 'Flat Rate - Fixed', '29.0000', '5.0000', '36.3900', '36.3900', '2018-08-14 05:53:40', '2018-08-14 05:53:40'),
(2, '000000002', 2, 1, 'Main Website\nMain Website Store\n', 2, '000000002', '2018-08-14 05:53:41', 'Veronica Costello', 'roni_cost@example.com', 1, 'checkmo', 'USD', 'USD', 'USD', 'USD', 'Veronica Costello', '6146 Honey Bluff Parkway Calder Michigan 49628-7978', '6146 Honey Bluff Parkway Calder Michigan 49628-7978', 'Flat Rate - Fixed', '32.0000', '5.0000', '39.6400', '39.6400', '2018-08-14 05:53:43', '2018-08-14 05:53:43');

-- --------------------------------------------------------

--
-- Структура таблиці `sales_invoice_item`
--

CREATE TABLE `sales_invoice_item` (
  `entity_id` int(10) UNSIGNED NOT NULL COMMENT 'Entity Id',
  `parent_id` int(10) UNSIGNED NOT NULL COMMENT 'Parent Id',
  `base_price` decimal(12,4) DEFAULT NULL COMMENT 'Base Price',
  `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
  `base_row_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total',
  `discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Amount',
  `row_total` decimal(12,4) DEFAULT NULL COMMENT 'Row Total',
  `base_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Amount',
  `price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Price Incl Tax',
  `base_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Amount',
  `base_price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Price Incl Tax',
  `qty` decimal(12,4) DEFAULT NULL COMMENT 'Qty',
  `base_cost` decimal(12,4) DEFAULT NULL COMMENT 'Base Cost',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total Incl Tax',
  `row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Row Total Incl Tax',
  `product_id` int(11) DEFAULT NULL COMMENT 'Product Id',
  `order_item_id` int(11) DEFAULT NULL COMMENT 'Order Item Id',
  `additional_data` text COMMENT 'Additional Data',
  `description` text COMMENT 'Description',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `tax_ratio` text COMMENT 'Ratio of tax invoiced over tax of the order item',
  `weee_tax_applied` text COMMENT 'Weee Tax Applied',
  `weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Amount',
  `weee_tax_applied_row_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Row Amount',
  `weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Disposition',
  `weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Row Disposition',
  `base_weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Amount',
  `base_weee_tax_applied_row_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Row Amnt',
  `base_weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Disposition',
  `base_weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Row Disposition'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Invoice Item';

--
-- Дамп даних таблиці `sales_invoice_item`
--

INSERT INTO `sales_invoice_item` (`entity_id`, `parent_id`, `base_price`, `tax_amount`, `base_row_total`, `discount_amount`, `row_total`, `base_discount_amount`, `price_incl_tax`, `base_tax_amount`, `base_price_incl_tax`, `qty`, `base_cost`, `price`, `base_row_total_incl_tax`, `row_total_incl_tax`, `product_id`, `order_item_id`, `additional_data`, `description`, `sku`, `name`, `discount_tax_compensation_amount`, `base_discount_tax_compensation_amount`, `tax_ratio`, `weee_tax_applied`, `weee_tax_applied_amount`, `weee_tax_applied_row_amount`, `weee_tax_disposition`, `weee_tax_row_disposition`, `base_weee_tax_applied_amount`, `base_weee_tax_applied_row_amnt`, `base_weee_tax_disposition`, `base_weee_tax_row_disposition`) VALUES
(1, 1, '29.0000', '2.3900', '29.0000', NULL, '29.0000', NULL, '31.3900', '2.3900', '31.3900', '1.0000', NULL, '29.0000', '31.3900', '31.3900', 1433, 1, NULL, NULL, 'WS03-XS-Red', 'Iris Workout Top', '0.0000', '0.0000', NULL, '[]', NULL, '0.0000', NULL, '0.0000', NULL, NULL, NULL, '0.0000'),
(2, 2, '32.0000', '2.6400', '32.0000', NULL, '32.0000', NULL, '34.6400', '2.6400', '34.6400', '1.0000', NULL, '32.0000', '34.6400', '34.6400', 1497, 2, NULL, NULL, 'WS08-XS-Blue', 'Minerva LumaTech&trade; V-Tee', '0.0000', '0.0000', NULL, '[]', NULL, '0.0000', NULL, '0.0000', NULL, NULL, NULL, '0.0000');

-- --------------------------------------------------------

--
-- Структура таблиці `sales_order`
--

CREATE TABLE `sales_order` (
  `entity_id` int(10) UNSIGNED NOT NULL COMMENT 'Entity Id',
  `state` varchar(32) DEFAULT NULL COMMENT 'State',
  `status` varchar(32) DEFAULT NULL COMMENT 'Status',
  `coupon_code` varchar(255) DEFAULT NULL COMMENT 'Coupon Code',
  `protect_code` varchar(255) DEFAULT NULL COMMENT 'Protect Code',
  `shipping_description` varchar(255) DEFAULT NULL COMMENT 'Shipping Description',
  `is_virtual` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Is Virtual',
  `store_id` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Store Id',
  `customer_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Customer Id',
  `base_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Amount',
  `base_discount_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Canceled',
  `base_discount_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Invoiced',
  `base_discount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Refunded',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `base_shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Amount',
  `base_shipping_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Canceled',
  `base_shipping_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Invoiced',
  `base_shipping_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Refunded',
  `base_shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Tax Amount',
  `base_shipping_tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Tax Refunded',
  `base_subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal',
  `base_subtotal_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Canceled',
  `base_subtotal_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Invoiced',
  `base_subtotal_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Refunded',
  `base_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Amount',
  `base_tax_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Canceled',
  `base_tax_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Invoiced',
  `base_tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Refunded',
  `base_to_global_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Global Rate',
  `base_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Order Rate',
  `base_total_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Canceled',
  `base_total_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Invoiced',
  `base_total_invoiced_cost` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Invoiced Cost',
  `base_total_offline_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Offline Refunded',
  `base_total_online_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Online Refunded',
  `base_total_paid` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Paid',
  `base_total_qty_ordered` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Qty Ordered',
  `base_total_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Refunded',
  `discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Amount',
  `discount_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Discount Canceled',
  `discount_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Discount Invoiced',
  `discount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Discount Refunded',
  `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
  `shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Amount',
  `shipping_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Canceled',
  `shipping_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Invoiced',
  `shipping_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Refunded',
  `shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Tax Amount',
  `shipping_tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Tax Refunded',
  `store_to_base_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Base Rate',
  `store_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Order Rate',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `subtotal_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Canceled',
  `subtotal_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Invoiced',
  `subtotal_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Refunded',
  `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
  `tax_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Tax Canceled',
  `tax_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Tax Invoiced',
  `tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Tax Refunded',
  `total_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Total Canceled',
  `total_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Total Invoiced',
  `total_offline_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Total Offline Refunded',
  `total_online_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Total Online Refunded',
  `total_paid` decimal(12,4) DEFAULT NULL COMMENT 'Total Paid',
  `total_qty_ordered` decimal(12,4) DEFAULT NULL COMMENT 'Total Qty Ordered',
  `total_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Total Refunded',
  `can_ship_partially` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Can Ship Partially',
  `can_ship_partially_item` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Can Ship Partially Item',
  `customer_is_guest` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Customer Is Guest',
  `customer_note_notify` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Customer Note Notify',
  `billing_address_id` int(11) DEFAULT NULL COMMENT 'Billing Address Id',
  `customer_group_id` int(11) DEFAULT NULL,
  `edit_increment` int(11) DEFAULT NULL COMMENT 'Edit Increment',
  `email_sent` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Email Sent',
  `send_email` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Send Email',
  `forced_shipment_with_invoice` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Forced Do Shipment With Invoice',
  `payment_auth_expiration` int(11) DEFAULT NULL COMMENT 'Payment Authorization Expiration',
  `quote_address_id` int(11) DEFAULT NULL COMMENT 'Quote Address Id',
  `quote_id` int(11) DEFAULT NULL COMMENT 'Quote Id',
  `shipping_address_id` int(11) DEFAULT NULL COMMENT 'Shipping Address Id',
  `adjustment_negative` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Negative',
  `adjustment_positive` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Positive',
  `base_adjustment_negative` decimal(12,4) DEFAULT NULL COMMENT 'Base Adjustment Negative',
  `base_adjustment_positive` decimal(12,4) DEFAULT NULL COMMENT 'Base Adjustment Positive',
  `base_shipping_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Discount Amount',
  `base_subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Incl Tax',
  `base_total_due` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Due',
  `payment_authorization_amount` decimal(12,4) DEFAULT NULL COMMENT 'Payment Authorization Amount',
  `shipping_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Discount Amount',
  `subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Incl Tax',
  `total_due` decimal(12,4) DEFAULT NULL COMMENT 'Total Due',
  `weight` decimal(12,4) DEFAULT NULL COMMENT 'Weight',
  `customer_dob` datetime DEFAULT NULL COMMENT 'Customer Dob',
  `increment_id` varchar(32) DEFAULT NULL COMMENT 'Increment Id',
  `applied_rule_ids` varchar(128) DEFAULT NULL COMMENT 'Applied Rule Ids',
  `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
  `customer_email` varchar(128) DEFAULT NULL COMMENT 'Customer Email',
  `customer_firstname` varchar(128) DEFAULT NULL COMMENT 'Customer Firstname',
  `customer_lastname` varchar(128) DEFAULT NULL COMMENT 'Customer Lastname',
  `customer_middlename` varchar(128) DEFAULT NULL COMMENT 'Customer Middlename',
  `customer_prefix` varchar(32) DEFAULT NULL COMMENT 'Customer Prefix',
  `customer_suffix` varchar(32) DEFAULT NULL COMMENT 'Customer Suffix',
  `customer_taxvat` varchar(32) DEFAULT NULL COMMENT 'Customer Taxvat',
  `discount_description` varchar(255) DEFAULT NULL COMMENT 'Discount Description',
  `ext_customer_id` varchar(32) DEFAULT NULL COMMENT 'Ext Customer Id',
  `ext_order_id` varchar(32) DEFAULT NULL COMMENT 'Ext Order Id',
  `global_currency_code` varchar(3) DEFAULT NULL COMMENT 'Global Currency Code',
  `hold_before_state` varchar(32) DEFAULT NULL COMMENT 'Hold Before State',
  `hold_before_status` varchar(32) DEFAULT NULL COMMENT 'Hold Before Status',
  `order_currency_code` varchar(3) DEFAULT NULL COMMENT 'Order Currency Code',
  `original_increment_id` varchar(32) DEFAULT NULL COMMENT 'Original Increment Id',
  `relation_child_id` varchar(32) DEFAULT NULL COMMENT 'Relation Child Id',
  `relation_child_real_id` varchar(32) DEFAULT NULL COMMENT 'Relation Child Real Id',
  `relation_parent_id` varchar(32) DEFAULT NULL COMMENT 'Relation Parent Id',
  `relation_parent_real_id` varchar(32) DEFAULT NULL COMMENT 'Relation Parent Real Id',
  `remote_ip` varchar(32) DEFAULT NULL COMMENT 'Remote Ip',
  `shipping_method` varchar(120) DEFAULT NULL,
  `store_currency_code` varchar(3) DEFAULT NULL COMMENT 'Store Currency Code',
  `store_name` varchar(32) DEFAULT NULL COMMENT 'Store Name',
  `x_forwarded_for` varchar(32) DEFAULT NULL COMMENT 'X Forwarded For',
  `customer_note` text COMMENT 'Customer Note',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `total_item_count` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Total Item Count',
  `customer_gender` int(11) DEFAULT NULL COMMENT 'Customer Gender',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `shipping_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Discount Tax Compensation Amount',
  `base_shipping_discount_tax_compensation_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Discount Tax Compensation Amount',
  `discount_tax_compensation_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Invoiced',
  `base_discount_tax_compensation_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Invoiced',
  `discount_tax_compensation_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Refunded',
  `base_discount_tax_compensation_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Refunded',
  `shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Incl Tax',
  `base_shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Incl Tax',
  `coupon_rule_name` varchar(255) DEFAULT NULL COMMENT 'Coupon Sales Rule Name',
  `paypal_ipn_customer_notified` int(11) DEFAULT '0' COMMENT 'Paypal Ipn Customer Notified',
  `gift_message_id` int(11) DEFAULT NULL COMMENT 'Gift Message Id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order';

--
-- Дамп даних таблиці `sales_order`
--

INSERT INTO `sales_order` (`entity_id`, `state`, `status`, `coupon_code`, `protect_code`, `shipping_description`, `is_virtual`, `store_id`, `customer_id`, `base_discount_amount`, `base_discount_canceled`, `base_discount_invoiced`, `base_discount_refunded`, `base_grand_total`, `base_shipping_amount`, `base_shipping_canceled`, `base_shipping_invoiced`, `base_shipping_refunded`, `base_shipping_tax_amount`, `base_shipping_tax_refunded`, `base_subtotal`, `base_subtotal_canceled`, `base_subtotal_invoiced`, `base_subtotal_refunded`, `base_tax_amount`, `base_tax_canceled`, `base_tax_invoiced`, `base_tax_refunded`, `base_to_global_rate`, `base_to_order_rate`, `base_total_canceled`, `base_total_invoiced`, `base_total_invoiced_cost`, `base_total_offline_refunded`, `base_total_online_refunded`, `base_total_paid`, `base_total_qty_ordered`, `base_total_refunded`, `discount_amount`, `discount_canceled`, `discount_invoiced`, `discount_refunded`, `grand_total`, `shipping_amount`, `shipping_canceled`, `shipping_invoiced`, `shipping_refunded`, `shipping_tax_amount`, `shipping_tax_refunded`, `store_to_base_rate`, `store_to_order_rate`, `subtotal`, `subtotal_canceled`, `subtotal_invoiced`, `subtotal_refunded`, `tax_amount`, `tax_canceled`, `tax_invoiced`, `tax_refunded`, `total_canceled`, `total_invoiced`, `total_offline_refunded`, `total_online_refunded`, `total_paid`, `total_qty_ordered`, `total_refunded`, `can_ship_partially`, `can_ship_partially_item`, `customer_is_guest`, `customer_note_notify`, `billing_address_id`, `customer_group_id`, `edit_increment`, `email_sent`, `send_email`, `forced_shipment_with_invoice`, `payment_auth_expiration`, `quote_address_id`, `quote_id`, `shipping_address_id`, `adjustment_negative`, `adjustment_positive`, `base_adjustment_negative`, `base_adjustment_positive`, `base_shipping_discount_amount`, `base_subtotal_incl_tax`, `base_total_due`, `payment_authorization_amount`, `shipping_discount_amount`, `subtotal_incl_tax`, `total_due`, `weight`, `customer_dob`, `increment_id`, `applied_rule_ids`, `base_currency_code`, `customer_email`, `customer_firstname`, `customer_lastname`, `customer_middlename`, `customer_prefix`, `customer_suffix`, `customer_taxvat`, `discount_description`, `ext_customer_id`, `ext_order_id`, `global_currency_code`, `hold_before_state`, `hold_before_status`, `order_currency_code`, `original_increment_id`, `relation_child_id`, `relation_child_real_id`, `relation_parent_id`, `relation_parent_real_id`, `remote_ip`, `shipping_method`, `store_currency_code`, `store_name`, `x_forwarded_for`, `customer_note`, `created_at`, `updated_at`, `total_item_count`, `customer_gender`, `discount_tax_compensation_amount`, `base_discount_tax_compensation_amount`, `shipping_discount_tax_compensation_amount`, `base_shipping_discount_tax_compensation_amnt`, `discount_tax_compensation_invoiced`, `base_discount_tax_compensation_invoiced`, `discount_tax_compensation_refunded`, `base_discount_tax_compensation_refunded`, `shipping_incl_tax`, `base_shipping_incl_tax`, `coupon_rule_name`, `paypal_ipn_customer_notified`, `gift_message_id`) VALUES
(1, 'processing', 'processing', NULL, '20181330fdac80d1ca9d66c99a59c911', 'Flat Rate - Fixed', 0, 1, 1, '0.0000', NULL, '0.0000', NULL, '36.3900', '5.0000', NULL, '5.0000', NULL, '0.0000', NULL, '29.0000', NULL, '29.0000', NULL, '2.3900', NULL, '2.3900', NULL, '1.0000', '1.0000', NULL, '36.3900', '0.0000', NULL, NULL, '36.3900', NULL, NULL, '0.0000', NULL, '0.0000', NULL, '36.3900', '5.0000', NULL, '5.0000', NULL, '0.0000', NULL, '0.0000', '0.0000', '29.0000', NULL, '29.0000', NULL, '2.3900', NULL, '2.3900', NULL, NULL, '36.3900', NULL, NULL, '36.3900', '1.0000', NULL, NULL, NULL, 0, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '0.0000', '31.3900', '36.3900', NULL, '0.0000', '31.3900', '36.3900', '1.0000', '1973-12-15 00:00:00', '000000001', '1', 'USD', 'roni_cost@example.com', 'Veronica', 'Costello', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'USD', NULL, NULL, 'USD', NULL, NULL, NULL, NULL, NULL, NULL, 'flatrate_flatrate', 'USD', 'Main Website\nMain Website Store\n', NULL, NULL, '2018-08-14 05:53:38', '2018-08-14 05:53:40', 1, 2, '0.0000', '0.0000', '0.0000', NULL, '0.0000', '0.0000', NULL, NULL, '5.0000', '5.0000', NULL, 0, NULL),
(2, 'closed', 'closed', NULL, '095d93a896c89fb4b518a58945242195', 'Flat Rate - Fixed', 0, 1, 1, '0.0000', NULL, '0.0000', '0.0000', '39.6400', '5.0000', NULL, '5.0000', '5.0000', '0.0000', '0.0000', '32.0000', NULL, '32.0000', '32.0000', '2.6400', NULL, '2.6400', '2.6400', '1.0000', '1.0000', NULL, '39.6400', '0.0000', '39.6400', NULL, '39.6400', NULL, '39.6400', '0.0000', NULL, '0.0000', '0.0000', '39.6400', '5.0000', NULL, '5.0000', '5.0000', '0.0000', '0.0000', '0.0000', '0.0000', '32.0000', NULL, '32.0000', '32.0000', '2.6400', NULL, '2.6400', '2.6400', NULL, '39.6400', '39.6400', NULL, '39.6400', '1.0000', '39.6400', NULL, NULL, 0, 1, 4, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, 3, '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '34.6400', '0.0000', NULL, '0.0000', '34.6400', '0.0000', '1.0000', '1973-12-15 00:00:00', '000000002', '1', 'USD', 'roni_cost@example.com', 'Veronica', 'Costello', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'USD', NULL, NULL, 'USD', NULL, NULL, NULL, NULL, NULL, NULL, 'flatrate_flatrate', 'USD', 'Main Website\nMain Website Store\n', NULL, NULL, '2018-08-14 05:53:41', '2018-08-14 05:53:44', 1, 2, '0.0000', '0.0000', '0.0000', NULL, '0.0000', '0.0000', '0.0000', '0.0000', '5.0000', '5.0000', NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `sales_order_address`
--

CREATE TABLE `sales_order_address` (
  `entity_id` int(10) UNSIGNED NOT NULL COMMENT 'Entity Id',
  `parent_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Parent Id',
  `customer_address_id` int(11) DEFAULT NULL COMMENT 'Customer Address Id',
  `quote_address_id` int(11) DEFAULT NULL COMMENT 'Quote Address Id',
  `region_id` int(11) DEFAULT NULL COMMENT 'Region Id',
  `customer_id` int(11) DEFAULT NULL COMMENT 'Customer Id',
  `fax` varchar(255) DEFAULT NULL COMMENT 'Fax',
  `region` varchar(255) DEFAULT NULL COMMENT 'Region',
  `postcode` varchar(255) DEFAULT NULL COMMENT 'Postcode',
  `lastname` varchar(255) DEFAULT NULL COMMENT 'Lastname',
  `street` varchar(255) DEFAULT NULL COMMENT 'Street',
  `city` varchar(255) DEFAULT NULL COMMENT 'City',
  `email` varchar(255) DEFAULT NULL COMMENT 'Email',
  `telephone` varchar(255) DEFAULT NULL COMMENT 'Phone Number',
  `country_id` varchar(2) DEFAULT NULL COMMENT 'Country Id',
  `firstname` varchar(255) DEFAULT NULL COMMENT 'Firstname',
  `address_type` varchar(255) DEFAULT NULL COMMENT 'Address Type',
  `prefix` varchar(255) DEFAULT NULL COMMENT 'Prefix',
  `middlename` varchar(255) DEFAULT NULL COMMENT 'Middlename',
  `suffix` varchar(255) DEFAULT NULL COMMENT 'Suffix',
  `company` varchar(255) DEFAULT NULL COMMENT 'Company',
  `vat_id` text COMMENT 'Vat Id',
  `vat_is_valid` smallint(6) DEFAULT NULL COMMENT 'Vat Is Valid',
  `vat_request_id` text COMMENT 'Vat Request Id',
  `vat_request_date` text COMMENT 'Vat Request Date',
  `vat_request_success` smallint(6) DEFAULT NULL COMMENT 'Vat Request Success'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order Address';

--
-- Дамп даних таблиці `sales_order_address`
--

INSERT INTO `sales_order_address` (`entity_id`, `parent_id`, `customer_address_id`, `quote_address_id`, `region_id`, `customer_id`, `fax`, `region`, `postcode`, `lastname`, `street`, `city`, `email`, `telephone`, `country_id`, `firstname`, `address_type`, `prefix`, `middlename`, `suffix`, `company`, `vat_id`, `vat_is_valid`, `vat_request_id`, `vat_request_date`, `vat_request_success`) VALUES
(1, 1, 1, 2, 33, NULL, NULL, 'Michigan', '49628-7978', 'Costello', '6146 Honey Bluff Parkway', 'Calder', 'roni_cost@example.com', '(555) 229-3326', 'US', 'Veronica', 'shipping', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 1, 1, 1, 33, NULL, NULL, 'Michigan', '49628-7978', 'Costello', '6146 Honey Bluff Parkway', 'Calder', 'roni_cost@example.com', '(555) 229-3326', 'US', 'Veronica', 'billing', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 2, 1, 4, 33, NULL, NULL, 'Michigan', '49628-7978', 'Costello', '6146 Honey Bluff Parkway', 'Calder', 'roni_cost@example.com', '(555) 229-3326', 'US', 'Veronica', 'shipping', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 2, 1, 3, 33, NULL, NULL, 'Michigan', '49628-7978', 'Costello', '6146 Honey Bluff Parkway', 'Calder', 'roni_cost@example.com', '(555) 229-3326', 'US', 'Veronica', 'billing', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `sales_order_aggregated_created`
--

CREATE TABLE `sales_order_aggregated_created` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) NOT NULL COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `total_qty_ordered` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Qty Ordered',
  `total_qty_invoiced` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Qty Invoiced',
  `total_income_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Income Amount',
  `total_revenue_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Revenue Amount',
  `total_profit_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Profit Amount',
  `total_invoiced_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Invoiced Amount',
  `total_canceled_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Canceled Amount',
  `total_paid_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Paid Amount',
  `total_refunded_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Refunded Amount',
  `total_tax_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Tax Amount',
  `total_tax_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Tax Amount Actual',
  `total_shipping_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Shipping Amount',
  `total_shipping_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Shipping Amount Actual',
  `total_discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Discount Amount',
  `total_discount_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Discount Amount Actual'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Aggregated Created';

-- --------------------------------------------------------

--
-- Структура таблиці `sales_order_aggregated_updated`
--

CREATE TABLE `sales_order_aggregated_updated` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) NOT NULL COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `total_qty_ordered` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Qty Ordered',
  `total_qty_invoiced` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Qty Invoiced',
  `total_income_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Income Amount',
  `total_revenue_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Revenue Amount',
  `total_profit_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Profit Amount',
  `total_invoiced_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Invoiced Amount',
  `total_canceled_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Canceled Amount',
  `total_paid_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Paid Amount',
  `total_refunded_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Refunded Amount',
  `total_tax_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Tax Amount',
  `total_tax_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Tax Amount Actual',
  `total_shipping_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Shipping Amount',
  `total_shipping_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Shipping Amount Actual',
  `total_discount_amount` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Discount Amount',
  `total_discount_amount_actual` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Total Discount Amount Actual'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Aggregated Updated';

-- --------------------------------------------------------

--
-- Структура таблиці `sales_order_grid`
--

CREATE TABLE `sales_order_grid` (
  `entity_id` int(10) UNSIGNED NOT NULL COMMENT 'Entity Id',
  `status` varchar(32) DEFAULT NULL COMMENT 'Status',
  `store_id` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Store Id',
  `store_name` varchar(255) DEFAULT NULL COMMENT 'Store Name',
  `customer_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Customer Id',
  `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
  `base_total_paid` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Paid',
  `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
  `total_paid` decimal(12,4) DEFAULT NULL COMMENT 'Total Paid',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
  `order_currency_code` varchar(255) DEFAULT NULL COMMENT 'Order Currency Code',
  `shipping_name` varchar(255) DEFAULT NULL COMMENT 'Shipping Name',
  `billing_name` varchar(255) DEFAULT NULL COMMENT 'Billing Name',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
  `billing_address` varchar(255) DEFAULT NULL COMMENT 'Billing Address',
  `shipping_address` varchar(255) DEFAULT NULL COMMENT 'Shipping Address',
  `shipping_information` varchar(255) DEFAULT NULL COMMENT 'Shipping Method Name',
  `customer_email` varchar(255) DEFAULT NULL COMMENT 'Customer Email',
  `customer_group` varchar(255) DEFAULT NULL COMMENT 'Customer Group',
  `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
  `shipping_and_handling` decimal(12,4) DEFAULT NULL COMMENT 'Shipping and handling amount',
  `customer_name` varchar(255) DEFAULT NULL COMMENT 'Customer Name',
  `payment_method` varchar(255) DEFAULT NULL COMMENT 'Payment Method',
  `total_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Total Refunded',
  `signifyd_guarantee_status` varchar(32) DEFAULT NULL COMMENT 'Signifyd Guarantee Disposition Status'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order Grid';

--
-- Дамп даних таблиці `sales_order_grid`
--

INSERT INTO `sales_order_grid` (`entity_id`, `status`, `store_id`, `store_name`, `customer_id`, `base_grand_total`, `base_total_paid`, `grand_total`, `total_paid`, `increment_id`, `base_currency_code`, `order_currency_code`, `shipping_name`, `billing_name`, `created_at`, `updated_at`, `billing_address`, `shipping_address`, `shipping_information`, `customer_email`, `customer_group`, `subtotal`, `shipping_and_handling`, `customer_name`, `payment_method`, `total_refunded`, `signifyd_guarantee_status`) VALUES
(1, 'processing', 1, 'Main Website\nMain Website Store\n', 1, '36.3900', '36.3900', '36.3900', '36.3900', '000000001', 'USD', 'USD', 'Veronica Costello', 'Veronica Costello', '2018-08-14 05:53:38', '2018-08-14 05:53:40', '6146 Honey Bluff Parkway Calder Michigan 49628-7978', '6146 Honey Bluff Parkway Calder Michigan 49628-7978', 'Flat Rate - Fixed', 'roni_cost@example.com', '1', '29.0000', '5.0000', 'Veronica Costello', 'checkmo', NULL, NULL),
(2, 'closed', 1, 'Main Website\nMain Website Store\n', 1, '39.6400', '39.6400', '39.6400', '39.6400', '000000002', 'USD', 'USD', 'Veronica Costello', 'Veronica Costello', '2018-08-14 05:53:41', '2018-08-14 05:53:44', '6146 Honey Bluff Parkway Calder Michigan 49628-7978', '6146 Honey Bluff Parkway Calder Michigan 49628-7978', 'Flat Rate - Fixed', 'roni_cost@example.com', '1', '32.0000', '5.0000', 'Veronica Costello', 'checkmo', '39.6400', NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `sales_order_item`
--

CREATE TABLE `sales_order_item` (
  `item_id` int(10) UNSIGNED NOT NULL COMMENT 'Item Id',
  `order_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Order Id',
  `parent_item_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Parent Item Id',
  `quote_item_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Quote Item Id',
  `store_id` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Store Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `product_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Product Id',
  `product_type` varchar(255) DEFAULT NULL COMMENT 'Product Type',
  `product_options` text COMMENT 'Product Options',
  `weight` decimal(12,4) DEFAULT '0.0000' COMMENT 'Weight',
  `is_virtual` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Is Virtual',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `description` text COMMENT 'Description',
  `applied_rule_ids` text COMMENT 'Applied Rule Ids',
  `additional_data` text COMMENT 'Additional Data',
  `is_qty_decimal` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Is Qty Decimal',
  `no_discount` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'No Discount',
  `qty_backordered` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Backordered',
  `qty_canceled` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Canceled',
  `qty_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Invoiced',
  `qty_ordered` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Ordered',
  `qty_refunded` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Refunded',
  `qty_shipped` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Shipped',
  `base_cost` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Cost',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  `base_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Price',
  `original_price` decimal(12,4) DEFAULT NULL COMMENT 'Original Price',
  `base_original_price` decimal(12,4) DEFAULT NULL COMMENT 'Base Original Price',
  `tax_percent` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Percent',
  `tax_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Amount',
  `base_tax_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Tax Amount',
  `tax_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Invoiced',
  `base_tax_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Tax Invoiced',
  `discount_percent` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Percent',
  `discount_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Amount',
  `base_discount_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Discount Amount',
  `discount_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Invoiced',
  `base_discount_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Discount Invoiced',
  `amount_refunded` decimal(12,4) DEFAULT '0.0000' COMMENT 'Amount Refunded',
  `base_amount_refunded` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Amount Refunded',
  `row_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Row Total',
  `base_row_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Row Total',
  `row_invoiced` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Row Invoiced',
  `base_row_invoiced` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Row Invoiced',
  `row_weight` decimal(12,4) DEFAULT '0.0000' COMMENT 'Row Weight',
  `base_tax_before_discount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Before Discount',
  `tax_before_discount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Before Discount',
  `ext_order_item_id` varchar(255) DEFAULT NULL COMMENT 'Ext Order Item Id',
  `locked_do_invoice` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Locked Do Invoice',
  `locked_do_ship` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Locked Do Ship',
  `price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Price Incl Tax',
  `base_price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Price Incl Tax',
  `row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Row Total Incl Tax',
  `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total Incl Tax',
  `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
  `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
  `discount_tax_compensation_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Invoiced',
  `base_discount_tax_compensation_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Invoiced',
  `discount_tax_compensation_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Refunded',
  `base_discount_tax_compensation_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Refunded',
  `tax_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Tax Canceled',
  `discount_tax_compensation_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Canceled',
  `tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Tax Refunded',
  `base_tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Refunded',
  `discount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Discount Refunded',
  `base_discount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Refunded',
  `free_shipping` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Free Shipping',
  `weee_tax_applied` text COMMENT 'Weee Tax Applied',
  `weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Amount',
  `weee_tax_applied_row_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Row Amount',
  `weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Disposition',
  `weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Row Disposition',
  `base_weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Amount',
  `base_weee_tax_applied_row_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Row Amnt',
  `base_weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Disposition',
  `base_weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Row Disposition',
  `gift_message_id` int(11) DEFAULT NULL COMMENT 'Gift Message Id',
  `gift_message_available` int(11) DEFAULT NULL COMMENT 'Gift Message Available'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order Item';

--
-- Дамп даних таблиці `sales_order_item`
--

INSERT INTO `sales_order_item` (`item_id`, `order_id`, `parent_item_id`, `quote_item_id`, `store_id`, `created_at`, `updated_at`, `product_id`, `product_type`, `product_options`, `weight`, `is_virtual`, `sku`, `name`, `description`, `applied_rule_ids`, `additional_data`, `is_qty_decimal`, `no_discount`, `qty_backordered`, `qty_canceled`, `qty_invoiced`, `qty_ordered`, `qty_refunded`, `qty_shipped`, `base_cost`, `price`, `base_price`, `original_price`, `base_original_price`, `tax_percent`, `tax_amount`, `base_tax_amount`, `tax_invoiced`, `base_tax_invoiced`, `discount_percent`, `discount_amount`, `base_discount_amount`, `discount_invoiced`, `base_discount_invoiced`, `amount_refunded`, `base_amount_refunded`, `row_total`, `base_row_total`, `row_invoiced`, `base_row_invoiced`, `row_weight`, `base_tax_before_discount`, `tax_before_discount`, `ext_order_item_id`, `locked_do_invoice`, `locked_do_ship`, `price_incl_tax`, `base_price_incl_tax`, `row_total_incl_tax`, `base_row_total_incl_tax`, `discount_tax_compensation_amount`, `base_discount_tax_compensation_amount`, `discount_tax_compensation_invoiced`, `base_discount_tax_compensation_invoiced`, `discount_tax_compensation_refunded`, `base_discount_tax_compensation_refunded`, `tax_canceled`, `discount_tax_compensation_canceled`, `tax_refunded`, `base_tax_refunded`, `discount_refunded`, `base_discount_refunded`, `free_shipping`, `weee_tax_applied`, `weee_tax_applied_amount`, `weee_tax_applied_row_amount`, `weee_tax_disposition`, `weee_tax_row_disposition`, `base_weee_tax_applied_amount`, `base_weee_tax_applied_row_amnt`, `base_weee_tax_disposition`, `base_weee_tax_row_disposition`, `gift_message_id`, `gift_message_available`) VALUES
(1, 1, NULL, NULL, 1, '2018-08-14 05:53:38', '2018-08-14 05:53:40', 1433, 'configurable', '{"info_buyRequest":{"qty":1,"super_attribute":{"141":"167","93":"58"},"options":[]},"attributes_info":[{"label":"Size","value":"XS","option_id":141,"option_value":"167"},{"label":"Color","value":"Red","option_id":93,"option_value":"58"}],"simple_name":"Iris Workout Top-XS-Red","simple_sku":"WS03-XS-Red","product_calculations":1,"shipment_type":0}', '1.0000', NULL, 'WS03-XS-Red', 'Iris Workout Top', NULL, '1', NULL, 0, 0, NULL, '0.0000', '1.0000', '1.0000', '0.0000', '1.0000', NULL, '29.0000', '29.0000', '29.0000', '29.0000', '8.2500', '2.3900', '2.3900', '2.3900', '2.3900', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '29.0000', '29.0000', '29.0000', '29.0000', '1.0000', NULL, NULL, NULL, NULL, NULL, '31.3900', '31.3900', '31.3900', '31.3900', '0.0000', '0.0000', '0.0000', '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 2, NULL, NULL, 1, '2018-08-14 05:53:41', '2018-08-14 05:53:44', 1497, 'configurable', '{"info_buyRequest":{"qty":1,"super_attribute":{"141":"167","93":"50"},"options":[]},"attributes_info":[{"label":"Size","value":"XS","option_id":141,"option_value":"167"},{"label":"Color","value":"Blue","option_id":93,"option_value":"50"}],"simple_name":"Minerva LumaTech&trade; V-Tee-XS-Blue","simple_sku":"WS08-XS-Blue","product_calculations":1,"shipment_type":0}', '1.0000', NULL, 'WS08-XS-Blue', 'Minerva LumaTech&trade; V-Tee', NULL, '1', NULL, 0, 0, NULL, '0.0000', '1.0000', '1.0000', '1.0000', '1.0000', NULL, '32.0000', '32.0000', '32.0000', '32.0000', '8.2500', '2.6400', '2.6400', '2.6400', '2.6400', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '32.0000', '32.0000', '32.0000', '32.0000', '32.0000', '32.0000', '1.0000', NULL, NULL, NULL, NULL, NULL, '34.6400', '34.6400', '34.6400', '34.6400', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', NULL, NULL, '2.6400', '2.6400', '0.0000', '0.0000', 0, '[]', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `sales_order_payment`
--

CREATE TABLE `sales_order_payment` (
  `entity_id` int(10) UNSIGNED NOT NULL COMMENT 'Entity Id',
  `parent_id` int(10) UNSIGNED NOT NULL COMMENT 'Parent Id',
  `base_shipping_captured` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Captured',
  `shipping_captured` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Captured',
  `amount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Amount Refunded',
  `base_amount_paid` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Paid',
  `amount_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Amount Canceled',
  `base_amount_authorized` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Authorized',
  `base_amount_paid_online` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Paid Online',
  `base_amount_refunded_online` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Refunded Online',
  `base_shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Amount',
  `shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Amount',
  `amount_paid` decimal(12,4) DEFAULT NULL COMMENT 'Amount Paid',
  `amount_authorized` decimal(12,4) DEFAULT NULL COMMENT 'Amount Authorized',
  `base_amount_ordered` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Ordered',
  `base_shipping_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Refunded',
  `shipping_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Refunded',
  `base_amount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Refunded',
  `amount_ordered` decimal(12,4) DEFAULT NULL COMMENT 'Amount Ordered',
  `base_amount_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount Canceled',
  `quote_payment_id` int(11) DEFAULT NULL COMMENT 'Quote Payment Id',
  `additional_data` text COMMENT 'Additional Data',
  `cc_exp_month` varchar(12) DEFAULT NULL COMMENT 'Cc Exp Month',
  `cc_ss_start_year` varchar(12) DEFAULT NULL COMMENT 'Cc Ss Start Year',
  `echeck_bank_name` varchar(128) DEFAULT NULL COMMENT 'Echeck Bank Name',
  `method` varchar(128) DEFAULT NULL COMMENT 'Method',
  `cc_debug_request_body` varchar(32) DEFAULT NULL COMMENT 'Cc Debug Request Body',
  `cc_secure_verify` varchar(32) DEFAULT NULL COMMENT 'Cc Secure Verify',
  `protection_eligibility` varchar(32) DEFAULT NULL COMMENT 'Protection Eligibility',
  `cc_approval` varchar(32) DEFAULT NULL COMMENT 'Cc Approval',
  `cc_last_4` varchar(100) DEFAULT NULL COMMENT 'Cc Last 4',
  `cc_status_description` varchar(32) DEFAULT NULL COMMENT 'Cc Status Description',
  `echeck_type` varchar(32) DEFAULT NULL COMMENT 'Echeck Type',
  `cc_debug_response_serialized` varchar(32) DEFAULT NULL COMMENT 'Cc Debug Response Serialized',
  `cc_ss_start_month` varchar(128) DEFAULT NULL COMMENT 'Cc Ss Start Month',
  `echeck_account_type` varchar(255) DEFAULT NULL COMMENT 'Echeck Account Type',
  `last_trans_id` varchar(32) DEFAULT NULL COMMENT 'Last Trans Id',
  `cc_cid_status` varchar(32) DEFAULT NULL COMMENT 'Cc Cid Status',
  `cc_owner` varchar(128) DEFAULT NULL COMMENT 'Cc Owner',
  `cc_type` varchar(32) DEFAULT NULL COMMENT 'Cc Type',
  `po_number` varchar(32) DEFAULT NULL COMMENT 'Po Number',
  `cc_exp_year` varchar(4) DEFAULT NULL COMMENT 'Cc Exp Year',
  `cc_status` varchar(4) DEFAULT NULL COMMENT 'Cc Status',
  `echeck_routing_number` varchar(32) DEFAULT NULL COMMENT 'Echeck Routing Number',
  `account_status` varchar(32) DEFAULT NULL COMMENT 'Account Status',
  `anet_trans_method` varchar(32) DEFAULT NULL COMMENT 'Anet Trans Method',
  `cc_debug_response_body` varchar(32) DEFAULT NULL COMMENT 'Cc Debug Response Body',
  `cc_ss_issue` varchar(32) DEFAULT NULL COMMENT 'Cc Ss Issue',
  `echeck_account_name` varchar(32) DEFAULT NULL COMMENT 'Echeck Account Name',
  `cc_avs_status` varchar(32) DEFAULT NULL COMMENT 'Cc Avs Status',
  `cc_number_enc` varchar(128) DEFAULT NULL,
  `cc_trans_id` varchar(32) DEFAULT NULL COMMENT 'Cc Trans Id',
  `address_status` varchar(32) DEFAULT NULL COMMENT 'Address Status',
  `additional_information` text COMMENT 'Additional Information'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order Payment';

--
-- Дамп даних таблиці `sales_order_payment`
--

INSERT INTO `sales_order_payment` (`entity_id`, `parent_id`, `base_shipping_captured`, `shipping_captured`, `amount_refunded`, `base_amount_paid`, `amount_canceled`, `base_amount_authorized`, `base_amount_paid_online`, `base_amount_refunded_online`, `base_shipping_amount`, `shipping_amount`, `amount_paid`, `amount_authorized`, `base_amount_ordered`, `base_shipping_refunded`, `shipping_refunded`, `base_amount_refunded`, `amount_ordered`, `base_amount_canceled`, `quote_payment_id`, `additional_data`, `cc_exp_month`, `cc_ss_start_year`, `echeck_bank_name`, `method`, `cc_debug_request_body`, `cc_secure_verify`, `protection_eligibility`, `cc_approval`, `cc_last_4`, `cc_status_description`, `echeck_type`, `cc_debug_response_serialized`, `cc_ss_start_month`, `echeck_account_type`, `last_trans_id`, `cc_cid_status`, `cc_owner`, `cc_type`, `po_number`, `cc_exp_year`, `cc_status`, `echeck_routing_number`, `account_status`, `anet_trans_method`, `cc_debug_response_body`, `cc_ss_issue`, `echeck_account_name`, `cc_avs_status`, `cc_number_enc`, `cc_trans_id`, `address_status`, `additional_information`) VALUES
(1, 1, '5.0000', '5.0000', NULL, '36.3900', NULL, NULL, NULL, NULL, '5.0000', '5.0000', '36.3900', NULL, '36.3900', NULL, NULL, NULL, '36.3900', NULL, NULL, NULL, NULL, NULL, NULL, 'checkmo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{"method_title":"Check \\/ Money order"}'),
(2, 2, '5.0000', '5.0000', '39.6400', '39.6400', NULL, NULL, NULL, NULL, '5.0000', '5.0000', '39.6400', NULL, '39.6400', '5.0000', '5.0000', '39.6400', '39.6400', NULL, NULL, NULL, NULL, NULL, NULL, 'checkmo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '{"method_title":"Check \\/ Money order"}');

-- --------------------------------------------------------

--
-- Структура таблиці `sales_order_status`
--

CREATE TABLE `sales_order_status` (
  `status` varchar(32) NOT NULL COMMENT 'Status',
  `label` varchar(128) NOT NULL COMMENT 'Label'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Status Table';

--
-- Дамп даних таблиці `sales_order_status`
--

INSERT INTO `sales_order_status` (`status`, `label`) VALUES
('canceled', 'Canceled'),
('closed', 'Closed'),
('complete', 'Complete'),
('fraud', 'Suspected Fraud'),
('holded', 'On Hold'),
('payment_review', 'Payment Review'),
('paypal_canceled_reversal', 'PayPal Canceled Reversal'),
('paypal_reversed', 'PayPal Reversed'),
('pending', 'Pending'),
('pending_payment', 'Pending Payment'),
('pending_paypal', 'Pending PayPal'),
('processing', 'Processing');

-- --------------------------------------------------------

--
-- Структура таблиці `sales_order_status_history`
--

CREATE TABLE `sales_order_status_history` (
  `entity_id` int(10) UNSIGNED NOT NULL COMMENT 'Entity Id',
  `parent_id` int(10) UNSIGNED NOT NULL COMMENT 'Parent Id',
  `is_customer_notified` int(11) DEFAULT NULL COMMENT 'Is Customer Notified',
  `is_visible_on_front` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Is Visible On Front',
  `comment` text COMMENT 'Comment',
  `status` varchar(32) DEFAULT NULL COMMENT 'Status',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `entity_name` varchar(32) DEFAULT NULL COMMENT 'Shows what entity history is bind to.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order Status History';

--
-- Дамп даних таблиці `sales_order_status_history`
--

INSERT INTO `sales_order_status_history` (`entity_id`, `parent_id`, `is_customer_notified`, `is_visible_on_front`, `comment`, `status`, `created_at`, `entity_name`) VALUES
(1, 2, 1, 0, 'We refunded $39.64 offline.', 'closed', '2018-08-14 05:53:44', 'creditmemo');

-- --------------------------------------------------------

--
-- Структура таблиці `sales_order_status_label`
--

CREATE TABLE `sales_order_status_label` (
  `status` varchar(32) NOT NULL COMMENT 'Status',
  `store_id` smallint(5) UNSIGNED NOT NULL COMMENT 'Store Id',
  `label` varchar(128) NOT NULL COMMENT 'Label'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Status Label Table';

-- --------------------------------------------------------

--
-- Структура таблиці `sales_order_status_state`
--

CREATE TABLE `sales_order_status_state` (
  `status` varchar(32) NOT NULL COMMENT 'Status',
  `state` varchar(32) NOT NULL COMMENT 'Label',
  `is_default` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Is Default',
  `visible_on_front` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Visible on front'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Status Table';

--
-- Дамп даних таблиці `sales_order_status_state`
--

INSERT INTO `sales_order_status_state` (`status`, `state`, `is_default`, `visible_on_front`) VALUES
('canceled', 'canceled', 1, 1),
('closed', 'closed', 1, 1),
('complete', 'complete', 1, 1),
('fraud', 'payment_review', 0, 1),
('fraud', 'processing', 0, 1),
('holded', 'holded', 1, 1),
('payment_review', 'payment_review', 1, 1),
('pending', 'new', 1, 1),
('pending_payment', 'pending_payment', 1, 0),
('processing', 'processing', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблиці `sales_order_tax`
--

CREATE TABLE `sales_order_tax` (
  `tax_id` int(10) UNSIGNED NOT NULL COMMENT 'Tax Id',
  `order_id` int(10) UNSIGNED NOT NULL COMMENT 'Order Id',
  `code` varchar(255) DEFAULT NULL COMMENT 'Code',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  `percent` decimal(12,4) DEFAULT NULL COMMENT 'Percent',
  `amount` decimal(12,4) DEFAULT NULL COMMENT 'Amount',
  `priority` int(11) NOT NULL COMMENT 'Priority',
  `position` int(11) NOT NULL COMMENT 'Position',
  `base_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Amount',
  `process` smallint(6) NOT NULL COMMENT 'Process',
  `base_real_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Real Amount'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Tax Table';

--
-- Дамп даних таблиці `sales_order_tax`
--

INSERT INTO `sales_order_tax` (`tax_id`, `order_id`, `code`, `title`, `percent`, `amount`, `priority`, `position`, `base_amount`, `process`, `base_real_amount`) VALUES
(1, 1, 'US-MI-*-Rate 1', 'US-MI-*-Rate 1', '8.2500', '2.3900', 0, 0, '2.3900', 0, '2.3900'),
(2, 2, 'US-MI-*-Rate 1', 'US-MI-*-Rate 1', '8.2500', '2.6400', 0, 0, '2.6400', 0, '2.6400');

-- --------------------------------------------------------

--
-- Структура таблиці `sales_order_tax_item`
--

CREATE TABLE `sales_order_tax_item` (
  `tax_item_id` int(10) UNSIGNED NOT NULL COMMENT 'Tax Item Id',
  `tax_id` int(10) UNSIGNED NOT NULL COMMENT 'Tax Id',
  `item_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Item Id',
  `tax_percent` decimal(12,4) NOT NULL COMMENT 'Real Tax Percent For Item',
  `amount` decimal(12,4) NOT NULL COMMENT 'Tax amount for the item and tax rate',
  `base_amount` decimal(12,4) NOT NULL COMMENT 'Base tax amount for the item and tax rate',
  `real_amount` decimal(12,4) NOT NULL COMMENT 'Real tax amount for the item and tax rate',
  `real_base_amount` decimal(12,4) NOT NULL COMMENT 'Real base tax amount for the item and tax rate',
  `associated_item_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Id of the associated item',
  `taxable_item_type` varchar(32) NOT NULL COMMENT 'Type of the taxable item'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Order Tax Item';

--
-- Дамп даних таблиці `sales_order_tax_item`
--

INSERT INTO `sales_order_tax_item` (`tax_item_id`, `tax_id`, `item_id`, `tax_percent`, `amount`, `base_amount`, `real_amount`, `real_base_amount`, `associated_item_id`, `taxable_item_type`) VALUES
(1, 1, NULL, '8.2500', '2.3900', '2.3900', '2.3900', '2.3900', NULL, 'product'),
(2, 2, NULL, '8.2500', '2.6400', '2.6400', '2.6400', '2.6400', NULL, 'product');

-- --------------------------------------------------------

--
-- Структура таблиці `sales_payment_transaction`
--

CREATE TABLE `sales_payment_transaction` (
  `transaction_id` int(10) UNSIGNED NOT NULL COMMENT 'Transaction Id',
  `parent_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Parent Id',
  `order_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Order Id',
  `payment_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Payment Id',
  `txn_id` varchar(100) DEFAULT NULL COMMENT 'Txn Id',
  `parent_txn_id` varchar(100) DEFAULT NULL COMMENT 'Parent Txn Id',
  `txn_type` varchar(15) DEFAULT NULL COMMENT 'Txn Type',
  `is_closed` smallint(5) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Is Closed',
  `additional_information` blob COMMENT 'Additional Information',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Payment Transaction';

-- --------------------------------------------------------

--
-- Структура таблиці `sales_refunded_aggregated`
--

CREATE TABLE `sales_refunded_aggregated` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) NOT NULL COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `refunded` decimal(12,4) DEFAULT NULL COMMENT 'Refunded',
  `online_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Online Refunded',
  `offline_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Offline Refunded'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Refunded Aggregated';

-- --------------------------------------------------------

--
-- Структура таблиці `sales_refunded_aggregated_order`
--

CREATE TABLE `sales_refunded_aggregated_order` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `refunded` decimal(12,4) DEFAULT NULL COMMENT 'Refunded',
  `online_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Online Refunded',
  `offline_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Offline Refunded'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Refunded Aggregated Order';

-- --------------------------------------------------------

--
-- Структура таблиці `sales_sequence_meta`
--

CREATE TABLE `sales_sequence_meta` (
  `meta_id` int(10) UNSIGNED NOT NULL COMMENT 'Id',
  `entity_type` varchar(32) NOT NULL COMMENT 'Prefix',
  `store_id` smallint(5) UNSIGNED NOT NULL COMMENT 'Store Id',
  `sequence_table` varchar(32) NOT NULL COMMENT 'table for sequence'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='sales_sequence_meta';

--
-- Дамп даних таблиці `sales_sequence_meta`
--

INSERT INTO `sales_sequence_meta` (`meta_id`, `entity_type`, `store_id`, `sequence_table`) VALUES
(1, 'order', 0, 'sequence_order_0'),
(2, 'invoice', 0, 'sequence_invoice_0'),
(3, 'creditmemo', 0, 'sequence_creditmemo_0'),
(4, 'shipment', 0, 'sequence_shipment_0'),
(5, 'order', 1, 'sequence_order_1'),
(6, 'invoice', 1, 'sequence_invoice_1'),
(7, 'creditmemo', 1, 'sequence_creditmemo_1'),
(8, 'shipment', 1, 'sequence_shipment_1');

-- --------------------------------------------------------

--
-- Структура таблиці `sales_sequence_profile`
--

CREATE TABLE `sales_sequence_profile` (
  `profile_id` int(10) UNSIGNED NOT NULL COMMENT 'Id',
  `meta_id` int(10) UNSIGNED NOT NULL COMMENT 'Meta_id',
  `prefix` varchar(32) DEFAULT NULL COMMENT 'Prefix',
  `suffix` varchar(32) DEFAULT NULL COMMENT 'Suffix',
  `start_value` int(10) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Start value for sequence',
  `step` int(10) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Step for sequence',
  `max_value` int(10) UNSIGNED NOT NULL COMMENT 'MaxValue for sequence',
  `warning_value` int(10) UNSIGNED NOT NULL COMMENT 'WarningValue for sequence',
  `is_active` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'isActive flag'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='sales_sequence_profile';

--
-- Дамп даних таблиці `sales_sequence_profile`
--

INSERT INTO `sales_sequence_profile` (`profile_id`, `meta_id`, `prefix`, `suffix`, `start_value`, `step`, `max_value`, `warning_value`, `is_active`) VALUES
(1, 1, NULL, NULL, 1, 1, 4294967295, 4294966295, 1),
(2, 2, NULL, NULL, 1, 1, 4294967295, 4294966295, 1),
(3, 3, NULL, NULL, 1, 1, 4294967295, 4294966295, 1),
(4, 4, NULL, NULL, 1, 1, 4294967295, 4294966295, 1),
(5, 5, NULL, NULL, 1, 1, 4294967295, 4294966295, 1),
(6, 6, NULL, NULL, 1, 1, 4294967295, 4294966295, 1),
(7, 7, NULL, NULL, 1, 1, 4294967295, 4294966295, 1),
(8, 8, NULL, NULL, 1, 1, 4294967295, 4294966295, 1);

-- --------------------------------------------------------

--
-- Структура таблиці `sales_shipment`
--

CREATE TABLE `sales_shipment` (
  `entity_id` int(10) UNSIGNED NOT NULL COMMENT 'Entity Id',
  `store_id` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Store Id',
  `total_weight` decimal(12,4) DEFAULT NULL COMMENT 'Total Weight',
  `total_qty` decimal(12,4) DEFAULT NULL COMMENT 'Total Qty',
  `email_sent` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Email Sent',
  `send_email` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Send Email',
  `order_id` int(10) UNSIGNED NOT NULL COMMENT 'Order Id',
  `customer_id` int(11) DEFAULT NULL COMMENT 'Customer Id',
  `shipping_address_id` int(11) DEFAULT NULL COMMENT 'Shipping Address Id',
  `billing_address_id` int(11) DEFAULT NULL COMMENT 'Billing Address Id',
  `shipment_status` int(11) DEFAULT NULL COMMENT 'Shipment Status',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
  `packages` text COMMENT 'Packed Products in Packages',
  `shipping_label` mediumblob COMMENT 'Shipping Label Content',
  `customer_note` text COMMENT 'Customer Note',
  `customer_note_notify` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Customer Note Notify'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment';

--
-- Дамп даних таблиці `sales_shipment`
--

INSERT INTO `sales_shipment` (`entity_id`, `store_id`, `total_weight`, `total_qty`, `email_sent`, `send_email`, `order_id`, `customer_id`, `shipping_address_id`, `billing_address_id`, `shipment_status`, `increment_id`, `created_at`, `updated_at`, `packages`, `shipping_label`, `customer_note`, `customer_note_notify`) VALUES
(1, 1, NULL, '1.0000', NULL, NULL, 1, 1, 1, 2, NULL, '000000001', '2018-08-14 05:53:40', '2018-08-14 05:53:40', '[]', NULL, NULL, NULL),
(2, 1, NULL, '1.0000', NULL, NULL, 2, 1, 3, 4, NULL, '000000002', '2018-08-14 05:53:43', '2018-08-14 05:53:43', '[]', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `sales_shipment_comment`
--

CREATE TABLE `sales_shipment_comment` (
  `entity_id` int(10) UNSIGNED NOT NULL COMMENT 'Entity Id',
  `parent_id` int(10) UNSIGNED NOT NULL COMMENT 'Parent Id',
  `is_customer_notified` int(11) DEFAULT NULL COMMENT 'Is Customer Notified',
  `is_visible_on_front` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Is Visible On Front',
  `comment` text COMMENT 'Comment',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment Comment';

-- --------------------------------------------------------

--
-- Структура таблиці `sales_shipment_grid`
--

CREATE TABLE `sales_shipment_grid` (
  `entity_id` int(10) UNSIGNED NOT NULL COMMENT 'Entity Id',
  `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
  `store_id` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Store Id',
  `order_increment_id` varchar(32) NOT NULL COMMENT 'Order Increment Id',
  `order_id` int(10) UNSIGNED NOT NULL COMMENT 'Order Id',
  `order_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Order Increment Id',
  `customer_name` varchar(128) NOT NULL COMMENT 'Customer Name',
  `total_qty` decimal(12,4) DEFAULT NULL COMMENT 'Total Qty',
  `shipment_status` int(11) DEFAULT NULL COMMENT 'Shipment Status',
  `order_status` varchar(32) DEFAULT NULL COMMENT 'Order',
  `billing_address` varchar(255) DEFAULT NULL COMMENT 'Billing Address',
  `shipping_address` varchar(255) DEFAULT NULL COMMENT 'Shipping Address',
  `billing_name` varchar(128) DEFAULT NULL COMMENT 'Billing Name',
  `shipping_name` varchar(128) DEFAULT NULL COMMENT 'Shipping Name',
  `customer_email` varchar(128) DEFAULT NULL COMMENT 'Customer Email',
  `customer_group_id` int(11) DEFAULT NULL,
  `payment_method` varchar(32) DEFAULT NULL COMMENT 'Payment Method',
  `shipping_information` varchar(255) DEFAULT NULL COMMENT 'Shipping Method Name',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment Grid';

--
-- Дамп даних таблиці `sales_shipment_grid`
--

INSERT INTO `sales_shipment_grid` (`entity_id`, `increment_id`, `store_id`, `order_increment_id`, `order_id`, `order_created_at`, `customer_name`, `total_qty`, `shipment_status`, `order_status`, `billing_address`, `shipping_address`, `billing_name`, `shipping_name`, `customer_email`, `customer_group_id`, `payment_method`, `shipping_information`, `created_at`, `updated_at`) VALUES
(1, '000000001', 1, '000000001', 1, '2018-08-14 05:53:38', 'Veronica Costello', '1.0000', NULL, 'processing', '6146 Honey Bluff Parkway Calder Michigan 49628-7978', '6146 Honey Bluff Parkway Calder Michigan 49628-7978', 'Veronica Costello', 'Veronica Costello', 'roni_cost@example.com', 1, 'checkmo', 'Flat Rate - Fixed', '2018-08-14 05:53:40', '2018-08-14 05:53:40'),
(2, '000000002', 1, '000000002', 2, '2018-08-14 05:53:41', 'Veronica Costello', '1.0000', NULL, 'processing', '6146 Honey Bluff Parkway Calder Michigan 49628-7978', '6146 Honey Bluff Parkway Calder Michigan 49628-7978', 'Veronica Costello', 'Veronica Costello', 'roni_cost@example.com', 1, 'checkmo', 'Flat Rate - Fixed', '2018-08-14 05:53:43', '2018-08-14 05:53:43');

-- --------------------------------------------------------

--
-- Структура таблиці `sales_shipment_item`
--

CREATE TABLE `sales_shipment_item` (
  `entity_id` int(10) UNSIGNED NOT NULL COMMENT 'Entity Id',
  `parent_id` int(10) UNSIGNED NOT NULL COMMENT 'Parent Id',
  `row_total` decimal(12,4) DEFAULT NULL COMMENT 'Row Total',
  `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
  `weight` decimal(12,4) DEFAULT NULL COMMENT 'Weight',
  `qty` decimal(12,4) DEFAULT NULL COMMENT 'Qty',
  `product_id` int(11) DEFAULT NULL COMMENT 'Product Id',
  `order_item_id` int(11) DEFAULT NULL COMMENT 'Order Item Id',
  `additional_data` text COMMENT 'Additional Data',
  `description` text COMMENT 'Description',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `sku` varchar(255) DEFAULT NULL COMMENT 'Sku'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment Item';

--
-- Дамп даних таблиці `sales_shipment_item`
--

INSERT INTO `sales_shipment_item` (`entity_id`, `parent_id`, `row_total`, `price`, `weight`, `qty`, `product_id`, `order_item_id`, `additional_data`, `description`, `name`, `sku`) VALUES
(1, 1, NULL, '29.0000', '1.0000', '1.0000', 1433, 1, NULL, NULL, 'Iris Workout Top', 'WS03-XS-Red'),
(2, 2, NULL, '32.0000', '1.0000', '1.0000', 1497, 2, NULL, NULL, 'Minerva LumaTech&trade; V-Tee', 'WS08-XS-Blue');

-- --------------------------------------------------------

--
-- Структура таблиці `sales_shipment_track`
--

CREATE TABLE `sales_shipment_track` (
  `entity_id` int(10) UNSIGNED NOT NULL COMMENT 'Entity Id',
  `parent_id` int(10) UNSIGNED NOT NULL COMMENT 'Parent Id',
  `weight` decimal(12,4) DEFAULT NULL COMMENT 'Weight',
  `qty` decimal(12,4) DEFAULT NULL COMMENT 'Qty',
  `order_id` int(10) UNSIGNED NOT NULL COMMENT 'Order Id',
  `track_number` text COMMENT 'Number',
  `description` text COMMENT 'Description',
  `title` varchar(255) DEFAULT NULL COMMENT 'Title',
  `carrier_code` varchar(32) DEFAULT NULL COMMENT 'Carrier Code',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment Track';

-- --------------------------------------------------------

--
-- Структура таблиці `sales_shipping_aggregated`
--

CREATE TABLE `sales_shipping_aggregated` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `shipping_description` varchar(255) DEFAULT NULL COMMENT 'Shipping Description',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `total_shipping` decimal(12,4) DEFAULT NULL COMMENT 'Total Shipping',
  `total_shipping_actual` decimal(12,4) DEFAULT NULL COMMENT 'Total Shipping Actual'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Shipping Aggregated';

-- --------------------------------------------------------

--
-- Структура таблиці `sales_shipping_aggregated_order`
--

CREATE TABLE `sales_shipping_aggregated_order` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Store Id',
  `order_status` varchar(50) DEFAULT NULL COMMENT 'Order Status',
  `shipping_description` varchar(255) DEFAULT NULL COMMENT 'Shipping Description',
  `orders_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `total_shipping` decimal(12,4) DEFAULT NULL COMMENT 'Total Shipping',
  `total_shipping_actual` decimal(12,4) DEFAULT NULL COMMENT 'Total Shipping Actual'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Shipping Aggregated Order';

-- --------------------------------------------------------

--
-- Структура таблиці `search_query`
--

CREATE TABLE `search_query` (
  `query_id` int(10) UNSIGNED NOT NULL COMMENT 'Query ID',
  `query_text` varchar(255) DEFAULT NULL COMMENT 'Query text',
  `num_results` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Num results',
  `popularity` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Popularity',
  `redirect` varchar(255) DEFAULT NULL COMMENT 'Redirect',
  `store_id` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `display_in_terms` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Display in terms',
  `is_active` smallint(6) DEFAULT '1' COMMENT 'Active status',
  `is_processed` smallint(6) DEFAULT '0' COMMENT 'Processed status',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated at'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Search query table';

-- --------------------------------------------------------

--
-- Структура таблиці `search_synonyms`
--

CREATE TABLE `search_synonyms` (
  `group_id` bigint(20) UNSIGNED NOT NULL COMMENT 'Synonyms Group Id',
  `synonyms` text NOT NULL COMMENT 'list of synonyms making up this group',
  `store_id` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Store Id - identifies the store view these synonyms belong to',
  `website_id` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Website Id - identifies the website id these synonyms belong to'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table storing various synonyms groups';

-- --------------------------------------------------------

--
-- Структура таблиці `sendfriend_log`
--

CREATE TABLE `sendfriend_log` (
  `log_id` int(10) UNSIGNED NOT NULL COMMENT 'Log ID',
  `ip` bigint(20) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Customer IP address',
  `time` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Log time',
  `website_id` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Website ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Send to friend function log storage table';

-- --------------------------------------------------------

--
-- Структура таблиці `sequence_creditmemo_0`
--

CREATE TABLE `sequence_creditmemo_0` (
  `sequence_value` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `sequence_creditmemo_1`
--

CREATE TABLE `sequence_creditmemo_1` (
  `sequence_value` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `sequence_creditmemo_1`
--

INSERT INTO `sequence_creditmemo_1` (`sequence_value`) VALUES
(1);

-- --------------------------------------------------------

--
-- Структура таблиці `sequence_invoice_0`
--

CREATE TABLE `sequence_invoice_0` (
  `sequence_value` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `sequence_invoice_1`
--

CREATE TABLE `sequence_invoice_1` (
  `sequence_value` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `sequence_invoice_1`
--

INSERT INTO `sequence_invoice_1` (`sequence_value`) VALUES
(1),
(2);

-- --------------------------------------------------------

--
-- Структура таблиці `sequence_order_0`
--

CREATE TABLE `sequence_order_0` (
  `sequence_value` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `sequence_order_1`
--

CREATE TABLE `sequence_order_1` (
  `sequence_value` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `sequence_order_1`
--

INSERT INTO `sequence_order_1` (`sequence_value`) VALUES
(1),
(2);

-- --------------------------------------------------------

--
-- Структура таблиці `sequence_shipment_0`
--

CREATE TABLE `sequence_shipment_0` (
  `sequence_value` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `sequence_shipment_1`
--

CREATE TABLE `sequence_shipment_1` (
  `sequence_value` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `sequence_shipment_1`
--

INSERT INTO `sequence_shipment_1` (`sequence_value`) VALUES
(1),
(2);

-- --------------------------------------------------------

--
-- Структура таблиці `session`
--

CREATE TABLE `session` (
  `session_id` varchar(255) NOT NULL COMMENT 'Session Id',
  `session_expires` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Date of Session Expiration',
  `session_data` mediumblob NOT NULL COMMENT 'Session Data'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Database Sessions Storage';

-- --------------------------------------------------------

--
-- Структура таблиці `setup_module`
--

CREATE TABLE `setup_module` (
  `module` varchar(50) NOT NULL COMMENT 'Module',
  `schema_version` varchar(50) DEFAULT NULL COMMENT 'Schema Version',
  `data_version` varchar(50) DEFAULT NULL COMMENT 'Data Version'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Module versions registry';

--
-- Дамп даних таблиці `setup_module`
--

INSERT INTO `setup_module` (`module`, `schema_version`, `data_version`) VALUES
('Amazon_Core', '2.0.12', '2.0.12'),
('Amazon_Login', '1.2.0', '1.2.0'),
('Amazon_Payment', '1.11.0', '1.11.0'),
('Dotdigitalgroup_Email', '2.5.0', '2.5.0'),
('Klarna_Core', '4.2.3', '4.2.3'),
('Klarna_Kp', '5.2.3', '5.2.3'),
('Klarna_Ordermanagement', '4.1.3', '4.1.3'),
('Magento_AdminNotification', '2.0.0', '2.0.0'),
('Magento_AdvancedPricingImportExport', '2.0.0', '2.0.0'),
('Magento_Analytics', '2.0.0', '2.0.0'),
('Magento_Authorization', '2.0.0', '2.0.0'),
('Magento_Authorizenet', '2.0.0', '2.0.0'),
('Magento_Backend', '2.0.0', '2.0.0'),
('Magento_Backup', '2.0.0', '2.0.0'),
('Magento_Braintree', '2.0.1', '2.0.1'),
('Magento_Bundle', '2.0.4', '2.0.4'),
('Magento_BundleImportExport', '2.0.0', '2.0.0'),
('Magento_BundleSampleData', '2.0.0', '2.0.0'),
('Magento_CacheInvalidate', '2.0.0', '2.0.0'),
('Magento_Captcha', '2.0.0', '2.0.0'),
('Magento_Catalog', '2.2.5', '2.2.5'),
('Magento_CatalogAnalytics', '2.0.0', '2.0.0'),
('Magento_CatalogImportExport', '2.0.0', '2.0.0'),
('Magento_CatalogInventory', '2.3.0', '2.3.0'),
('Magento_CatalogRule', '2.1.0', '2.1.0'),
('Magento_CatalogRuleConfigurable', '2.0.0', '2.0.0'),
('Magento_CatalogRuleSampleData', '2.0.0', '2.0.0'),
('Magento_CatalogSampleData', '2.0.0', '2.0.0'),
('Magento_CatalogSearch', '2.0.0', '2.0.0'),
('Magento_CatalogUrlRewrite', '2.0.0', '2.0.0'),
('Magento_CatalogWidget', '2.0.0', '2.0.0'),
('Magento_Checkout', '2.0.0', '2.0.0'),
('Magento_CheckoutAgreements', '2.2.0', '2.2.0'),
('Magento_Cms', '2.0.2', '2.0.2'),
('Magento_CmsSampleData', '2.0.0', '2.0.0'),
('Magento_CmsUrlRewrite', '2.0.0', '2.0.0'),
('Magento_Config', '2.0.0', '2.0.0'),
('Magento_ConfigurableImportExport', '2.0.0', '2.0.0'),
('Magento_ConfigurableProduct', '2.2.0', '2.2.0'),
('Magento_ConfigurableProductSales', '2.2.0', '2.2.0'),
('Magento_ConfigurableSampleData', '2.0.0', '2.0.0'),
('Magento_Contact', '2.0.0', '2.0.0'),
('Magento_Cookie', '2.0.0', '2.0.0'),
('Magento_Cron', '2.0.0', '2.0.0'),
('Magento_CurrencySymbol', '2.0.1', '2.0.1'),
('Magento_Customer', '2.0.13', '2.0.13'),
('Magento_CustomerAnalytics', '2.0.0', '2.0.0'),
('Magento_CustomerImportExport', '2.0.0', '2.0.0'),
('Magento_CustomerSampleData', '2.0.0', '2.0.0'),
('Magento_Deploy', '2.0.0', '2.0.0'),
('Magento_Developer', '2.0.0', '2.0.0'),
('Magento_Dhl', '2.0.0', '2.0.0'),
('Magento_Directory', '2.0.2', '2.0.2'),
('Magento_Downloadable', '2.0.2', '2.0.2'),
('Magento_DownloadableImportExport', '2.0.0', '2.0.0'),
('Magento_DownloadableSampleData', '2.0.0', '2.0.0'),
('Magento_Eav', '2.1.1', '2.1.1'),
('Magento_Email', '2.0.0', '2.0.0'),
('Magento_EncryptionKey', '2.0.0', '2.0.0'),
('Magento_Fedex', '2.0.0', '2.0.0'),
('Magento_GiftMessage', '2.0.1', '2.0.1'),
('Magento_GoogleAdwords', '2.0.0', '2.0.0'),
('Magento_GoogleAnalytics', '2.0.0', '2.0.0'),
('Magento_GoogleOptimizer', '2.0.0', '2.0.0'),
('Magento_GroupedImportExport', '2.0.0', '2.0.0'),
('Magento_GroupedProduct', '2.0.1', '2.0.1'),
('Magento_GroupedProductSampleData', '2.0.0', '2.0.0'),
('Magento_ImportExport', '2.0.1', '2.0.1'),
('Magento_Indexer', '2.1.0', '2.1.0'),
('Magento_InstantPurchase', '1.0.0', '1.0.0'),
('Magento_Integration', '2.2.0', '2.2.0'),
('Magento_LayeredNavigation', '2.0.0', '2.0.0'),
('Magento_Marketplace', '1.0.0', '1.0.0'),
('Magento_MediaStorage', '2.0.0', '2.0.0'),
('Magento_Msrp', '2.1.3', '2.1.3'),
('Magento_MsrpSampleData', '2.0.0', '2.0.0'),
('Magento_Multishipping', '2.0.0', '2.0.0'),
('Magento_NewRelicReporting', '2.0.1', '2.0.1'),
('Magento_Newsletter', '2.0.1', '2.0.1'),
('Magento_OfflinePayments', '2.0.0', '2.0.0'),
('Magento_OfflineShipping', '2.0.1', '2.0.1'),
('Magento_OfflineShippingSampleData', '2.0.0', '2.0.0'),
('Magento_PageCache', '2.0.0', '2.0.0'),
('Magento_Payment', '2.0.0', '2.0.0'),
('Magento_Paypal', '2.0.1', '2.0.1'),
('Magento_Persistent', '2.0.0', '2.0.0'),
('Magento_ProductAlert', '2.0.0', '2.0.0'),
('Magento_ProductLinksSampleData', '2.0.0', '2.0.0'),
('Magento_ProductVideo', '2.0.0.2', '2.0.0.2'),
('Magento_Quote', '2.0.7', '2.0.7'),
('Magento_QuoteAnalytics', '2.0.0', '2.0.0'),
('Magento_ReleaseNotification', '2.2.0', '2.2.0'),
('Magento_Reports', '2.0.0', '2.0.0'),
('Magento_RequireJs', '2.0.0', '2.0.0'),
('Magento_Review', '2.0.0', '2.0.0'),
('Magento_ReviewAnalytics', '2.0.0', '2.0.0'),
('Magento_ReviewSampleData', '2.0.0', '2.0.0'),
('Magento_Robots', '2.0.0', '2.0.0'),
('Magento_Rss', '2.0.0', '2.0.0'),
('Magento_Rule', '2.0.0', '2.0.0'),
('Magento_Sales', '2.0.9', '2.0.9'),
('Magento_SalesAnalytics', '2.0.0', '2.0.0'),
('Magento_SalesInventory', '1.0.0', '1.0.0'),
('Magento_SalesRule', '2.0.3', '2.0.3'),
('Magento_SalesRuleSampleData', '2.0.0', '2.0.0'),
('Magento_SalesSampleData', '2.0.0', '2.0.0'),
('Magento_SalesSequence', '2.0.0', '2.0.0'),
('Magento_SampleData', '2.0.0', '2.0.0'),
('Magento_Search', '2.0.4', '2.0.4'),
('Magento_Security', '2.0.1', '2.0.1'),
('Magento_SendFriend', '2.0.0', '2.0.0'),
('Magento_Shipping', '2.0.0', '2.0.0'),
('Magento_Signifyd', '2.2.0', '2.2.0'),
('Magento_Sitemap', '2.0.0', '2.0.0'),
('Magento_Store', '2.1.0', '2.1.0'),
('Magento_Swagger', '2.0.0', '2.0.0'),
('Magento_Swatches', '2.0.3', '2.0.3'),
('Magento_SwatchesLayeredNavigation', '2.0.0', '2.0.0'),
('Magento_SwatchesSampleData', '2.0.0', '2.0.0'),
('Magento_Tax', '2.0.3', '2.0.3'),
('Magento_TaxImportExport', '2.0.0', '2.0.0'),
('Magento_TaxSampleData', '2.0.0', '2.0.0'),
('Magento_Theme', '2.0.2', '2.0.2'),
('Magento_ThemeSampleData', '2.0.0', '2.0.0'),
('Magento_Translation', '2.0.0', '2.0.0'),
('Magento_Ui', '2.0.0', '2.0.0'),
('Magento_Ups', '2.0.0', '2.0.0'),
('Magento_UrlRewrite', '2.0.1', '2.0.1'),
('Magento_User', '2.0.3', '2.0.3'),
('Magento_Usps', '2.0.1', '2.0.1'),
('Magento_Variable', '2.0.0', '2.0.0'),
('Magento_Vault', '2.0.3', '2.0.3'),
('Magento_Version', '2.0.0', '2.0.0'),
('Magento_Webapi', '2.0.0', '2.0.0'),
('Magento_WebapiSecurity', '2.0.0', '2.0.0'),
('Magento_Weee', '2.0.0', '2.0.0'),
('Magento_Widget', '2.0.1', '2.0.1'),
('Magento_WidgetSampleData', '2.0.0', '2.0.0'),
('Magento_Wishlist', '2.0.1', '2.0.1'),
('Magento_WishlistAnalytics', '2.0.0', '2.0.0'),
('Magento_WishlistSampleData', '2.0.0', '2.0.0'),
('Shopial_Facebook', '1.0.0', '1.0.0'),
('Temando_Shipping', '1.2.0', '1.2.0'),
('Test_Widget', '2.0.0', '2.0.0'),
('Trive_Core', '1.0.0', '1.0.0'),
('Trive_Revo', '1.0.2', '1.0.2'),
('Vertex_Tax', '100.2.0', '100.2.0');

-- --------------------------------------------------------

--
-- Структура таблиці `shipping_tablerate`
--

CREATE TABLE `shipping_tablerate` (
  `pk` int(10) UNSIGNED NOT NULL COMMENT 'Primary key',
  `website_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Website Id',
  `dest_country_id` varchar(4) NOT NULL DEFAULT '0' COMMENT 'Destination coutry ISO/2 or ISO/3 code',
  `dest_region_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Destination Region Id',
  `dest_zip` varchar(10) NOT NULL DEFAULT '*' COMMENT 'Destination Post Code (Zip)',
  `condition_name` varchar(20) NOT NULL COMMENT 'Rate Condition name',
  `condition_value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Rate condition value',
  `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
  `cost` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Cost'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Shipping Tablerate';

--
-- Дамп даних таблиці `shipping_tablerate`
--

INSERT INTO `shipping_tablerate` (`pk`, `website_id`, `dest_country_id`, `dest_region_id`, `dest_zip`, `condition_name`, `condition_value`, `price`, `cost`) VALUES
(1, 1, 'US', 0, '*', 'package_value', '0.0000', '15.0000', '0.0000'),
(2, 1, 'US', 0, '*', 'package_value', '50.0000', '10.0000', '0.0000'),
(3, 1, 'US', 0, '*', 'package_value', '100.0000', '5.0000', '0.0000'),
(4, 1, 'US', 2, '*', 'package_value', '0.0000', '20.0000', '0.0000'),
(5, 1, 'US', 2, '*', 'package_value', '50.0000', '15.0000', '0.0000'),
(6, 1, 'US', 2, '*', 'package_value', '100.0000', '10.0000', '0.0000'),
(7, 1, 'US', 21, '*', 'package_value', '0.0000', '20.0000', '0.0000'),
(8, 1, 'US', 21, '*', 'package_value', '50.0000', '15.0000', '0.0000'),
(9, 1, 'US', 21, '*', 'package_value', '100.0000', '10.0000', '0.0000');

-- --------------------------------------------------------

--
-- Структура таблиці `signifyd_case`
--

CREATE TABLE `signifyd_case` (
  `entity_id` int(10) UNSIGNED NOT NULL COMMENT 'Entity_id',
  `order_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Order_id',
  `case_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Case_id',
  `guarantee_eligible` tinyint(1) DEFAULT NULL COMMENT 'Guarantee_eligible',
  `guarantee_disposition` varchar(32) DEFAULT 'PENDING' COMMENT 'Guarantee_disposition',
  `status` varchar(32) DEFAULT 'PENDING' COMMENT 'Status',
  `score` int(10) UNSIGNED DEFAULT NULL COMMENT 'Score',
  `associated_team` text COMMENT 'Associated_team',
  `review_disposition` varchar(32) DEFAULT NULL COMMENT 'Review_disposition',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created_at',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated_at'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='signifyd_case';

-- --------------------------------------------------------

--
-- Структура таблиці `sitemap`
--

CREATE TABLE `sitemap` (
  `sitemap_id` int(10) UNSIGNED NOT NULL COMMENT 'Sitemap Id',
  `sitemap_type` varchar(32) DEFAULT NULL COMMENT 'Sitemap Type',
  `sitemap_filename` varchar(32) DEFAULT NULL COMMENT 'Sitemap Filename',
  `sitemap_path` varchar(255) DEFAULT NULL COMMENT 'Sitemap Path',
  `sitemap_time` timestamp NULL DEFAULT NULL COMMENT 'Sitemap Time',
  `store_id` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Store id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='XML Sitemap';

-- --------------------------------------------------------

--
-- Структура таблиці `store`
--

CREATE TABLE `store` (
  `store_id` smallint(5) UNSIGNED NOT NULL COMMENT 'Store Id',
  `code` varchar(32) DEFAULT NULL COMMENT 'Code',
  `website_id` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Website Id',
  `group_id` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Group Id',
  `name` varchar(255) NOT NULL COMMENT 'Store Name',
  `sort_order` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Store Sort Order',
  `is_active` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Store Activity'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores';

--
-- Дамп даних таблиці `store`
--

INSERT INTO `store` (`store_id`, `code`, `website_id`, `group_id`, `name`, `sort_order`, `is_active`) VALUES
(0, 'admin', 0, 0, 'Admin', 0, 1),
(1, 'default', 1, 1, 'Default Store View', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблиці `store_group`
--

CREATE TABLE `store_group` (
  `group_id` smallint(5) UNSIGNED NOT NULL COMMENT 'Group Id',
  `website_id` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Website Id',
  `code` varchar(32) DEFAULT NULL COMMENT 'Store group unique code',
  `name` varchar(255) NOT NULL COMMENT 'Store Group Name',
  `root_category_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Root Category Id',
  `default_store_id` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Default Store Id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Store Groups';

--
-- Дамп даних таблиці `store_group`
--

INSERT INTO `store_group` (`group_id`, `website_id`, `code`, `name`, `root_category_id`, `default_store_id`) VALUES
(0, 0, 'default', 'Default', 0, 0),
(1, 1, 'main_website_store', 'Main Website Store', 2, 1);

-- --------------------------------------------------------

--
-- Структура таблиці `store_website`
--

CREATE TABLE `store_website` (
  `website_id` smallint(5) UNSIGNED NOT NULL COMMENT 'Website Id',
  `code` varchar(32) DEFAULT NULL COMMENT 'Code',
  `name` varchar(64) DEFAULT NULL COMMENT 'Website Name',
  `sort_order` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `default_group_id` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Default Group Id',
  `is_default` smallint(5) UNSIGNED DEFAULT '0' COMMENT 'Defines Is Website Default'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Websites';

--
-- Дамп даних таблиці `store_website`
--

INSERT INTO `store_website` (`website_id`, `code`, `name`, `sort_order`, `default_group_id`, `is_default`) VALUES
(0, 'admin', 'Admin', 0, 0, 0),
(1, 'base', 'Main Website', 0, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблиці `tax_calculation`
--

CREATE TABLE `tax_calculation` (
  `tax_calculation_id` int(11) NOT NULL COMMENT 'Tax Calculation Id',
  `tax_calculation_rate_id` int(11) NOT NULL COMMENT 'Tax Calculation Rate Id',
  `tax_calculation_rule_id` int(11) NOT NULL COMMENT 'Tax Calculation Rule Id',
  `customer_tax_class_id` smallint(6) NOT NULL COMMENT 'Customer Tax Class Id',
  `product_tax_class_id` smallint(6) NOT NULL COMMENT 'Product Tax Class Id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tax Calculation';

--
-- Дамп даних таблиці `tax_calculation`
--

INSERT INTO `tax_calculation` (`tax_calculation_id`, `tax_calculation_rate_id`, `tax_calculation_rule_id`, `customer_tax_class_id`, `product_tax_class_id`) VALUES
(1, 3, 1, 3, 2);

-- --------------------------------------------------------

--
-- Структура таблиці `tax_calculation_rate`
--

CREATE TABLE `tax_calculation_rate` (
  `tax_calculation_rate_id` int(11) NOT NULL COMMENT 'Tax Calculation Rate Id',
  `tax_country_id` varchar(2) NOT NULL COMMENT 'Tax Country Id',
  `tax_region_id` int(11) NOT NULL COMMENT 'Tax Region Id',
  `tax_postcode` varchar(21) DEFAULT NULL COMMENT 'Tax Postcode',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `rate` decimal(12,4) NOT NULL COMMENT 'Rate',
  `zip_is_range` smallint(6) DEFAULT NULL COMMENT 'Zip Is Range',
  `zip_from` int(10) UNSIGNED DEFAULT NULL COMMENT 'Zip From',
  `zip_to` int(10) UNSIGNED DEFAULT NULL COMMENT 'Zip To'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tax Calculation Rate';

--
-- Дамп даних таблиці `tax_calculation_rate`
--

INSERT INTO `tax_calculation_rate` (`tax_calculation_rate_id`, `tax_country_id`, `tax_region_id`, `tax_postcode`, `code`, `rate`, `zip_is_range`, `zip_from`, `zip_to`) VALUES
(1, 'US', 12, '*', 'US-CA-*-Rate 1', '8.2500', NULL, NULL, NULL),
(2, 'US', 43, '*', 'US-NY-*-Rate 1', '8.3750', NULL, NULL, NULL),
(3, 'US', 33, '*', 'US-MI-*-Rate 1', '8.2500', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `tax_calculation_rate_title`
--

CREATE TABLE `tax_calculation_rate_title` (
  `tax_calculation_rate_title_id` int(11) NOT NULL COMMENT 'Tax Calculation Rate Title Id',
  `tax_calculation_rate_id` int(11) NOT NULL COMMENT 'Tax Calculation Rate Id',
  `store_id` smallint(5) UNSIGNED NOT NULL COMMENT 'Store Id',
  `value` varchar(255) NOT NULL COMMENT 'Value'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tax Calculation Rate Title';

-- --------------------------------------------------------

--
-- Структура таблиці `tax_calculation_rule`
--

CREATE TABLE `tax_calculation_rule` (
  `tax_calculation_rule_id` int(11) NOT NULL COMMENT 'Tax Calculation Rule Id',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `priority` int(11) NOT NULL COMMENT 'Priority',
  `position` int(11) NOT NULL COMMENT 'Position',
  `calculate_subtotal` int(11) NOT NULL COMMENT 'Calculate off subtotal option'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tax Calculation Rule';

--
-- Дамп даних таблиці `tax_calculation_rule`
--

INSERT INTO `tax_calculation_rule` (`tax_calculation_rule_id`, `code`, `priority`, `position`, `calculate_subtotal`) VALUES
(1, 'Rule1', 0, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблиці `tax_class`
--

CREATE TABLE `tax_class` (
  `class_id` smallint(6) NOT NULL COMMENT 'Class Id',
  `class_name` varchar(255) NOT NULL COMMENT 'Class Name',
  `class_type` varchar(8) NOT NULL DEFAULT 'CUSTOMER' COMMENT 'Class Type'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tax Class';

--
-- Дамп даних таблиці `tax_class`
--

INSERT INTO `tax_class` (`class_id`, `class_name`, `class_type`) VALUES
(2, 'Taxable Goods', 'PRODUCT'),
(3, 'Retail Customer', 'CUSTOMER'),
(4, 'Refund Adjustments', 'PRODUCT'),
(5, 'Gift Options', 'PRODUCT'),
(6, 'Order Gift Wrapping', 'PRODUCT'),
(7, 'Item Gift Wrapping', 'PRODUCT'),
(8, 'Printed Gift Card', 'PRODUCT'),
(9, 'Reward Points', 'PRODUCT');

-- --------------------------------------------------------

--
-- Структура таблиці `tax_order_aggregated_created`
--

CREATE TABLE `tax_order_aggregated_created` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Store Id',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `order_status` varchar(50) NOT NULL COMMENT 'Order Status',
  `percent` float DEFAULT NULL COMMENT 'Percent',
  `orders_count` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `tax_base_amount_sum` float DEFAULT NULL COMMENT 'Tax Base Amount Sum'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tax Order Aggregation';

-- --------------------------------------------------------

--
-- Структура таблиці `tax_order_aggregated_updated`
--

CREATE TABLE `tax_order_aggregated_updated` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Id',
  `period` date DEFAULT NULL COMMENT 'Period',
  `store_id` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Store Id',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `order_status` varchar(50) NOT NULL COMMENT 'Order Status',
  `percent` float DEFAULT NULL COMMENT 'Percent',
  `orders_count` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Orders Count',
  `tax_base_amount_sum` float DEFAULT NULL COMMENT 'Tax Base Amount Sum'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tax Order Aggregated Updated';

-- --------------------------------------------------------

--
-- Структура таблиці `temando_checkout_address`
--

CREATE TABLE `temando_checkout_address` (
  `entity_id` int(10) UNSIGNED NOT NULL COMMENT 'Entity Id',
  `shipping_address_id` int(10) UNSIGNED NOT NULL COMMENT 'Magento Quote Address Id',
  `service_selection` text COMMENT 'Value Added Services'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Temando Checkout Address';

-- --------------------------------------------------------

--
-- Структура таблиці `temando_collection_point_search`
--

CREATE TABLE `temando_collection_point_search` (
  `shipping_address_id` int(10) UNSIGNED NOT NULL COMMENT 'Entity Id',
  `country_id` varchar(2) NOT NULL COMMENT 'Country Code',
  `postcode` varchar(255) NOT NULL COMMENT 'Zip/Postal Code'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Collection Point Search';

-- --------------------------------------------------------

--
-- Структура таблиці `temando_order`
--

CREATE TABLE `temando_order` (
  `entity_id` int(10) UNSIGNED NOT NULL COMMENT 'Entity Id',
  `order_id` int(10) UNSIGNED NOT NULL COMMENT 'Magento Order Id',
  `ext_order_id` varchar(64) NOT NULL COMMENT 'Temando Order Id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Temando Order';

-- --------------------------------------------------------

--
-- Структура таблиці `temando_order_collection_point`
--

CREATE TABLE `temando_order_collection_point` (
  `recipient_address_id` int(10) UNSIGNED NOT NULL COMMENT 'Entity Id',
  `collection_point_id` varchar(64) NOT NULL COMMENT 'Collection Point Id',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `country` varchar(2) NOT NULL COMMENT 'Country Code',
  `region` varchar(255) NOT NULL COMMENT 'Region',
  `postcode` varchar(255) NOT NULL COMMENT 'Zip/Postal Code',
  `city` varchar(255) NOT NULL COMMENT 'City',
  `street` text NOT NULL COMMENT 'Street'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Order Collection Point Entity';

-- --------------------------------------------------------

--
-- Структура таблиці `temando_quote_collection_point`
--

CREATE TABLE `temando_quote_collection_point` (
  `entity_id` int(10) UNSIGNED NOT NULL COMMENT 'Entity Id',
  `recipient_address_id` int(10) UNSIGNED NOT NULL COMMENT 'Quote Address Id',
  `collection_point_id` varchar(64) NOT NULL COMMENT 'Collection Point Id',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name',
  `country` varchar(2) NOT NULL COMMENT 'Country Code',
  `region` varchar(255) NOT NULL COMMENT 'Region',
  `postcode` varchar(255) NOT NULL COMMENT 'Zip/Postal Code',
  `city` varchar(255) NOT NULL COMMENT 'City',
  `street` text NOT NULL COMMENT 'Street',
  `opening_hours` text NOT NULL COMMENT 'Opening Hours',
  `shipping_experiences` text NOT NULL COMMENT 'Shipping Experiences',
  `selected` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Is Selected'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Quote Collection Point Entity';

-- --------------------------------------------------------

--
-- Структура таблиці `temando_shipment`
--

CREATE TABLE `temando_shipment` (
  `entity_id` int(10) UNSIGNED NOT NULL COMMENT 'Entity Id',
  `shipment_id` int(10) UNSIGNED NOT NULL COMMENT 'Magento Shipment Id',
  `ext_shipment_id` varchar(64) NOT NULL COMMENT 'External Shipment Id',
  `ext_location_id` varchar(64) DEFAULT NULL,
  `ext_tracking_url` varchar(255) DEFAULT NULL COMMENT 'External Tracking Url',
  `ext_tracking_reference` varchar(255) DEFAULT NULL COMMENT 'External Tracking Reference',
  `ext_return_shipment_id` varchar(64) DEFAULT NULL COMMENT 'External Return Shipment Id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Temando Shipment';

-- --------------------------------------------------------

--
-- Структура таблиці `theme`
--

CREATE TABLE `theme` (
  `theme_id` int(10) UNSIGNED NOT NULL COMMENT 'Theme identifier',
  `parent_id` int(11) DEFAULT NULL COMMENT 'Parent Id',
  `theme_path` varchar(255) DEFAULT NULL COMMENT 'Theme Path',
  `theme_title` varchar(255) NOT NULL COMMENT 'Theme Title',
  `preview_image` varchar(255) DEFAULT NULL COMMENT 'Preview Image',
  `is_featured` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Is Theme Featured',
  `area` varchar(255) NOT NULL COMMENT 'Theme Area',
  `type` smallint(6) NOT NULL COMMENT 'Theme type: 0:physical, 1:virtual, 2:staging',
  `code` text COMMENT 'Full theme code, including package'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Core theme';

--
-- Дамп даних таблиці `theme`
--

INSERT INTO `theme` (`theme_id`, `parent_id`, `theme_path`, `theme_title`, `preview_image`, `is_featured`, `area`, `type`, `code`) VALUES
(1, NULL, 'Magento/blank', 'Magento Blank', 'preview_image_5b726c338733d.jpeg', 0, 'frontend', 0, 'Magento/blank'),
(2, 1, 'Magento/luma', 'Magento Luma', 'preview_image_5b726c34685af.jpeg', 0, 'frontend', 0, 'Magento/luma'),
(3, NULL, 'Magento/backend', 'Magento 2 backend', NULL, 0, 'adminhtml', 0, 'Magento/backend'),
(4, 2, 'Home/work', 'Home Work', NULL, 0, 'frontend', 0, 'Home/work');

-- --------------------------------------------------------

--
-- Структура таблиці `theme_file`
--

CREATE TABLE `theme_file` (
  `theme_files_id` int(10) UNSIGNED NOT NULL COMMENT 'Theme files identifier',
  `theme_id` int(10) UNSIGNED NOT NULL COMMENT 'Theme Id',
  `file_path` varchar(255) DEFAULT NULL COMMENT 'Relative path to file',
  `file_type` varchar(32) NOT NULL COMMENT 'File Type',
  `content` longtext NOT NULL COMMENT 'File Content',
  `sort_order` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Sort Order',
  `is_temporary` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Is Temporary File'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Core theme files';

-- --------------------------------------------------------

--
-- Структура таблиці `translation`
--

CREATE TABLE `translation` (
  `key_id` int(10) UNSIGNED NOT NULL COMMENT 'Key Id of Translation',
  `string` varchar(255) NOT NULL DEFAULT 'Translate String' COMMENT 'Translation String',
  `store_id` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `translate` varchar(255) DEFAULT NULL COMMENT 'Translate',
  `locale` varchar(20) NOT NULL DEFAULT 'en_US' COMMENT 'Locale',
  `crc_string` bigint(20) NOT NULL DEFAULT '1591228201' COMMENT 'Translation String CRC32 Hash'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Translations';

-- --------------------------------------------------------

--
-- Структура таблиці `trive_revo`
--

CREATE TABLE `trive_revo` (
  `slider_id` int(10) UNSIGNED NOT NULL COMMENT 'Slider ID',
  `title` text COMMENT 'Slider title',
  `display_title` smallint(6) DEFAULT NULL COMMENT 'Display title',
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Slider status',
  `description` text COMMENT 'Description',
  `type` text COMMENT 'Slyder type',
  `grid` smallint(6) DEFAULT NULL COMMENT 'Display items grid',
  `exclude_from_cart` smallint(6) DEFAULT NULL COMMENT 'Don''t display slider on cart page ',
  `exclude_from_checkout` smallint(6) DEFAULT NULL COMMENT 'Don''t display slider on checkout ',
  `location` text COMMENT 'Slider location',
  `start_time` datetime DEFAULT NULL COMMENT 'Slider start time',
  `end_time` datetime DEFAULT NULL COMMENT 'Slider end time',
  `navigation` smallint(6) DEFAULT NULL COMMENT 'Navigation dots',
  `infinite` tinyint(1) DEFAULT NULL COMMENT 'Infinite loop',
  `slides_to_show` smallint(6) DEFAULT NULL COMMENT 'Slides to show',
  `slides_to_scroll` smallint(6) DEFAULT NULL COMMENT 'Slides to scroll',
  `speed` smallint(6) DEFAULT NULL COMMENT 'Speed',
  `autoplay` tinyint(1) DEFAULT NULL COMMENT 'Autoplay',
  `autoplay_speed` smallint(6) DEFAULT NULL COMMENT 'Autoplay speed',
  `rtl` tinyint(1) DEFAULT NULL COMMENT 'Right to left',
  `breakpoint_large` smallint(6) DEFAULT NULL COMMENT 'Large breakpoint',
  `large_slides_to_show` smallint(6) DEFAULT NULL COMMENT 'Slides to show for large',
  `large_slides_to_scroll` smallint(6) DEFAULT NULL COMMENT 'Slides to scroll for large',
  `breakpoint_medium` smallint(6) DEFAULT NULL COMMENT 'Medium breakpoint',
  `medium_slides_to_show` smallint(6) DEFAULT NULL COMMENT 'Slides to show for medium',
  `medium_slides_to_scroll` smallint(6) DEFAULT NULL COMMENT 'Slides to scroll for Medium',
  `breakpoint_small` smallint(6) DEFAULT NULL COMMENT 'Small breakpoint',
  `small_slides_to_show` smallint(6) DEFAULT NULL COMMENT 'Slides to show for small',
  `small_slides_to_scroll` smallint(6) DEFAULT NULL COMMENT 'Slides to scroll for small',
  `display_price` smallint(5) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Display product price',
  `display_cart` smallint(5) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Display add to cart button',
  `display_wishlist` smallint(5) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Display add to wish list',
  `display_compare` smallint(5) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Display add to compare',
  `products_number` smallint(5) UNSIGNED NOT NULL COMMENT 'Number of products in slider',
  `enable_swatches` smallint(5) UNSIGNED NOT NULL COMMENT 'Enable color swatches',
  `enable_ajaxcart` smallint(5) UNSIGNED NOT NULL COMMENT 'Enable ajax add to cart'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Trive Main Product Slider Table';

--
-- Дамп даних таблиці `trive_revo`
--

INSERT INTO `trive_revo` (`slider_id`, `title`, `display_title`, `status`, `description`, `type`, `grid`, `exclude_from_cart`, `exclude_from_checkout`, `location`, `start_time`, `end_time`, `navigation`, `infinite`, `slides_to_show`, `slides_to_scroll`, `speed`, `autoplay`, `autoplay_speed`, `rtl`, `breakpoint_large`, `large_slides_to_show`, `large_slides_to_scroll`, `breakpoint_medium`, `medium_slides_to_show`, `medium_slides_to_scroll`, `breakpoint_small`, `small_slides_to_show`, `small_slides_to_scroll`, `display_price`, `display_cart`, `display_wishlist`, `display_compare`, `products_number`, `enable_swatches`, `enable_ajaxcart`) VALUES
(1, 'Slider Products', 0, 1, NULL, 'onsale', 0, 0, 0, NULL, NULL, NULL, 0, 1, 4, 2, 500, 0, 1000, 0, 1024, 3, 2, 768, 2, 1, 480, 1, 1, 1, 1, 1, 1, 10, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблиці `trive_revo_products`
--

CREATE TABLE `trive_revo_products` (
  `slider_id` int(10) UNSIGNED NOT NULL COMMENT 'Slider ID',
  `product_id` int(10) UNSIGNED NOT NULL COMMENT 'Product ID',
  `position` int(11) NOT NULL DEFAULT '0' COMMENT 'Position'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product To Slider Linkage Table';

-- --------------------------------------------------------

--
-- Структура таблиці `trive_revo_stores`
--

CREATE TABLE `trive_revo_stores` (
  `slider_id` int(10) UNSIGNED NOT NULL COMMENT 'Slider ID',
  `store_id` smallint(5) UNSIGNED NOT NULL COMMENT 'Store ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Revo store table';

--
-- Дамп даних таблиці `trive_revo_stores`
--

INSERT INTO `trive_revo_stores` (`slider_id`, `store_id`) VALUES
(1, 0);

-- --------------------------------------------------------

--
-- Структура таблиці `ui_bookmark`
--

CREATE TABLE `ui_bookmark` (
  `bookmark_id` int(10) UNSIGNED NOT NULL COMMENT 'Bookmark identifier',
  `user_id` int(10) UNSIGNED NOT NULL COMMENT 'User Id',
  `namespace` varchar(255) NOT NULL COMMENT 'Bookmark namespace',
  `identifier` varchar(255) NOT NULL COMMENT 'Bookmark Identifier',
  `current` smallint(6) NOT NULL COMMENT 'Mark current bookmark per user and identifier',
  `title` varchar(255) DEFAULT NULL COMMENT 'Bookmark title',
  `config` longtext COMMENT 'Bookmark config',
  `created_at` datetime NOT NULL COMMENT 'Bookmark created at',
  `updated_at` datetime NOT NULL COMMENT 'Bookmark updated at'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Bookmark';

--
-- Дамп даних таблиці `ui_bookmark`
--

INSERT INTO `ui_bookmark` (`bookmark_id`, `user_id`, `namespace`, `identifier`, `current`, `title`, `config`, `created_at`, `updated_at`) VALUES
(1, 1, 'design_config_listing', 'current', 0, NULL, '{"current":{"positions":{"default":0,"store_website_id":1,"store_group_id":2,"store_id":3,"theme_theme_id":4,"actions":5},"columns":{"default":{"visible":true,"sorting":false},"actions":{"visible":true,"sorting":false},"theme_theme_id":{"visible":true,"sorting":false},"store_website_id":{"visible":true,"sorting":false},"store_group_id":{"visible":true,"sorting":false},"store_id":{"visible":true,"sorting":false}},"filters":{"applied":{"placeholder":true}},"displayMode":"grid","paging":{"options":{"20":{"value":20,"label":20},"30":{"value":30,"label":30},"50":{"value":50,"label":50},"100":{"value":100,"label":100},"200":{"value":200,"label":200}},"value":20}}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 1, 'design_config_listing', 'default', 1, 'Default View', '{"views":{"default":{"label":"Default View","index":"default","editable":false,"data":{"positions":{"default":0,"store_website_id":1,"store_group_id":2,"store_id":3,"theme_theme_id":4,"actions":5},"columns":{"default":{"visible":true,"sorting":false},"actions":{"visible":true,"sorting":false},"theme_theme_id":{"visible":true,"sorting":false},"store_website_id":{"visible":true,"sorting":false},"store_group_id":{"visible":true,"sorting":false},"store_id":{"visible":true,"sorting":false}},"filters":{"applied":{"placeholder":true}},"displayMode":"grid","paging":{"options":{"20":{"value":20,"label":20},"30":{"value":30,"label":30},"50":{"value":50,"label":50},"100":{"value":100,"label":100},"200":{"value":200,"label":200}},"value":20}},"value":"Default View"}}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 1, 'cms_block_listing', 'current', 0, NULL, '{"current":{"filters":{"applied":{"placeholder":true}},"search":{"value":""},"columns":{"block_id":{"visible":true,"sorting":"asc"},"title":{"visible":true,"sorting":false},"identifier":{"visible":true,"sorting":false},"store_id":{"visible":true,"sorting":false},"ids":{"visible":true,"sorting":false},"is_active":{"visible":true,"sorting":false},"creation_time":{"visible":true,"sorting":false},"update_time":{"visible":true,"sorting":false},"actions":{"visible":true,"sorting":false}},"displayMode":"grid","positions":{"ids":0,"block_id":1,"title":2,"identifier":3,"store_id":4,"is_active":5,"creation_time":6,"update_time":7,"actions":8},"paging":{"options":{"20":{"value":20,"label":20},"30":{"value":30,"label":30},"50":{"value":50,"label":50},"100":{"value":100,"label":100},"200":{"value":200,"label":200}},"value":30}}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 1, 'cms_block_listing', 'default', 1, 'Default View', '{"views":{"default":{"label":"Default View","index":"default","editable":false,"data":{"filters":{"applied":{"placeholder":true}},"search":{"value":""},"columns":{"block_id":{"visible":true,"sorting":"asc"},"title":{"visible":true,"sorting":false},"identifier":{"visible":true,"sorting":false},"store_id":{"visible":true,"sorting":false},"ids":{"visible":true,"sorting":false},"is_active":{"visible":true,"sorting":false},"creation_time":{"visible":true,"sorting":false},"update_time":{"visible":true,"sorting":false},"actions":{"visible":true,"sorting":false}},"displayMode":"grid","positions":{"ids":0,"block_id":1,"title":2,"identifier":3,"store_id":4,"is_active":5,"creation_time":6,"update_time":7,"actions":8},"paging":{"options":{"20":{"value":20,"label":20},"30":{"value":30,"label":30},"50":{"value":50,"label":50},"100":{"value":100,"label":100},"200":{"value":200,"label":200}},"value":20}},"value":"Default View"}}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 1, 'cms_page_listing', 'default', 1, 'Default View', '{"views":{"default":{"label":"Default View","index":"default","editable":false,"data":{"search":{"value":""},"filters":{"applied":{"placeholder":true}},"columns":{"page_id":{"visible":true,"sorting":"asc"},"title":{"visible":true,"sorting":false},"identifier":{"visible":true,"sorting":false},"store_id":{"visible":true,"sorting":false},"meta_title":{"visible":false,"sorting":false},"meta_keywords":{"visible":false,"sorting":false},"meta_description":{"visible":false,"sorting":false},"ids":{"visible":true,"sorting":false},"page_layout":{"visible":true,"sorting":false},"is_active":{"visible":true,"sorting":false},"custom_theme":{"visible":false,"sorting":false},"custom_root_template":{"visible":false,"sorting":false},"creation_time":{"visible":true,"sorting":false},"update_time":{"visible":true,"sorting":false},"custom_theme_from":{"visible":false,"sorting":false},"custom_theme_to":{"visible":false,"sorting":false},"actions":{"visible":true,"sorting":false}},"displayMode":"grid","paging":{"options":{"20":{"value":20,"label":20},"30":{"value":30,"label":30},"50":{"value":50,"label":50},"100":{"value":100,"label":100},"200":{"value":200,"label":200}},"value":20},"positions":{"ids":0,"page_id":1,"title":2,"identifier":3,"page_layout":4,"store_id":5,"is_active":6,"creation_time":7,"update_time":8,"custom_theme_from":9,"custom_theme_to":10,"custom_theme":11,"custom_root_template":12,"meta_title":13,"meta_keywords":14,"meta_description":15,"actions":16}},"value":"Default View"}}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 1, 'cms_page_listing', 'current', 0, NULL, '{"current":{"search":{"value":""},"filters":{"applied":{"placeholder":true}},"columns":{"page_id":{"visible":true,"sorting":"asc"},"title":{"visible":true,"sorting":false},"identifier":{"visible":true,"sorting":false},"store_id":{"visible":true,"sorting":false},"meta_title":{"visible":false,"sorting":false},"meta_keywords":{"visible":false,"sorting":false},"meta_description":{"visible":false,"sorting":false},"ids":{"visible":true,"sorting":false},"page_layout":{"visible":true,"sorting":false},"is_active":{"visible":true,"sorting":false},"custom_theme":{"visible":false,"sorting":false},"custom_root_template":{"visible":false,"sorting":false},"creation_time":{"visible":true,"sorting":false},"update_time":{"visible":true,"sorting":false},"custom_theme_from":{"visible":false,"sorting":false},"custom_theme_to":{"visible":false,"sorting":false},"actions":{"visible":true,"sorting":false}},"displayMode":"grid","paging":{"options":{"20":{"value":20,"label":20},"30":{"value":30,"label":30},"50":{"value":50,"label":50},"100":{"value":100,"label":100},"200":{"value":200,"label":200}},"value":20},"positions":{"ids":0,"page_id":1,"title":2,"identifier":3,"page_layout":4,"store_id":5,"is_active":6,"creation_time":7,"update_time":8,"custom_theme_from":9,"custom_theme_to":10,"custom_theme":11,"custom_root_template":12,"meta_title":13,"meta_keywords":14,"meta_description":15,"actions":16}}}', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблиці `url_rewrite`
--

CREATE TABLE `url_rewrite` (
  `url_rewrite_id` int(10) UNSIGNED NOT NULL COMMENT 'Rewrite Id',
  `entity_type` varchar(32) NOT NULL COMMENT 'Entity type code',
  `entity_id` int(10) UNSIGNED NOT NULL COMMENT 'Entity ID',
  `request_path` varchar(255) DEFAULT NULL COMMENT 'Request Path',
  `target_path` varchar(255) DEFAULT NULL COMMENT 'Target Path',
  `redirect_type` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Redirect Type',
  `store_id` smallint(5) UNSIGNED NOT NULL COMMENT 'Store Id',
  `description` varchar(255) DEFAULT NULL COMMENT 'Description',
  `is_autogenerated` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Is rewrite generated automatically flag',
  `metadata` varchar(255) DEFAULT NULL COMMENT 'Meta data for url rewrite'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Url Rewrites';

--
-- Дамп даних таблиці `url_rewrite`
--

INSERT INTO `url_rewrite` (`url_rewrite_id`, `entity_type`, `entity_id`, `request_path`, `target_path`, `redirect_type`, `store_id`, `description`, `is_autogenerated`, `metadata`) VALUES
(1, 'cms-page', 1, 'no-route', 'cms/page/view/page_id/1', 0, 1, NULL, 1, NULL),
(2, 'cms-page', 2, 'home', 'cms/page/view/page_id/2', 0, 1, NULL, 1, NULL),
(3, 'cms-page', 3, 'enable-cookies', 'cms/page/view/page_id/3', 0, 1, NULL, 1, NULL),
(4, 'cms-page', 4, 'privacy-policy-cookie-restriction-mode', 'cms/page/view/page_id/4', 0, 1, NULL, 1, NULL),
(5, 'category', 3, 'gear.html', 'catalog/category/view/id/3', 0, 1, NULL, 1, NULL),
(6, 'category', 4, 'gear/bags.html', 'catalog/category/view/id/4', 0, 1, NULL, 1, NULL),
(7, 'category', 5, 'gear/fitness-equipment.html', 'catalog/category/view/id/5', 0, 1, NULL, 1, NULL),
(8, 'category', 6, 'gear/watches.html', 'catalog/category/view/id/6', 0, 1, NULL, 1, NULL),
(9, 'category', 7, 'collections.html', 'catalog/category/view/id/7', 0, 1, NULL, 1, NULL),
(10, 'category', 8, 'collections/yoga-new.html', 'catalog/category/view/id/8', 0, 1, NULL, 1, NULL),
(11, 'product', 1, 'joust-duffle-bag.html', 'catalog/product/view/id/1', 0, 1, NULL, 1, NULL),
(12, 'product', 1, 'gear/joust-duffle-bag.html', 'catalog/product/view/id/1/category/3', 0, 1, NULL, 1, '{"category_id":"3"}'),
(13, 'product', 1, 'gear/bags/joust-duffle-bag.html', 'catalog/product/view/id/1/category/4', 0, 1, NULL, 1, '{"category_id":"4"}'),
(14, 'product', 2, 'strive-shoulder-pack.html', 'catalog/product/view/id/2', 0, 1, NULL, 1, NULL),
(15, 'product', 2, 'gear/strive-shoulder-pack.html', 'catalog/product/view/id/2/category/3', 0, 1, NULL, 1, '{"category_id":"3"}'),
(16, 'product', 2, 'collections/strive-shoulder-pack.html', 'catalog/product/view/id/2/category/7', 0, 1, NULL, 1, '{"category_id":"7"}'),
(17, 'product', 2, 'gear/bags/strive-shoulder-pack.html', 'catalog/product/view/id/2/category/4', 0, 1, NULL, 1, '{"category_id":"4"}'),
(18, 'product', 3, 'crown-summit-backpack.html', 'catalog/product/view/id/3', 0, 1, NULL, 1, NULL),
(19, 'product', 3, 'gear/crown-summit-backpack.html', 'catalog/product/view/id/3/category/3', 0, 1, NULL, 1, '{"category_id":"3"}'),
(20, 'product', 3, 'gear/bags/crown-summit-backpack.html', 'catalog/product/view/id/3/category/4', 0, 1, NULL, 1, '{"category_id":"4"}'),
(21, 'product', 4, 'wayfarer-messenger-bag.html', 'catalog/product/view/id/4', 0, 1, NULL, 1, NULL),
(22, 'product', 4, 'gear/wayfarer-messenger-bag.html', 'catalog/product/view/id/4/category/3', 0, 1, NULL, 1, '{"category_id":"3"}'),
(23, 'product', 4, 'collections/wayfarer-messenger-bag.html', 'catalog/product/view/id/4/category/7', 0, 1, NULL, 1, '{"category_id":"7"}'),
(24, 'product', 4, 'gear/bags/wayfarer-messenger-bag.html', 'catalog/product/view/id/4/category/4', 0, 1, NULL, 1, '{"category_id":"4"}'),
(25, 'product', 4, 'collections/yoga-new/wayfarer-messenger-bag.html', 'catalog/product/view/id/4/category/8', 0, 1, NULL, 1, '{"category_id":"8"}'),
(26, 'product', 5, 'rival-field-messenger.html', 'catalog/product/view/id/5', 0, 1, NULL, 1, NULL),
(27, 'product', 5, 'gear/rival-field-messenger.html', 'catalog/product/view/id/5/category/3', 0, 1, NULL, 1, '{"category_id":"3"}'),
(28, 'product', 5, 'collections/rival-field-messenger.html', 'catalog/product/view/id/5/category/7', 0, 1, NULL, 1, '{"category_id":"7"}'),
(29, 'product', 5, 'gear/bags/rival-field-messenger.html', 'catalog/product/view/id/5/category/4', 0, 1, NULL, 1, '{"category_id":"4"}'),
(30, 'product', 5, 'collections/yoga-new/rival-field-messenger.html', 'catalog/product/view/id/5/category/8', 0, 1, NULL, 1, '{"category_id":"8"}'),
(31, 'product', 6, 'fusion-backpack.html', 'catalog/product/view/id/6', 0, 1, NULL, 1, NULL),
(32, 'product', 6, 'gear/fusion-backpack.html', 'catalog/product/view/id/6/category/3', 0, 1, NULL, 1, '{"category_id":"3"}'),
(33, 'product', 6, 'gear/bags/fusion-backpack.html', 'catalog/product/view/id/6/category/4', 0, 1, NULL, 1, '{"category_id":"4"}'),
(34, 'product', 7, 'impulse-duffle.html', 'catalog/product/view/id/7', 0, 1, NULL, 1, NULL),
(35, 'product', 7, 'gear/impulse-duffle.html', 'catalog/product/view/id/7/category/3', 0, 1, NULL, 1, '{"category_id":"3"}'),
(36, 'product', 7, 'gear/bags/impulse-duffle.html', 'catalog/product/view/id/7/category/4', 0, 1, NULL, 1, '{"category_id":"4"}'),
(37, 'product', 8, 'voyage-yoga-bag.html', 'catalog/product/view/id/8', 0, 1, NULL, 1, NULL),
(38, 'product', 8, 'gear/voyage-yoga-bag.html', 'catalog/product/view/id/8/category/3', 0, 1, NULL, 1, '{"category_id":"3"}'),
(39, 'product', 8, 'gear/bags/voyage-yoga-bag.html', 'catalog/product/view/id/8/category/4', 0, 1, NULL, 1, '{"category_id":"4"}'),
(40, 'product', 9, 'compete-track-tote.html', 'catalog/product/view/id/9', 0, 1, NULL, 1, NULL),
(41, 'product', 9, 'gear/compete-track-tote.html', 'catalog/product/view/id/9/category/3', 0, 1, NULL, 1, '{"category_id":"3"}'),
(42, 'product', 9, 'gear/bags/compete-track-tote.html', 'catalog/product/view/id/9/category/4', 0, 1, NULL, 1, '{"category_id":"4"}'),
(43, 'product', 10, 'savvy-shoulder-tote.html', 'catalog/product/view/id/10', 0, 1, NULL, 1, NULL),
(44, 'product', 10, 'gear/savvy-shoulder-tote.html', 'catalog/product/view/id/10/category/3', 0, 1, NULL, 1, '{"category_id":"3"}'),
(45, 'product', 10, 'collections/savvy-shoulder-tote.html', 'catalog/product/view/id/10/category/7', 0, 1, NULL, 1, '{"category_id":"7"}'),
(46, 'product', 10, 'gear/bags/savvy-shoulder-tote.html', 'catalog/product/view/id/10/category/4', 0, 1, NULL, 1, '{"category_id":"4"}'),
(47, 'product', 11, 'endeavor-daytrip-backpack.html', 'catalog/product/view/id/11', 0, 1, NULL, 1, NULL),
(48, 'product', 11, 'gear/endeavor-daytrip-backpack.html', 'catalog/product/view/id/11/category/3', 0, 1, NULL, 1, '{"category_id":"3"}'),
(49, 'product', 11, 'collections/endeavor-daytrip-backpack.html', 'catalog/product/view/id/11/category/7', 0, 1, NULL, 1, '{"category_id":"7"}'),
(50, 'product', 11, 'gear/bags/endeavor-daytrip-backpack.html', 'catalog/product/view/id/11/category/4', 0, 1, NULL, 1, '{"category_id":"4"}'),
(51, 'product', 12, 'driven-backpack.html', 'catalog/product/view/id/12', 0, 1, NULL, 1, NULL),
(52, 'product', 12, 'gear/driven-backpack.html', 'catalog/product/view/id/12/category/3', 0, 1, NULL, 1, '{"category_id":"3"}'),
(53, 'product', 12, 'gear/bags/driven-backpack.html', 'catalog/product/view/id/12/category/4', 0, 1, NULL, 1, '{"category_id":"4"}'),
(54, 'product', 13, 'overnight-duffle.html', 'catalog/product/view/id/13', 0, 1, NULL, 1, NULL),
(55, 'product', 13, 'gear/overnight-duffle.html', 'catalog/product/view/id/13/category/3', 0, 1, NULL, 1, '{"category_id":"3"}'),
(56, 'product', 13, 'collections/overnight-duffle.html', 'catalog/product/view/id/13/category/7', 0, 1, NULL, 1, '{"category_id":"7"}'),
(57, 'product', 13, 'gear/bags/overnight-duffle.html', 'catalog/product/view/id/13/category/4', 0, 1, NULL, 1, '{"category_id":"4"}'),
(58, 'product', 13, 'collections/yoga-new/overnight-duffle.html', 'catalog/product/view/id/13/category/8', 0, 1, NULL, 1, '{"category_id":"8"}'),
(59, 'product', 14, 'push-it-messenger-bag.html', 'catalog/product/view/id/14', 0, 1, NULL, 1, NULL),
(60, 'product', 14, 'gear/push-it-messenger-bag.html', 'catalog/product/view/id/14/category/3', 0, 1, NULL, 1, '{"category_id":"3"}'),
(61, 'product', 14, 'collections/push-it-messenger-bag.html', 'catalog/product/view/id/14/category/7', 0, 1, NULL, 1, '{"category_id":"7"}'),
(62, 'product', 14, 'gear/bags/push-it-messenger-bag.html', 'catalog/product/view/id/14/category/4', 0, 1, NULL, 1, '{"category_id":"4"}'),
(63, 'product', 15, 'affirm-water-bottle.html', 'catalog/product/view/id/15', 0, 1, NULL, 1, NULL),
(64, 'product', 15, 'gear/affirm-water-bottle.html', 'catalog/product/view/id/15/category/3', 0, 1, NULL, 1, '{"category_id":"3"}'),
(65, 'product', 15, 'gear/fitness-equipment/affirm-water-bottle.html', 'catalog/product/view/id/15/category/5', 0, 1, NULL, 1, '{"category_id":"5"}'),
(66, 'product', 16, 'dual-handle-cardio-ball.html', 'catalog/product/view/id/16', 0, 1, NULL, 1, NULL),
(67, 'product', 16, 'gear/dual-handle-cardio-ball.html', 'catalog/product/view/id/16/category/3', 0, 1, NULL, 1, '{"category_id":"3"}'),
(68, 'product', 16, 'collections/dual-handle-cardio-ball.html', 'catalog/product/view/id/16/category/7', 0, 1, NULL, 1, '{"category_id":"7"}'),
(69, 'product', 16, 'gear/fitness-equipment/dual-handle-cardio-ball.html', 'catalog/product/view/id/16/category/5', 0, 1, NULL, 1, '{"category_id":"5"}'),
(70, 'product', 17, 'zing-jump-rope.html', 'catalog/product/view/id/17', 0, 1, NULL, 1, NULL),
(71, 'product', 17, 'gear/zing-jump-rope.html', 'catalog/product/view/id/17/category/3', 0, 1, NULL, 1, '{"category_id":"3"}'),
(72, 'product', 17, 'gear/fitness-equipment/zing-jump-rope.html', 'catalog/product/view/id/17/category/5', 0, 1, NULL, 1, '{"category_id":"5"}'),
(73, 'product', 18, 'pursuit-lumaflex-trade-tone-band.html', 'catalog/product/view/id/18', 0, 1, NULL, 1, NULL),
(74, 'product', 18, 'gear/pursuit-lumaflex-trade-tone-band.html', 'catalog/product/view/id/18/category/3', 0, 1, NULL, 1, '{"category_id":"3"}'),
(75, 'product', 18, 'gear/fitness-equipment/pursuit-lumaflex-trade-tone-band.html', 'catalog/product/view/id/18/category/5', 0, 1, NULL, 1, '{"category_id":"5"}'),
(76, 'product', 19, 'go-get-r-pushup-grips.html', 'catalog/product/view/id/19', 0, 1, NULL, 1, NULL),
(77, 'product', 19, 'gear/go-get-r-pushup-grips.html', 'catalog/product/view/id/19/category/3', 0, 1, NULL, 1, '{"category_id":"3"}'),
(78, 'product', 19, 'collections/go-get-r-pushup-grips.html', 'catalog/product/view/id/19/category/7', 0, 1, NULL, 1, '{"category_id":"7"}'),
(79, 'product', 19, 'gear/fitness-equipment/go-get-r-pushup-grips.html', 'catalog/product/view/id/19/category/5', 0, 1, NULL, 1, '{"category_id":"5"}'),
(80, 'product', 19, 'collections/yoga-new/go-get-r-pushup-grips.html', 'catalog/product/view/id/19/category/8', 0, 1, NULL, 1, '{"category_id":"8"}'),
(81, 'product', 20, 'quest-lumaflex-trade-band.html', 'catalog/product/view/id/20', 0, 1, NULL, 1, NULL),
(82, 'product', 20, 'gear/quest-lumaflex-trade-band.html', 'catalog/product/view/id/20/category/3', 0, 1, NULL, 1, '{"category_id":"3"}'),
(83, 'product', 20, 'gear/fitness-equipment/quest-lumaflex-trade-band.html', 'catalog/product/view/id/20/category/5', 0, 1, NULL, 1, '{"category_id":"5"}'),
(84, 'product', 21, 'sprite-foam-yoga-brick.html', 'catalog/product/view/id/21', 0, 1, NULL, 1, NULL),
(85, 'product', 21, 'gear/sprite-foam-yoga-brick.html', 'catalog/product/view/id/21/category/3', 0, 1, NULL, 1, '{"category_id":"3"}'),
(86, 'product', 21, 'gear/fitness-equipment/sprite-foam-yoga-brick.html', 'catalog/product/view/id/21/category/5', 0, 1, NULL, 1, '{"category_id":"5"}'),
(87, 'product', 22, 'sprite-foam-roller.html', 'catalog/product/view/id/22', 0, 1, NULL, 1, NULL),
(88, 'product', 22, 'gear/sprite-foam-roller.html', 'catalog/product/view/id/22/category/3', 0, 1, NULL, 1, '{"category_id":"3"}'),
(89, 'product', 22, 'gear/fitness-equipment/sprite-foam-roller.html', 'catalog/product/view/id/22/category/5', 0, 1, NULL, 1, '{"category_id":"5"}'),
(90, 'product', 23, 'harmony-lumaflex-trade-strength-band-kit.html', 'catalog/product/view/id/23', 0, 1, NULL, 1, NULL),
(91, 'product', 23, 'gear/harmony-lumaflex-trade-strength-band-kit.html', 'catalog/product/view/id/23/category/3', 0, 1, NULL, 1, '{"category_id":"3"}'),
(92, 'product', 23, 'gear/fitness-equipment/harmony-lumaflex-trade-strength-band-kit.html', 'catalog/product/view/id/23/category/5', 0, 1, NULL, 1, '{"category_id":"5"}'),
(93, 'product', 36, 'aim-analog-watch.html', 'catalog/product/view/id/36', 0, 1, NULL, 1, NULL),
(94, 'product', 36, 'gear/aim-analog-watch.html', 'catalog/product/view/id/36/category/3', 0, 1, NULL, 1, '{"category_id":"3"}'),
(95, 'product', 36, 'gear/watches/aim-analog-watch.html', 'catalog/product/view/id/36/category/6', 0, 1, NULL, 1, '{"category_id":"6"}'),
(96, 'product', 37, 'endurance-watch.html', 'catalog/product/view/id/37', 0, 1, NULL, 1, NULL),
(97, 'product', 37, 'gear/endurance-watch.html', 'catalog/product/view/id/37/category/3', 0, 1, NULL, 1, '{"category_id":"3"}'),
(98, 'product', 37, 'gear/watches/endurance-watch.html', 'catalog/product/view/id/37/category/6', 0, 1, NULL, 1, '{"category_id":"6"}'),
(99, 'product', 38, 'summit-watch.html', 'catalog/product/view/id/38', 0, 1, NULL, 1, NULL),
(100, 'product', 38, 'gear/summit-watch.html', 'catalog/product/view/id/38/category/3', 0, 1, NULL, 1, '{"category_id":"3"}'),
(101, 'product', 38, 'collections/summit-watch.html', 'catalog/product/view/id/38/category/7', 0, 1, NULL, 1, '{"category_id":"7"}'),
(102, 'product', 38, 'gear/watches/summit-watch.html', 'catalog/product/view/id/38/category/6', 0, 1, NULL, 1, '{"category_id":"6"}'),
(103, 'product', 38, 'collections/yoga-new/summit-watch.html', 'catalog/product/view/id/38/category/8', 0, 1, NULL, 1, '{"category_id":"8"}'),
(104, 'product', 39, 'cruise-dual-analog-watch.html', 'catalog/product/view/id/39', 0, 1, NULL, 1, NULL),
(105, 'product', 39, 'gear/cruise-dual-analog-watch.html', 'catalog/product/view/id/39/category/3', 0, 1, NULL, 1, '{"category_id":"3"}'),
(106, 'product', 39, 'collections/cruise-dual-analog-watch.html', 'catalog/product/view/id/39/category/7', 0, 1, NULL, 1, '{"category_id":"7"}'),
(107, 'product', 39, 'gear/watches/cruise-dual-analog-watch.html', 'catalog/product/view/id/39/category/6', 0, 1, NULL, 1, '{"category_id":"6"}'),
(108, 'product', 39, 'collections/yoga-new/cruise-dual-analog-watch.html', 'catalog/product/view/id/39/category/8', 0, 1, NULL, 1, '{"category_id":"8"}'),
(109, 'product', 40, 'dash-digital-watch.html', 'catalog/product/view/id/40', 0, 1, NULL, 1, NULL),
(110, 'product', 40, 'gear/dash-digital-watch.html', 'catalog/product/view/id/40/category/3', 0, 1, NULL, 1, '{"category_id":"3"}'),
(111, 'product', 40, 'collections/dash-digital-watch.html', 'catalog/product/view/id/40/category/7', 0, 1, NULL, 1, '{"category_id":"7"}'),
(112, 'product', 40, 'gear/watches/dash-digital-watch.html', 'catalog/product/view/id/40/category/6', 0, 1, NULL, 1, '{"category_id":"6"}'),
(113, 'product', 40, 'collections/yoga-new/dash-digital-watch.html', 'catalog/product/view/id/40/category/8', 0, 1, NULL, 1, '{"category_id":"8"}'),
(114, 'product', 41, 'luma-analog-watch.html', 'catalog/product/view/id/41', 0, 1, NULL, 1, NULL),
(115, 'product', 41, 'gear/luma-analog-watch.html', 'catalog/product/view/id/41/category/3', 0, 1, NULL, 1, '{"category_id":"3"}'),
(116, 'product', 41, 'gear/watches/luma-analog-watch.html', 'catalog/product/view/id/41/category/6', 0, 1, NULL, 1, '{"category_id":"6"}'),
(117, 'product', 42, 'bolo-sport-watch.html', 'catalog/product/view/id/42', 0, 1, NULL, 1, NULL),
(118, 'product', 42, 'gear/bolo-sport-watch.html', 'catalog/product/view/id/42/category/3', 0, 1, NULL, 1, '{"category_id":"3"}'),
(119, 'product', 42, 'gear/watches/bolo-sport-watch.html', 'catalog/product/view/id/42/category/6', 0, 1, NULL, 1, '{"category_id":"6"}'),
(120, 'product', 43, 'clamber-watch.html', 'catalog/product/view/id/43', 0, 1, NULL, 1, NULL),
(121, 'product', 43, 'gear/clamber-watch.html', 'catalog/product/view/id/43/category/3', 0, 1, NULL, 1, '{"category_id":"3"}'),
(122, 'product', 43, 'gear/watches/clamber-watch.html', 'catalog/product/view/id/43/category/6', 0, 1, NULL, 1, '{"category_id":"6"}'),
(123, 'product', 44, 'didi-sport-watch.html', 'catalog/product/view/id/44', 0, 1, NULL, 1, NULL),
(124, 'product', 44, 'gear/didi-sport-watch.html', 'catalog/product/view/id/44/category/3', 0, 1, NULL, 1, '{"category_id":"3"}'),
(125, 'product', 44, 'collections/didi-sport-watch.html', 'catalog/product/view/id/44/category/7', 0, 1, NULL, 1, '{"category_id":"7"}'),
(126, 'product', 44, 'gear/watches/didi-sport-watch.html', 'catalog/product/view/id/44/category/6', 0, 1, NULL, 1, '{"category_id":"6"}'),
(127, 'product', 44, 'collections/yoga-new/didi-sport-watch.html', 'catalog/product/view/id/44/category/8', 0, 1, NULL, 1, '{"category_id":"8"}'),
(128, 'product', 45, 'sprite-yoga-companion-kit.html', 'catalog/product/view/id/45', 0, 1, NULL, 1, NULL),
(129, 'product', 45, 'gear/sprite-yoga-companion-kit.html', 'catalog/product/view/id/45/category/3', 0, 1, NULL, 1, '{"category_id":"3"}'),
(130, 'product', 45, 'gear/fitness-equipment/sprite-yoga-companion-kit.html', 'catalog/product/view/id/45/category/5', 0, 1, NULL, 1, '{"category_id":"5"}'),
(131, 'category', 9, 'training.html', 'catalog/category/view/id/9', 0, 1, NULL, 1, NULL),
(132, 'category', 10, 'training/training-video.html', 'catalog/category/view/id/10', 0, 1, NULL, 1, NULL),
(133, 'product', 46, 'beginner-s-yoga.html', 'catalog/product/view/id/46', 0, 1, NULL, 1, NULL),
(134, 'product', 46, 'training/beginner-s-yoga.html', 'catalog/product/view/id/46/category/9', 0, 1, NULL, 1, '{"category_id":"9"}'),
(135, 'product', 46, 'training/training-video/beginner-s-yoga.html', 'catalog/product/view/id/46/category/10', 0, 1, NULL, 1, '{"category_id":"10"}'),
(136, 'product', 47, 'lifelong-fitness-iv.html', 'catalog/product/view/id/47', 0, 1, NULL, 1, NULL),
(137, 'product', 47, 'training/lifelong-fitness-iv.html', 'catalog/product/view/id/47/category/9', 0, 1, NULL, 1, '{"category_id":"9"}'),
(138, 'product', 47, 'training/training-video/lifelong-fitness-iv.html', 'catalog/product/view/id/47/category/10', 0, 1, NULL, 1, '{"category_id":"10"}'),
(139, 'product', 48, 'yoga-adventure.html', 'catalog/product/view/id/48', 0, 1, NULL, 1, NULL),
(140, 'product', 48, 'training/yoga-adventure.html', 'catalog/product/view/id/48/category/9', 0, 1, NULL, 1, '{"category_id":"9"}'),
(141, 'product', 48, 'training/training-video/yoga-adventure.html', 'catalog/product/view/id/48/category/10', 0, 1, NULL, 1, '{"category_id":"10"}'),
(142, 'product', 49, 'solo-power-circuit.html', 'catalog/product/view/id/49', 0, 1, NULL, 1, NULL),
(143, 'product', 49, 'training/solo-power-circuit.html', 'catalog/product/view/id/49/category/9', 0, 1, NULL, 1, '{"category_id":"9"}'),
(144, 'product', 49, 'training/training-video/solo-power-circuit.html', 'catalog/product/view/id/49/category/10', 0, 1, NULL, 1, '{"category_id":"10"}'),
(145, 'product', 50, 'advanced-pilates-yoga-strength.html', 'catalog/product/view/id/50', 0, 1, NULL, 1, NULL),
(146, 'product', 50, 'training/advanced-pilates-yoga-strength.html', 'catalog/product/view/id/50/category/9', 0, 1, NULL, 1, '{"category_id":"9"}'),
(147, 'product', 50, 'training/training-video/advanced-pilates-yoga-strength.html', 'catalog/product/view/id/50/category/10', 0, 1, NULL, 1, '{"category_id":"10"}'),
(148, 'product', 51, 'luma-yoga-for-life.html', 'catalog/product/view/id/51', 0, 1, NULL, 1, NULL),
(149, 'product', 51, 'training/luma-yoga-for-life.html', 'catalog/product/view/id/51/category/9', 0, 1, NULL, 1, '{"category_id":"9"}'),
(150, 'product', 51, 'training/training-video/luma-yoga-for-life.html', 'catalog/product/view/id/51/category/10', 0, 1, NULL, 1, '{"category_id":"10"}'),
(151, 'category', 11, 'men.html', 'catalog/category/view/id/11', 0, 1, NULL, 1, NULL),
(152, 'category', 12, 'men/tops-men.html', 'catalog/category/view/id/12', 0, 1, NULL, 1, NULL),
(153, 'category', 13, 'men/bottoms-men.html', 'catalog/category/view/id/13', 0, 1, NULL, 1, NULL),
(154, 'category', 14, 'men/tops-men/jackets-men.html', 'catalog/category/view/id/14', 0, 1, NULL, 1, NULL),
(155, 'category', 15, 'men/tops-men/hoodies-and-sweatshirts-men.html', 'catalog/category/view/id/15', 0, 1, NULL, 1, NULL),
(156, 'category', 16, 'men/tops-men/tees-men.html', 'catalog/category/view/id/16', 0, 1, NULL, 1, NULL),
(157, 'category', 17, 'men/tops-men/tanks-men.html', 'catalog/category/view/id/17', 0, 1, NULL, 1, NULL),
(158, 'category', 18, 'men/bottoms-men/pants-men.html', 'catalog/category/view/id/18', 0, 1, NULL, 1, NULL),
(159, 'category', 19, 'men/bottoms-men/shorts-men.html', 'catalog/category/view/id/19', 0, 1, NULL, 1, NULL),
(160, 'category', 20, 'women.html', 'catalog/category/view/id/20', 0, 1, NULL, 1, NULL),
(161, 'category', 21, 'women/tops-women.html', 'catalog/category/view/id/21', 0, 1, NULL, 1, NULL),
(162, 'category', 22, 'women/bottoms-women.html', 'catalog/category/view/id/22', 0, 1, NULL, 1, NULL),
(163, 'category', 23, 'women/tops-women/jackets-women.html', 'catalog/category/view/id/23', 0, 1, NULL, 1, NULL),
(164, 'category', 24, 'women/tops-women/hoodies-and-sweatshirts-women.html', 'catalog/category/view/id/24', 0, 1, NULL, 1, NULL),
(165, 'category', 25, 'women/tops-women/tees-women.html', 'catalog/category/view/id/25', 0, 1, NULL, 1, NULL),
(166, 'category', 26, 'women/tops-women/tanks-women.html', 'catalog/category/view/id/26', 0, 1, NULL, 1, NULL),
(167, 'category', 27, 'women/bottoms-women/pants-women.html', 'catalog/category/view/id/27', 0, 1, NULL, 1, NULL),
(168, 'category', 28, 'women/bottoms-women/shorts-women.html', 'catalog/category/view/id/28', 0, 1, NULL, 1, NULL),
(169, 'category', 29, 'promotions.html', 'catalog/category/view/id/29', 0, 1, NULL, 1, NULL),
(170, 'category', 30, 'promotions/women-sale.html', 'catalog/category/view/id/30', 0, 1, NULL, 1, NULL),
(171, 'category', 31, 'promotions/men-sale.html', 'catalog/category/view/id/31', 0, 1, NULL, 1, NULL),
(172, 'category', 32, 'promotions/pants-all.html', 'catalog/category/view/id/32', 0, 1, NULL, 1, NULL),
(173, 'category', 33, 'promotions/tees-all.html', 'catalog/category/view/id/33', 0, 1, NULL, 1, NULL),
(174, 'category', 34, 'collections/erin-recommends.html', 'catalog/category/view/id/34', 0, 1, NULL, 1, NULL),
(175, 'category', 35, 'collections/performance-fabrics.html', 'catalog/category/view/id/35', 0, 1, NULL, 1, NULL),
(176, 'category', 36, 'collections/eco-friendly.html', 'catalog/category/view/id/36', 0, 1, NULL, 1, NULL),
(177, 'product', 67, 'chaz-kangeroo-hoodie.html', 'catalog/product/view/id/67', 0, 1, NULL, 1, NULL),
(178, 'product', 83, 'teton-pullover-hoodie.html', 'catalog/product/view/id/83', 0, 1, NULL, 1, NULL),
(179, 'product', 99, 'bruno-compete-hoodie.html', 'catalog/product/view/id/99', 0, 1, NULL, 1, NULL),
(180, 'product', 115, 'frankie-sweatshirt.html', 'catalog/product/view/id/115', 0, 1, NULL, 1, NULL),
(181, 'product', 131, 'hollister-backyard-sweatshirt.html', 'catalog/product/view/id/131', 0, 1, NULL, 1, NULL),
(182, 'product', 147, 'stark-fundamental-hoodie.html', 'catalog/product/view/id/147', 0, 1, NULL, 1, NULL),
(183, 'product', 67, 'men/tops-men/hoodies-and-sweatshirts-men/chaz-kangeroo-hoodie.html', 'catalog/product/view/id/67/category/15', 0, 1, NULL, 1, '{"category_id":"15"}'),
(184, 'product', 67, 'collections/eco-friendly/chaz-kangeroo-hoodie.html', 'catalog/product/view/id/67/category/36', 0, 1, NULL, 1, '{"category_id":"36"}'),
(185, 'product', 83, 'men/tops-men/hoodies-and-sweatshirts-men/teton-pullover-hoodie.html', 'catalog/product/view/id/83/category/15', 0, 1, NULL, 1, '{"category_id":"15"}'),
(186, 'product', 99, 'men/tops-men/hoodies-and-sweatshirts-men/bruno-compete-hoodie.html', 'catalog/product/view/id/99/category/15', 0, 1, NULL, 1, '{"category_id":"15"}'),
(187, 'product', 99, 'collections/eco-friendly/bruno-compete-hoodie.html', 'catalog/product/view/id/99/category/36', 0, 1, NULL, 1, '{"category_id":"36"}'),
(188, 'product', 115, 'men/tops-men/hoodies-and-sweatshirts-men/frankie-sweatshirt.html', 'catalog/product/view/id/115/category/15', 0, 1, NULL, 1, '{"category_id":"15"}'),
(189, 'product', 115, 'collections/eco-friendly/frankie-sweatshirt.html', 'catalog/product/view/id/115/category/36', 0, 1, NULL, 1, '{"category_id":"36"}'),
(190, 'product', 131, 'men/tops-men/hoodies-and-sweatshirts-men/hollister-backyard-sweatshirt.html', 'catalog/product/view/id/131/category/15', 0, 1, NULL, 1, '{"category_id":"15"}'),
(191, 'product', 147, 'men/tops-men/hoodies-and-sweatshirts-men/stark-fundamental-hoodie.html', 'catalog/product/view/id/147/category/15', 0, 1, NULL, 1, '{"category_id":"15"}'),
(192, 'product', 163, 'hero-hoodie.html', 'catalog/product/view/id/163', 0, 1, NULL, 1, NULL),
(193, 'product', 179, 'oslo-trek-hoodie.html', 'catalog/product/view/id/179', 0, 1, NULL, 1, NULL),
(194, 'product', 195, 'abominable-hoodie.html', 'catalog/product/view/id/195', 0, 1, NULL, 1, NULL),
(195, 'product', 211, 'mach-street-sweatshirt.html', 'catalog/product/view/id/211', 0, 1, NULL, 1, NULL),
(196, 'product', 227, 'grayson-crewneck-sweatshirt.html', 'catalog/product/view/id/227', 0, 1, NULL, 1, NULL),
(197, 'product', 243, 'ajax-full-zip-sweatshirt.html', 'catalog/product/view/id/243', 0, 1, NULL, 1, NULL),
(198, 'product', 163, 'men/tops-men/hoodies-and-sweatshirts-men/hero-hoodie.html', 'catalog/product/view/id/163/category/15', 0, 1, NULL, 1, '{"category_id":"15"}'),
(199, 'product', 179, 'men/tops-men/hoodies-and-sweatshirts-men/oslo-trek-hoodie.html', 'catalog/product/view/id/179/category/15', 0, 1, NULL, 1, '{"category_id":"15"}'),
(200, 'product', 195, 'men/tops-men/hoodies-and-sweatshirts-men/abominable-hoodie.html', 'catalog/product/view/id/195/category/15', 0, 1, NULL, 1, '{"category_id":"15"}'),
(201, 'product', 211, 'men/tops-men/hoodies-and-sweatshirts-men/mach-street-sweatshirt.html', 'catalog/product/view/id/211/category/15', 0, 1, NULL, 1, '{"category_id":"15"}'),
(202, 'product', 227, 'men/tops-men/hoodies-and-sweatshirts-men/grayson-crewneck-sweatshirt.html', 'catalog/product/view/id/227/category/15', 0, 1, NULL, 1, '{"category_id":"15"}'),
(203, 'product', 243, 'men/tops-men/hoodies-and-sweatshirts-men/ajax-full-zip-sweatshirt.html', 'catalog/product/view/id/243/category/15', 0, 1, NULL, 1, '{"category_id":"15"}'),
(204, 'product', 259, 'marco-lightweight-active-hoodie.html', 'catalog/product/view/id/259', 0, 1, NULL, 1, NULL),
(205, 'product', 275, 'beaumont-summit-kit.html', 'catalog/product/view/id/275', 0, 1, NULL, 1, NULL),
(206, 'product', 291, 'hyperion-elements-jacket.html', 'catalog/product/view/id/291', 0, 1, NULL, 1, NULL),
(207, 'product', 307, 'kenobi-trail-jacket.html', 'catalog/product/view/id/307', 0, 1, NULL, 1, NULL),
(208, 'product', 323, 'orion-two-tone-fitted-jacket.html', 'catalog/product/view/id/323', 0, 1, NULL, 1, NULL),
(209, 'product', 339, 'lando-gym-jacket.html', 'catalog/product/view/id/339', 0, 1, NULL, 1, NULL),
(210, 'product', 259, 'men/tops-men/hoodies-and-sweatshirts-men/marco-lightweight-active-hoodie.html', 'catalog/product/view/id/259/category/15', 0, 1, NULL, 1, '{"category_id":"15"}'),
(211, 'product', 275, 'men/tops-men/jackets-men/beaumont-summit-kit.html', 'catalog/product/view/id/275/category/14', 0, 1, NULL, 1, '{"category_id":"14"}'),
(212, 'product', 291, 'men/tops-men/jackets-men/hyperion-elements-jacket.html', 'catalog/product/view/id/291/category/14', 0, 1, NULL, 1, '{"category_id":"14"}'),
(213, 'product', 307, 'men/tops-men/jackets-men/kenobi-trail-jacket.html', 'catalog/product/view/id/307/category/14', 0, 1, NULL, 1, '{"category_id":"14"}'),
(214, 'product', 307, 'collections/eco-friendly/kenobi-trail-jacket.html', 'catalog/product/view/id/307/category/36', 0, 1, NULL, 1, '{"category_id":"36"}'),
(215, 'product', 323, 'men/tops-men/jackets-men/orion-two-tone-fitted-jacket.html', 'catalog/product/view/id/323/category/14', 0, 1, NULL, 1, '{"category_id":"14"}'),
(216, 'product', 339, 'men/tops-men/jackets-men/lando-gym-jacket.html', 'catalog/product/view/id/339/category/14', 0, 1, NULL, 1, '{"category_id":"14"}'),
(217, 'product', 355, 'taurus-elements-shell.html', 'catalog/product/view/id/355', 0, 1, NULL, 1, NULL),
(218, 'product', 371, 'mars-heattech-trade-pullover.html', 'catalog/product/view/id/371', 0, 1, NULL, 1, NULL),
(219, 'product', 387, 'typhon-performance-fleece-lined-jacket.html', 'catalog/product/view/id/387', 0, 1, NULL, 1, NULL),
(220, 'product', 403, 'jupiter-all-weather-trainer.html', 'catalog/product/view/id/403', 0, 1, NULL, 1, NULL),
(221, 'product', 419, 'montana-wind-jacket.html', 'catalog/product/view/id/419', 0, 1, NULL, 1, NULL),
(222, 'product', 435, 'proteus-fitness-jackshirt.html', 'catalog/product/view/id/435', 0, 1, NULL, 1, NULL),
(223, 'product', 451, 'gobi-heattec-reg-tee.html', 'catalog/product/view/id/451', 0, 1, NULL, 1, NULL),
(224, 'product', 355, 'men/tops-men/jackets-men/taurus-elements-shell.html', 'catalog/product/view/id/355/category/14', 0, 1, NULL, 1, '{"category_id":"14"}'),
(225, 'product', 371, 'men/tops-men/jackets-men/mars-heattech-trade-pullover.html', 'catalog/product/view/id/371/category/14', 0, 1, NULL, 1, '{"category_id":"14"}'),
(226, 'product', 387, 'men/tops-men/jackets-men/typhon-performance-fleece-lined-jacket.html', 'catalog/product/view/id/387/category/14', 0, 1, NULL, 1, '{"category_id":"14"}'),
(227, 'product', 403, 'men/tops-men/jackets-men/jupiter-all-weather-trainer.html', 'catalog/product/view/id/403/category/14', 0, 1, NULL, 1, '{"category_id":"14"}'),
(228, 'product', 403, 'collections/eco-friendly/jupiter-all-weather-trainer.html', 'catalog/product/view/id/403/category/36', 0, 1, NULL, 1, '{"category_id":"36"}'),
(229, 'product', 419, 'men/tops-men/jackets-men/montana-wind-jacket.html', 'catalog/product/view/id/419/category/14', 0, 1, NULL, 1, '{"category_id":"14"}'),
(230, 'product', 435, 'men/tops-men/jackets-men/proteus-fitness-jackshirt.html', 'catalog/product/view/id/435/category/14', 0, 1, NULL, 1, '{"category_id":"14"}'),
(231, 'product', 451, 'men/tops-men/tees-men/gobi-heattec-reg-tee.html', 'catalog/product/view/id/451/category/16', 0, 1, NULL, 1, '{"category_id":"16"}'),
(232, 'product', 467, 'helios-evercool-trade-tee.html', 'catalog/product/view/id/467', 0, 1, NULL, 1, NULL),
(233, 'product', 483, 'ryker-lumatech-trade-tee-crew-neck.html', 'catalog/product/view/id/483', 0, 1, NULL, 1, NULL),
(234, 'product', 499, 'atomic-endurance-running-tee-v-neck.html', 'catalog/product/view/id/499', 0, 1, NULL, 1, NULL),
(235, 'product', 515, 'atomic-endurance-running-tee-crew-neck.html', 'catalog/product/view/id/515', 0, 1, NULL, 1, NULL),
(236, 'product', 531, 'balboa-persistence-tee.html', 'catalog/product/view/id/531', 0, 1, NULL, 1, NULL),
(237, 'product', 547, 'zoltan-gym-tee.html', 'catalog/product/view/id/547', 0, 1, NULL, 1, NULL),
(238, 'product', 467, 'men/tops-men/tees-men/helios-evercool-trade-tee.html', 'catalog/product/view/id/467/category/16', 0, 1, NULL, 1, '{"category_id":"16"}'),
(239, 'product', 467, 'collections/eco-friendly/helios-evercool-trade-tee.html', 'catalog/product/view/id/467/category/36', 0, 1, NULL, 1, '{"category_id":"36"}'),
(240, 'product', 483, 'men/tops-men/tees-men/ryker-lumatech-trade-tee-crew-neck.html', 'catalog/product/view/id/483/category/16', 0, 1, NULL, 1, '{"category_id":"16"}'),
(241, 'product', 499, 'men/tops-men/tees-men/atomic-endurance-running-tee-v-neck.html', 'catalog/product/view/id/499/category/16', 0, 1, NULL, 1, '{"category_id":"16"}'),
(242, 'product', 515, 'men/tops-men/tees-men/atomic-endurance-running-tee-crew-neck.html', 'catalog/product/view/id/515/category/16', 0, 1, NULL, 1, '{"category_id":"16"}'),
(243, 'product', 531, 'men/tops-men/tees-men/balboa-persistence-tee.html', 'catalog/product/view/id/531/category/16', 0, 1, NULL, 1, '{"category_id":"16"}'),
(244, 'product', 547, 'men/tops-men/tees-men/zoltan-gym-tee.html', 'catalog/product/view/id/547/category/16', 0, 1, NULL, 1, '{"category_id":"16"}'),
(245, 'product', 563, 'aero-daily-fitness-tee.html', 'catalog/product/view/id/563', 0, 1, NULL, 1, NULL),
(246, 'product', 579, 'ryker-lumatech-trade-tee-v-neck.html', 'catalog/product/view/id/579', 0, 1, NULL, 1, NULL),
(247, 'product', 595, 'logan-heattec-reg-tee.html', 'catalog/product/view/id/595', 0, 1, NULL, 1, NULL),
(248, 'product', 611, 'deion-long-sleeve-evercool-trade-tee.html', 'catalog/product/view/id/611', 0, 1, NULL, 1, NULL),
(249, 'product', 627, 'strike-endurance-tee.html', 'catalog/product/view/id/627', 0, 1, NULL, 1, NULL),
(250, 'product', 643, 'erikssen-cooltech-trade-fitness-tank.html', 'catalog/product/view/id/643', 0, 1, NULL, 1, NULL),
(251, 'product', 563, 'men/tops-men/tees-men/aero-daily-fitness-tee.html', 'catalog/product/view/id/563/category/16', 0, 1, NULL, 1, '{"category_id":"16"}'),
(252, 'product', 579, 'men/tops-men/tees-men/ryker-lumatech-trade-tee-v-neck.html', 'catalog/product/view/id/579/category/16', 0, 1, NULL, 1, '{"category_id":"16"}'),
(253, 'product', 595, 'men/tops-men/tees-men/logan-heattec-reg-tee.html', 'catalog/product/view/id/595/category/16', 0, 1, NULL, 1, '{"category_id":"16"}'),
(254, 'product', 611, 'men/tops-men/tees-men/deion-long-sleeve-evercool-trade-tee.html', 'catalog/product/view/id/611/category/16', 0, 1, NULL, 1, '{"category_id":"16"}'),
(255, 'product', 627, 'men/tops-men/tees-men/strike-endurance-tee.html', 'catalog/product/view/id/627/category/16', 0, 1, NULL, 1, '{"category_id":"16"}'),
(256, 'product', 643, 'men/tops-men/tanks-men/erikssen-cooltech-trade-fitness-tank.html', 'catalog/product/view/id/643/category/17', 0, 1, NULL, 1, '{"category_id":"17"}'),
(257, 'product', 659, 'tristan-endurance-tank.html', 'catalog/product/view/id/659', 0, 1, NULL, 1, NULL),
(258, 'product', 675, 'primo-endurance-tank.html', 'catalog/product/view/id/675', 0, 1, NULL, 1, NULL),
(259, 'product', 681, 'helios-endurance-tank.html', 'catalog/product/view/id/681', 0, 1, NULL, 1, NULL),
(260, 'product', 687, 'rocco-gym-tank.html', 'catalog/product/view/id/687', 0, 1, NULL, 1, NULL),
(261, 'product', 693, 'vulcan-weightlifting-tank.html', 'catalog/product/view/id/693', 0, 1, NULL, 1, NULL),
(262, 'product', 699, 'argus-all-weather-tank.html', 'catalog/product/view/id/699', 0, 1, NULL, 1, NULL),
(263, 'product', 705, 'sparta-gym-tank.html', 'catalog/product/view/id/705', 0, 1, NULL, 1, NULL),
(264, 'product', 711, 'sinbad-fitness-tank.html', 'catalog/product/view/id/711', 0, 1, NULL, 1, NULL),
(265, 'product', 717, 'tiberius-gym-tank.html', 'catalog/product/view/id/717', 0, 1, NULL, 1, NULL),
(266, 'product', 723, 'atlas-fitness-tank.html', 'catalog/product/view/id/723', 0, 1, NULL, 1, NULL),
(267, 'product', 729, 'cassius-sparring-tank.html', 'catalog/product/view/id/729', 0, 1, NULL, 1, NULL),
(268, 'product', 742, 'caesar-warm-up-pant.html', 'catalog/product/view/id/742', 0, 1, NULL, 1, NULL),
(269, 'product', 659, 'men/tops-men/tanks-men/tristan-endurance-tank.html', 'catalog/product/view/id/659/category/17', 0, 1, NULL, 1, '{"category_id":"17"}'),
(270, 'product', 675, 'men/tops-men/tanks-men/primo-endurance-tank.html', 'catalog/product/view/id/675/category/17', 0, 1, NULL, 1, '{"category_id":"17"}'),
(271, 'product', 681, 'men/tops-men/tanks-men/helios-endurance-tank.html', 'catalog/product/view/id/681/category/17', 0, 1, NULL, 1, '{"category_id":"17"}'),
(272, 'product', 687, 'men/tops-men/tanks-men/rocco-gym-tank.html', 'catalog/product/view/id/687/category/17', 0, 1, NULL, 1, '{"category_id":"17"}'),
(273, 'product', 693, 'men/tops-men/tanks-men/vulcan-weightlifting-tank.html', 'catalog/product/view/id/693/category/17', 0, 1, NULL, 1, '{"category_id":"17"}'),
(274, 'product', 699, 'men/tops-men/tanks-men/argus-all-weather-tank.html', 'catalog/product/view/id/699/category/17', 0, 1, NULL, 1, '{"category_id":"17"}'),
(275, 'product', 699, 'collections/eco-friendly/argus-all-weather-tank.html', 'catalog/product/view/id/699/category/36', 0, 1, NULL, 1, '{"category_id":"36"}'),
(276, 'product', 705, 'men/tops-men/tanks-men/sparta-gym-tank.html', 'catalog/product/view/id/705/category/17', 0, 1, NULL, 1, '{"category_id":"17"}'),
(277, 'product', 711, 'men/tops-men/tanks-men/sinbad-fitness-tank.html', 'catalog/product/view/id/711/category/17', 0, 1, NULL, 1, '{"category_id":"17"}'),
(278, 'product', 717, 'men/tops-men/tanks-men/tiberius-gym-tank.html', 'catalog/product/view/id/717/category/17', 0, 1, NULL, 1, '{"category_id":"17"}'),
(279, 'product', 723, 'men/tops-men/tanks-men/atlas-fitness-tank.html', 'catalog/product/view/id/723/category/17', 0, 1, NULL, 1, '{"category_id":"17"}'),
(280, 'product', 723, 'collections/eco-friendly/atlas-fitness-tank.html', 'catalog/product/view/id/723/category/36', 0, 1, NULL, 1, '{"category_id":"36"}'),
(281, 'product', 729, 'men/tops-men/tanks-men/cassius-sparring-tank.html', 'catalog/product/view/id/729/category/17', 0, 1, NULL, 1, '{"category_id":"17"}'),
(282, 'product', 742, 'men/bottoms-men/pants-men/caesar-warm-up-pant.html', 'catalog/product/view/id/742/category/18', 0, 1, NULL, 1, '{"category_id":"18"}'),
(283, 'product', 742, 'promotions/pants-all/caesar-warm-up-pant.html', 'catalog/product/view/id/742/category/32', 0, 1, NULL, 1, '{"category_id":"32"}'),
(284, 'product', 755, 'viktor-lumatech-trade-pant.html', 'catalog/product/view/id/755', 0, 1, NULL, 1, NULL),
(285, 'product', 768, 'geo-insulated-jogging-pant.html', 'catalog/product/view/id/768', 0, 1, NULL, 1, NULL),
(286, 'product', 781, 'supernova-sport-pant.html', 'catalog/product/view/id/781', 0, 1, NULL, 1, NULL),
(287, 'product', 794, 'kratos-gym-pant.html', 'catalog/product/view/id/794', 0, 1, NULL, 1, NULL),
(288, 'product', 807, 'mithra-warmup-pant.html', 'catalog/product/view/id/807', 0, 1, NULL, 1, NULL),
(289, 'product', 820, 'thorpe-track-pant.html', 'catalog/product/view/id/820', 0, 1, NULL, 1, NULL),
(290, 'product', 833, 'zeppelin-yoga-pant.html', 'catalog/product/view/id/833', 0, 1, NULL, 1, NULL),
(291, 'product', 846, 'livingston-all-purpose-tight.html', 'catalog/product/view/id/846', 0, 1, NULL, 1, NULL),
(292, 'product', 755, 'men/bottoms-men/pants-men/viktor-lumatech-trade-pant.html', 'catalog/product/view/id/755/category/18', 0, 1, NULL, 1, '{"category_id":"18"}'),
(293, 'product', 755, 'promotions/pants-all/viktor-lumatech-trade-pant.html', 'catalog/product/view/id/755/category/32', 0, 1, NULL, 1, '{"category_id":"32"}'),
(294, 'product', 755, 'collections/performance-fabrics/viktor-lumatech-trade-pant.html', 'catalog/product/view/id/755/category/35', 0, 1, NULL, 1, '{"category_id":"35"}'),
(295, 'product', 768, 'men/bottoms-men/pants-men/geo-insulated-jogging-pant.html', 'catalog/product/view/id/768/category/18', 0, 1, NULL, 1, '{"category_id":"18"}'),
(296, 'product', 768, 'promotions/pants-all/geo-insulated-jogging-pant.html', 'catalog/product/view/id/768/category/32', 0, 1, NULL, 1, '{"category_id":"32"}'),
(297, 'product', 768, 'collections/yoga-new/geo-insulated-jogging-pant.html', 'catalog/product/view/id/768/category/8', 0, 1, NULL, 1, '{"category_id":"8"}'),
(298, 'product', 781, 'men/bottoms-men/pants-men/supernova-sport-pant.html', 'catalog/product/view/id/781/category/18', 0, 1, NULL, 1, '{"category_id":"18"}'),
(299, 'product', 781, 'promotions/pants-all/supernova-sport-pant.html', 'catalog/product/view/id/781/category/32', 0, 1, NULL, 1, '{"category_id":"32"}'),
(300, 'product', 794, 'men/bottoms-men/pants-men/kratos-gym-pant.html', 'catalog/product/view/id/794/category/18', 0, 1, NULL, 1, '{"category_id":"18"}'),
(301, 'product', 794, 'promotions/pants-all/kratos-gym-pant.html', 'catalog/product/view/id/794/category/32', 0, 1, NULL, 1, '{"category_id":"32"}'),
(302, 'product', 794, 'collections/yoga-new/kratos-gym-pant.html', 'catalog/product/view/id/794/category/8', 0, 1, NULL, 1, '{"category_id":"8"}'),
(303, 'product', 807, 'men/bottoms-men/pants-men/mithra-warmup-pant.html', 'catalog/product/view/id/807/category/18', 0, 1, NULL, 1, '{"category_id":"18"}'),
(304, 'product', 807, 'promotions/pants-all/mithra-warmup-pant.html', 'catalog/product/view/id/807/category/32', 0, 1, NULL, 1, '{"category_id":"32"}'),
(305, 'product', 807, 'collections/eco-friendly/mithra-warmup-pant.html', 'catalog/product/view/id/807/category/36', 0, 1, NULL, 1, '{"category_id":"36"}'),
(306, 'product', 820, 'men/bottoms-men/pants-men/thorpe-track-pant.html', 'catalog/product/view/id/820/category/18', 0, 1, NULL, 1, '{"category_id":"18"}'),
(307, 'product', 820, 'promotions/pants-all/thorpe-track-pant.html', 'catalog/product/view/id/820/category/32', 0, 1, NULL, 1, '{"category_id":"32"}'),
(308, 'product', 820, 'collections/yoga-new/thorpe-track-pant.html', 'catalog/product/view/id/820/category/8', 0, 1, NULL, 1, '{"category_id":"8"}'),
(309, 'product', 833, 'men/bottoms-men/pants-men/zeppelin-yoga-pant.html', 'catalog/product/view/id/833/category/18', 0, 1, NULL, 1, '{"category_id":"18"}'),
(310, 'product', 833, 'promotions/pants-all/zeppelin-yoga-pant.html', 'catalog/product/view/id/833/category/32', 0, 1, NULL, 1, '{"category_id":"32"}'),
(311, 'product', 833, 'collections/yoga-new/zeppelin-yoga-pant.html', 'catalog/product/view/id/833/category/8', 0, 1, NULL, 1, '{"category_id":"8"}'),
(312, 'product', 846, 'men/bottoms-men/pants-men/livingston-all-purpose-tight.html', 'catalog/product/view/id/846/category/18', 0, 1, NULL, 1, '{"category_id":"18"}'),
(313, 'product', 846, 'promotions/pants-all/livingston-all-purpose-tight.html', 'catalog/product/view/id/846/category/32', 0, 1, NULL, 1, '{"category_id":"32"}'),
(314, 'product', 846, 'collections/yoga-new/livingston-all-purpose-tight.html', 'catalog/product/view/id/846/category/8', 0, 1, NULL, 1, '{"category_id":"8"}'),
(315, 'product', 846, 'collections/erin-recommends/livingston-all-purpose-tight.html', 'catalog/product/view/id/846/category/34', 0, 1, NULL, 1, '{"category_id":"34"}'),
(316, 'product', 859, 'orestes-yoga-pant.html', 'catalog/product/view/id/859', 0, 1, NULL, 1, NULL),
(317, 'product', 872, 'aether-gym-pant.html', 'catalog/product/view/id/872', 0, 1, NULL, 1, NULL),
(318, 'product', 885, 'cronus-yoga-pant.html', 'catalog/product/view/id/885', 0, 1, NULL, 1, NULL),
(319, 'product', 898, 'cobalt-cooltech-trade-fitness-short.html', 'catalog/product/view/id/898', 0, 1, NULL, 1, NULL),
(320, 'product', 903, 'apollo-running-short.html', 'catalog/product/view/id/903', 0, 1, NULL, 1, NULL),
(321, 'product', 916, 'meteor-workout-short.html', 'catalog/product/view/id/916', 0, 1, NULL, 1, NULL),
(322, 'product', 929, 'torque-power-short.html', 'catalog/product/view/id/929', 0, 1, NULL, 1, NULL),
(323, 'product', 942, 'hawkeye-yoga-short.html', 'catalog/product/view/id/942', 0, 1, NULL, 1, NULL),
(324, 'product', 859, 'men/bottoms-men/pants-men/orestes-yoga-pant.html', 'catalog/product/view/id/859/category/18', 0, 1, NULL, 1, '{"category_id":"18"}'),
(325, 'product', 859, 'promotions/pants-all/orestes-yoga-pant.html', 'catalog/product/view/id/859/category/32', 0, 1, NULL, 1, '{"category_id":"32"}'),
(326, 'product', 859, 'collections/erin-recommends/orestes-yoga-pant.html', 'catalog/product/view/id/859/category/34', 0, 1, NULL, 1, '{"category_id":"34"}'),
(327, 'product', 872, 'men/bottoms-men/pants-men/aether-gym-pant.html', 'catalog/product/view/id/872/category/18', 0, 1, NULL, 1, '{"category_id":"18"}'),
(328, 'product', 872, 'promotions/pants-all/aether-gym-pant.html', 'catalog/product/view/id/872/category/32', 0, 1, NULL, 1, '{"category_id":"32"}'),
(329, 'product', 872, 'collections/erin-recommends/aether-gym-pant.html', 'catalog/product/view/id/872/category/34', 0, 1, NULL, 1, '{"category_id":"34"}'),
(330, 'product', 885, 'men/bottoms-men/pants-men/cronus-yoga-pant.html', 'catalog/product/view/id/885/category/18', 0, 1, NULL, 1, '{"category_id":"18"}'),
(331, 'product', 885, 'promotions/pants-all/cronus-yoga-pant.html', 'catalog/product/view/id/885/category/32', 0, 1, NULL, 1, '{"category_id":"32"}'),
(332, 'product', 898, 'men/bottoms-men/shorts-men/cobalt-cooltech-trade-fitness-short.html', 'catalog/product/view/id/898/category/19', 0, 1, NULL, 1, '{"category_id":"19"}'),
(333, 'product', 898, 'collections/erin-recommends/cobalt-cooltech-trade-fitness-short.html', 'catalog/product/view/id/898/category/34', 0, 1, NULL, 1, '{"category_id":"34"}'),
(334, 'product', 903, 'men/bottoms-men/shorts-men/apollo-running-short.html', 'catalog/product/view/id/903/category/19', 0, 1, NULL, 1, '{"category_id":"19"}'),
(335, 'product', 916, 'men/bottoms-men/shorts-men/meteor-workout-short.html', 'catalog/product/view/id/916/category/19', 0, 1, NULL, 1, '{"category_id":"19"}'),
(336, 'product', 916, 'collections/performance-fabrics/meteor-workout-short.html', 'catalog/product/view/id/916/category/35', 0, 1, NULL, 1, '{"category_id":"35"}'),
(337, 'product', 929, 'men/bottoms-men/shorts-men/torque-power-short.html', 'catalog/product/view/id/929/category/19', 0, 1, NULL, 1, '{"category_id":"19"}'),
(338, 'product', 942, 'men/bottoms-men/shorts-men/hawkeye-yoga-short.html', 'catalog/product/view/id/942/category/19', 0, 1, NULL, 1, '{"category_id":"19"}'),
(339, 'product', 942, 'collections/performance-fabrics/hawkeye-yoga-short.html', 'catalog/product/view/id/942/category/35', 0, 1, NULL, 1, '{"category_id":"35"}'),
(340, 'product', 955, 'lono-yoga-short.html', 'catalog/product/view/id/955', 0, 1, NULL, 1, NULL),
(341, 'product', 968, 'rapha-sports-short.html', 'catalog/product/view/id/968', 0, 1, NULL, 1, NULL),
(342, 'product', 981, 'orestes-fitness-short.html', 'catalog/product/view/id/981', 0, 1, NULL, 1, NULL),
(343, 'product', 994, 'troy-yoga-short.html', 'catalog/product/view/id/994', 0, 1, NULL, 1, NULL),
(344, 'product', 1007, 'sol-active-short.html', 'catalog/product/view/id/1007', 0, 1, NULL, 1, NULL),
(345, 'product', 1020, 'arcadio-gym-short.html', 'catalog/product/view/id/1020', 0, 1, NULL, 1, NULL),
(346, 'product', 1033, 'pierce-gym-short.html', 'catalog/product/view/id/1033', 0, 1, NULL, 1, NULL),
(347, 'product', 1049, 'mona-pullover-hoodlie.html', 'catalog/product/view/id/1049', 0, 1, NULL, 1, NULL),
(348, 'product', 955, 'men/bottoms-men/shorts-men/lono-yoga-short.html', 'catalog/product/view/id/955/category/19', 0, 1, NULL, 1, '{"category_id":"19"}'),
(349, 'product', 955, 'collections/yoga-new/lono-yoga-short.html', 'catalog/product/view/id/955/category/8', 0, 1, NULL, 1, '{"category_id":"8"}'),
(350, 'product', 968, 'men/bottoms-men/shorts-men/rapha-sports-short.html', 'catalog/product/view/id/968/category/19', 0, 1, NULL, 1, '{"category_id":"19"}'),
(351, 'product', 968, 'promotions/men-sale/rapha-sports-short.html', 'catalog/product/view/id/968/category/31', 0, 1, NULL, 1, '{"category_id":"31"}'),
(352, 'product', 968, 'collections/erin-recommends/rapha-sports-short.html', 'catalog/product/view/id/968/category/34', 0, 1, NULL, 1, '{"category_id":"34"}'),
(353, 'product', 981, 'men/bottoms-men/shorts-men/orestes-fitness-short.html', 'catalog/product/view/id/981/category/19', 0, 1, NULL, 1, '{"category_id":"19"}'),
(354, 'product', 981, 'promotions/men-sale/orestes-fitness-short.html', 'catalog/product/view/id/981/category/31', 0, 1, NULL, 1, '{"category_id":"31"}'),
(355, 'product', 994, 'men/bottoms-men/shorts-men/troy-yoga-short.html', 'catalog/product/view/id/994/category/19', 0, 1, NULL, 1, '{"category_id":"19"}'),
(356, 'product', 994, 'collections/performance-fabrics/troy-yoga-short.html', 'catalog/product/view/id/994/category/35', 0, 1, NULL, 1, '{"category_id":"35"}'),
(357, 'product', 1007, 'men/bottoms-men/shorts-men/sol-active-short.html', 'catalog/product/view/id/1007/category/19', 0, 1, NULL, 1, '{"category_id":"19"}'),
(358, 'product', 1007, 'collections/yoga-new/sol-active-short.html', 'catalog/product/view/id/1007/category/8', 0, 1, NULL, 1, '{"category_id":"8"}'),
(359, 'product', 1020, 'men/bottoms-men/shorts-men/arcadio-gym-short.html', 'catalog/product/view/id/1020/category/19', 0, 1, NULL, 1, '{"category_id":"19"}'),
(360, 'product', 1033, 'men/bottoms-men/shorts-men/pierce-gym-short.html', 'catalog/product/view/id/1033/category/19', 0, 1, NULL, 1, '{"category_id":"19"}'),
(361, 'product', 1033, 'promotions/men-sale/pierce-gym-short.html', 'catalog/product/view/id/1033/category/31', 0, 1, NULL, 1, '{"category_id":"31"}'),
(362, 'product', 1049, 'women/tops-women/hoodies-and-sweatshirts-women/mona-pullover-hoodlie.html', 'catalog/product/view/id/1049/category/24', 0, 1, NULL, 1, '{"category_id":"24"}'),
(363, 'product', 1049, 'collections/yoga-new/mona-pullover-hoodlie.html', 'catalog/product/view/id/1049/category/8', 0, 1, NULL, 1, '{"category_id":"8"}'),
(364, 'product', 1049, 'collections/erin-recommends/mona-pullover-hoodlie.html', 'catalog/product/view/id/1049/category/34', 0, 1, NULL, 1, '{"category_id":"34"}'),
(365, 'product', 1065, 'hera-pullover-hoodie.html', 'catalog/product/view/id/1065', 0, 1, NULL, 1, NULL),
(366, 'product', 1081, 'autumn-pullie.html', 'catalog/product/view/id/1081', 0, 1, NULL, 1, NULL),
(367, 'product', 1097, 'miko-pullover-hoodie.html', 'catalog/product/view/id/1097', 0, 1, NULL, 1, NULL),
(368, 'product', 1113, 'selene-yoga-hoodie.html', 'catalog/product/view/id/1113', 0, 1, NULL, 1, NULL),
(369, 'product', 1119, 'daphne-full-zip-hoodie.html', 'catalog/product/view/id/1119', 0, 1, NULL, 1, NULL),
(370, 'product', 1135, 'phoebe-zipper-sweatshirt.html', 'catalog/product/view/id/1135', 0, 1, NULL, 1, NULL),
(371, 'product', 1151, 'cassia-funnel-sweatshirt.html', 'catalog/product/view/id/1151', 0, 1, NULL, 1, NULL),
(372, 'product', 1065, 'women/tops-women/hoodies-and-sweatshirts-women/hera-pullover-hoodie.html', 'catalog/product/view/id/1065/category/24', 0, 1, NULL, 1, '{"category_id":"24"}'),
(373, 'product', 1065, 'promotions/women-sale/hera-pullover-hoodie.html', 'catalog/product/view/id/1065/category/30', 0, 1, NULL, 1, '{"category_id":"30"}'),
(374, 'product', 1081, 'women/tops-women/hoodies-and-sweatshirts-women/autumn-pullie.html', 'catalog/product/view/id/1081/category/24', 0, 1, NULL, 1, '{"category_id":"24"}'),
(375, 'product', 1097, 'women/tops-women/hoodies-and-sweatshirts-women/miko-pullover-hoodie.html', 'catalog/product/view/id/1097/category/24', 0, 1, NULL, 1, '{"category_id":"24"}'),
(376, 'product', 1113, 'women/tops-women/hoodies-and-sweatshirts-women/selene-yoga-hoodie.html', 'catalog/product/view/id/1113/category/24', 0, 1, NULL, 1, '{"category_id":"24"}'),
(377, 'product', 1119, 'women/tops-women/hoodies-and-sweatshirts-women/daphne-full-zip-hoodie.html', 'catalog/product/view/id/1119/category/24', 0, 1, NULL, 1, '{"category_id":"24"}'),
(378, 'product', 1135, 'women/tops-women/hoodies-and-sweatshirts-women/phoebe-zipper-sweatshirt.html', 'catalog/product/view/id/1135/category/24', 0, 1, NULL, 1, '{"category_id":"24"}'),
(379, 'product', 1135, 'collections/yoga-new/phoebe-zipper-sweatshirt.html', 'catalog/product/view/id/1135/category/8', 0, 1, NULL, 1, '{"category_id":"8"}'),
(380, 'product', 1135, 'collections/erin-recommends/phoebe-zipper-sweatshirt.html', 'catalog/product/view/id/1135/category/34', 0, 1, NULL, 1, '{"category_id":"34"}'),
(381, 'product', 1151, 'women/tops-women/hoodies-and-sweatshirts-women/cassia-funnel-sweatshirt.html', 'catalog/product/view/id/1151/category/24', 0, 1, NULL, 1, '{"category_id":"24"}');
INSERT INTO `url_rewrite` (`url_rewrite_id`, `entity_type`, `entity_id`, `request_path`, `target_path`, `redirect_type`, `store_id`, `description`, `is_autogenerated`, `metadata`) VALUES
(382, 'product', 1151, 'promotions/women-sale/cassia-funnel-sweatshirt.html', 'catalog/product/view/id/1151/category/30', 0, 1, NULL, 1, '{"category_id":"30"}'),
(383, 'product', 1151, 'collections/performance-fabrics/cassia-funnel-sweatshirt.html', 'catalog/product/view/id/1151/category/35', 0, 1, NULL, 1, '{"category_id":"35"}'),
(384, 'product', 1167, 'ariel-roll-sleeve-sweatshirt.html', 'catalog/product/view/id/1167', 0, 1, NULL, 1, NULL),
(385, 'product', 1183, 'helena-hooded-fleece.html', 'catalog/product/view/id/1183', 0, 1, NULL, 1, NULL),
(386, 'product', 1199, 'eos-v-neck-hoodie.html', 'catalog/product/view/id/1199', 0, 1, NULL, 1, NULL),
(387, 'product', 1215, 'circe-hooded-ice-fleece.html', 'catalog/product/view/id/1215', 0, 1, NULL, 1, NULL),
(388, 'product', 1225, 'stellar-solar-jacket.html', 'catalog/product/view/id/1225', 0, 1, NULL, 1, NULL),
(389, 'product', 1241, 'josie-yoga-jacket.html', 'catalog/product/view/id/1241', 0, 1, NULL, 1, NULL),
(390, 'product', 1167, 'women/tops-women/hoodies-and-sweatshirts-women/ariel-roll-sleeve-sweatshirt.html', 'catalog/product/view/id/1167/category/24', 0, 1, NULL, 1, '{"category_id":"24"}'),
(391, 'product', 1167, 'collections/eco-friendly/ariel-roll-sleeve-sweatshirt.html', 'catalog/product/view/id/1167/category/36', 0, 1, NULL, 1, '{"category_id":"36"}'),
(392, 'product', 1183, 'women/tops-women/hoodies-and-sweatshirts-women/helena-hooded-fleece.html', 'catalog/product/view/id/1183/category/24', 0, 1, NULL, 1, '{"category_id":"24"}'),
(393, 'product', 1183, 'collections/yoga-new/helena-hooded-fleece.html', 'catalog/product/view/id/1183/category/8', 0, 1, NULL, 1, '{"category_id":"8"}'),
(394, 'product', 1199, 'women/tops-women/hoodies-and-sweatshirts-women/eos-v-neck-hoodie.html', 'catalog/product/view/id/1199/category/24', 0, 1, NULL, 1, '{"category_id":"24"}'),
(395, 'product', 1199, 'collections/erin-recommends/eos-v-neck-hoodie.html', 'catalog/product/view/id/1199/category/34', 0, 1, NULL, 1, '{"category_id":"34"}'),
(396, 'product', 1215, 'women/tops-women/hoodies-and-sweatshirts-women/circe-hooded-ice-fleece.html', 'catalog/product/view/id/1215/category/24', 0, 1, NULL, 1, '{"category_id":"24"}'),
(397, 'product', 1215, 'collections/performance-fabrics/circe-hooded-ice-fleece.html', 'catalog/product/view/id/1215/category/35', 0, 1, NULL, 1, '{"category_id":"35"}'),
(398, 'product', 1225, 'women/tops-women/jackets-women/stellar-solar-jacket.html', 'catalog/product/view/id/1225/category/23', 0, 1, NULL, 1, '{"category_id":"23"}'),
(399, 'product', 1225, 'collections/yoga-new/stellar-solar-jacket.html', 'catalog/product/view/id/1225/category/8', 0, 1, NULL, 1, '{"category_id":"8"}'),
(400, 'product', 1225, 'collections/erin-recommends/stellar-solar-jacket.html', 'catalog/product/view/id/1225/category/34', 0, 1, NULL, 1, '{"category_id":"34"}'),
(401, 'product', 1241, 'women/tops-women/jackets-women/josie-yoga-jacket.html', 'catalog/product/view/id/1241/category/23', 0, 1, NULL, 1, '{"category_id":"23"}'),
(402, 'product', 1241, 'promotions/women-sale/josie-yoga-jacket.html', 'catalog/product/view/id/1241/category/30', 0, 1, NULL, 1, '{"category_id":"30"}'),
(403, 'product', 1257, 'augusta-pullover-jacket.html', 'catalog/product/view/id/1257', 0, 1, NULL, 1, NULL),
(404, 'product', 1273, 'ingrid-running-jacket.html', 'catalog/product/view/id/1273', 0, 1, NULL, 1, NULL),
(405, 'product', 1289, 'riona-full-zip-jacket.html', 'catalog/product/view/id/1289', 0, 1, NULL, 1, NULL),
(406, 'product', 1305, 'inez-full-zip-jacket.html', 'catalog/product/view/id/1305', 0, 1, NULL, 1, NULL),
(407, 'product', 1321, 'adrienne-trek-jacket.html', 'catalog/product/view/id/1321', 0, 1, NULL, 1, NULL),
(408, 'product', 1337, 'jade-yoga-jacket.html', 'catalog/product/view/id/1337', 0, 1, NULL, 1, NULL),
(409, 'product', 1257, 'women/tops-women/jackets-women/augusta-pullover-jacket.html', 'catalog/product/view/id/1257/category/23', 0, 1, NULL, 1, '{"category_id":"23"}'),
(410, 'product', 1273, 'women/tops-women/jackets-women/ingrid-running-jacket.html', 'catalog/product/view/id/1273/category/23', 0, 1, NULL, 1, '{"category_id":"23"}'),
(411, 'product', 1273, 'collections/yoga-new/ingrid-running-jacket.html', 'catalog/product/view/id/1273/category/8', 0, 1, NULL, 1, '{"category_id":"8"}'),
(412, 'product', 1273, 'collections/performance-fabrics/ingrid-running-jacket.html', 'catalog/product/view/id/1273/category/35', 0, 1, NULL, 1, '{"category_id":"35"}'),
(413, 'product', 1289, 'women/tops-women/jackets-women/riona-full-zip-jacket.html', 'catalog/product/view/id/1289/category/23', 0, 1, NULL, 1, '{"category_id":"23"}'),
(414, 'product', 1305, 'women/tops-women/jackets-women/inez-full-zip-jacket.html', 'catalog/product/view/id/1305/category/23', 0, 1, NULL, 1, '{"category_id":"23"}'),
(415, 'product', 1321, 'women/tops-women/jackets-women/adrienne-trek-jacket.html', 'catalog/product/view/id/1321/category/23', 0, 1, NULL, 1, '{"category_id":"23"}'),
(416, 'product', 1321, 'promotions/women-sale/adrienne-trek-jacket.html', 'catalog/product/view/id/1321/category/30', 0, 1, NULL, 1, '{"category_id":"30"}'),
(417, 'product', 1321, 'collections/erin-recommends/adrienne-trek-jacket.html', 'catalog/product/view/id/1321/category/34', 0, 1, NULL, 1, '{"category_id":"34"}'),
(418, 'product', 1337, 'women/tops-women/jackets-women/jade-yoga-jacket.html', 'catalog/product/view/id/1337/category/23', 0, 1, NULL, 1, '{"category_id":"23"}'),
(419, 'product', 1337, 'collections/erin-recommends/jade-yoga-jacket.html', 'catalog/product/view/id/1337/category/34', 0, 1, NULL, 1, '{"category_id":"34"}'),
(420, 'product', 1353, 'nadia-elements-shell.html', 'catalog/product/view/id/1353', 0, 1, NULL, 1, NULL),
(421, 'product', 1369, 'neve-studio-dance-jacket.html', 'catalog/product/view/id/1369', 0, 1, NULL, 1, NULL),
(422, 'product', 1385, 'juno-jacket.html', 'catalog/product/view/id/1385', 0, 1, NULL, 1, NULL),
(423, 'product', 1401, 'olivia-1-4-zip-light-jacket.html', 'catalog/product/view/id/1401', 0, 1, NULL, 1, NULL),
(424, 'product', 1417, 'gabrielle-micro-sleeve-top.html', 'catalog/product/view/id/1417', 0, 1, NULL, 1, NULL),
(425, 'product', 1433, 'iris-workout-top.html', 'catalog/product/view/id/1433', 0, 1, NULL, 1, NULL),
(426, 'product', 1449, 'layla-tee.html', 'catalog/product/view/id/1449', 0, 1, NULL, 1, NULL),
(427, 'product', 1353, 'women/tops-women/jackets-women/nadia-elements-shell.html', 'catalog/product/view/id/1353/category/23', 0, 1, NULL, 1, '{"category_id":"23"}'),
(428, 'product', 1353, 'collections/yoga-new/nadia-elements-shell.html', 'catalog/product/view/id/1353/category/8', 0, 1, NULL, 1, '{"category_id":"8"}'),
(429, 'product', 1353, 'collections/performance-fabrics/nadia-elements-shell.html', 'catalog/product/view/id/1353/category/35', 0, 1, NULL, 1, '{"category_id":"35"}'),
(430, 'product', 1369, 'women/tops-women/jackets-women/neve-studio-dance-jacket.html', 'catalog/product/view/id/1369/category/23', 0, 1, NULL, 1, '{"category_id":"23"}'),
(431, 'product', 1369, 'collections/yoga-new/neve-studio-dance-jacket.html', 'catalog/product/view/id/1369/category/8', 0, 1, NULL, 1, '{"category_id":"8"}'),
(432, 'product', 1385, 'women/tops-women/jackets-women/juno-jacket.html', 'catalog/product/view/id/1385/category/23', 0, 1, NULL, 1, '{"category_id":"23"}'),
(433, 'product', 1385, 'promotions/women-sale/juno-jacket.html', 'catalog/product/view/id/1385/category/30', 0, 1, NULL, 1, '{"category_id":"30"}'),
(434, 'product', 1385, 'collections/performance-fabrics/juno-jacket.html', 'catalog/product/view/id/1385/category/35', 0, 1, NULL, 1, '{"category_id":"35"}'),
(435, 'product', 1401, 'women/tops-women/jackets-women/olivia-1-4-zip-light-jacket.html', 'catalog/product/view/id/1401/category/23', 0, 1, NULL, 1, '{"category_id":"23"}'),
(436, 'product', 1401, 'promotions/women-sale/olivia-1-4-zip-light-jacket.html', 'catalog/product/view/id/1401/category/30', 0, 1, NULL, 1, '{"category_id":"30"}'),
(437, 'product', 1401, 'collections/performance-fabrics/olivia-1-4-zip-light-jacket.html', 'catalog/product/view/id/1401/category/35', 0, 1, NULL, 1, '{"category_id":"35"}'),
(438, 'product', 1417, 'women/tops-women/tees-women/gabrielle-micro-sleeve-top.html', 'catalog/product/view/id/1417/category/25', 0, 1, NULL, 1, '{"category_id":"25"}'),
(439, 'product', 1417, 'promotions/tees-all/gabrielle-micro-sleeve-top.html', 'catalog/product/view/id/1417/category/33', 0, 1, NULL, 1, '{"category_id":"33"}'),
(440, 'product', 1417, 'collections/yoga-new/gabrielle-micro-sleeve-top.html', 'catalog/product/view/id/1417/category/8', 0, 1, NULL, 1, '{"category_id":"8"}'),
(441, 'product', 1433, 'women/tops-women/tees-women/iris-workout-top.html', 'catalog/product/view/id/1433/category/25', 0, 1, NULL, 1, '{"category_id":"25"}'),
(442, 'product', 1433, 'promotions/tees-all/iris-workout-top.html', 'catalog/product/view/id/1433/category/33', 0, 1, NULL, 1, '{"category_id":"33"}'),
(443, 'product', 1433, 'collections/performance-fabrics/iris-workout-top.html', 'catalog/product/view/id/1433/category/35', 0, 1, NULL, 1, '{"category_id":"35"}'),
(444, 'product', 1449, 'women/tops-women/tees-women/layla-tee.html', 'catalog/product/view/id/1449/category/25', 0, 1, NULL, 1, '{"category_id":"25"}'),
(445, 'product', 1449, 'promotions/tees-all/layla-tee.html', 'catalog/product/view/id/1449/category/33', 0, 1, NULL, 1, '{"category_id":"33"}'),
(446, 'product', 1449, 'collections/yoga-new/layla-tee.html', 'catalog/product/view/id/1449/category/8', 0, 1, NULL, 1, '{"category_id":"8"}'),
(447, 'product', 1449, 'collections/eco-friendly/layla-tee.html', 'catalog/product/view/id/1449/category/36', 0, 1, NULL, 1, '{"category_id":"36"}'),
(448, 'product', 1465, 'elisa-evercool-trade-tee.html', 'catalog/product/view/id/1465', 0, 1, NULL, 1, NULL),
(449, 'product', 1481, 'juliana-short-sleeve-tee.html', 'catalog/product/view/id/1481', 0, 1, NULL, 1, NULL),
(450, 'product', 1497, 'minerva-lumatech-trade-v-tee.html', 'catalog/product/view/id/1497', 0, 1, NULL, 1, NULL),
(451, 'product', 1513, 'tiffany-fitness-tee.html', 'catalog/product/view/id/1513', 0, 1, NULL, 1, NULL),
(452, 'product', 1529, 'karissa-v-neck-tee.html', 'catalog/product/view/id/1529', 0, 1, NULL, 1, NULL),
(453, 'product', 1545, 'diva-gym-tee.html', 'catalog/product/view/id/1545', 0, 1, NULL, 1, NULL),
(454, 'product', 1465, 'women/tops-women/tees-women/elisa-evercool-trade-tee.html', 'catalog/product/view/id/1465/category/25', 0, 1, NULL, 1, '{"category_id":"25"}'),
(455, 'product', 1465, 'promotions/tees-all/elisa-evercool-trade-tee.html', 'catalog/product/view/id/1465/category/33', 0, 1, NULL, 1, '{"category_id":"33"}'),
(456, 'product', 1465, 'collections/yoga-new/elisa-evercool-trade-tee.html', 'catalog/product/view/id/1465/category/8', 0, 1, NULL, 1, '{"category_id":"8"}'),
(457, 'product', 1465, 'collections/eco-friendly/elisa-evercool-trade-tee.html', 'catalog/product/view/id/1465/category/36', 0, 1, NULL, 1, '{"category_id":"36"}'),
(458, 'product', 1481, 'women/tops-women/tees-women/juliana-short-sleeve-tee.html', 'catalog/product/view/id/1481/category/25', 0, 1, NULL, 1, '{"category_id":"25"}'),
(459, 'product', 1481, 'promotions/tees-all/juliana-short-sleeve-tee.html', 'catalog/product/view/id/1481/category/33', 0, 1, NULL, 1, '{"category_id":"33"}'),
(460, 'product', 1481, 'collections/yoga-new/juliana-short-sleeve-tee.html', 'catalog/product/view/id/1481/category/8', 0, 1, NULL, 1, '{"category_id":"8"}'),
(461, 'product', 1481, 'collections/erin-recommends/juliana-short-sleeve-tee.html', 'catalog/product/view/id/1481/category/34', 0, 1, NULL, 1, '{"category_id":"34"}'),
(462, 'product', 1497, 'women/tops-women/tees-women/minerva-lumatech-trade-v-tee.html', 'catalog/product/view/id/1497/category/25', 0, 1, NULL, 1, '{"category_id":"25"}'),
(463, 'product', 1497, 'promotions/tees-all/minerva-lumatech-trade-v-tee.html', 'catalog/product/view/id/1497/category/33', 0, 1, NULL, 1, '{"category_id":"33"}'),
(464, 'product', 1497, 'promotions/women-sale/minerva-lumatech-trade-v-tee.html', 'catalog/product/view/id/1497/category/30', 0, 1, NULL, 1, '{"category_id":"30"}'),
(465, 'product', 1513, 'women/tops-women/tees-women/tiffany-fitness-tee.html', 'catalog/product/view/id/1513/category/25', 0, 1, NULL, 1, '{"category_id":"25"}'),
(466, 'product', 1513, 'promotions/tees-all/tiffany-fitness-tee.html', 'catalog/product/view/id/1513/category/33', 0, 1, NULL, 1, '{"category_id":"33"}'),
(467, 'product', 1513, 'promotions/women-sale/tiffany-fitness-tee.html', 'catalog/product/view/id/1513/category/30', 0, 1, NULL, 1, '{"category_id":"30"}'),
(468, 'product', 1513, 'collections/eco-friendly/tiffany-fitness-tee.html', 'catalog/product/view/id/1513/category/36', 0, 1, NULL, 1, '{"category_id":"36"}'),
(469, 'product', 1529, 'women/tops-women/tees-women/karissa-v-neck-tee.html', 'catalog/product/view/id/1529/category/25', 0, 1, NULL, 1, '{"category_id":"25"}'),
(470, 'product', 1529, 'promotions/tees-all/karissa-v-neck-tee.html', 'catalog/product/view/id/1529/category/33', 0, 1, NULL, 1, '{"category_id":"33"}'),
(471, 'product', 1529, 'collections/performance-fabrics/karissa-v-neck-tee.html', 'catalog/product/view/id/1529/category/35', 0, 1, NULL, 1, '{"category_id":"35"}'),
(472, 'product', 1545, 'women/tops-women/tees-women/diva-gym-tee.html', 'catalog/product/view/id/1545/category/25', 0, 1, NULL, 1, '{"category_id":"25"}'),
(473, 'product', 1545, 'promotions/tees-all/diva-gym-tee.html', 'catalog/product/view/id/1545/category/33', 0, 1, NULL, 1, '{"category_id":"33"}'),
(474, 'product', 1545, 'promotions/women-sale/diva-gym-tee.html', 'catalog/product/view/id/1545/category/30', 0, 1, NULL, 1, '{"category_id":"30"}'),
(475, 'product', 1545, 'collections/erin-recommends/diva-gym-tee.html', 'catalog/product/view/id/1545/category/34', 0, 1, NULL, 1, '{"category_id":"34"}'),
(476, 'product', 1561, 'radiant-tee.html', 'catalog/product/view/id/1561', 0, 1, NULL, 1, NULL),
(477, 'product', 1577, 'gwyn-endurance-tee.html', 'catalog/product/view/id/1577', 0, 1, NULL, 1, NULL),
(478, 'product', 1593, 'desiree-fitness-tee.html', 'catalog/product/view/id/1593', 0, 1, NULL, 1, NULL),
(479, 'product', 1609, 'electra-bra-top.html', 'catalog/product/view/id/1609', 0, 1, NULL, 1, NULL),
(480, 'product', 1625, 'erica-evercool-sports-bra.html', 'catalog/product/view/id/1625', 0, 1, NULL, 1, NULL),
(481, 'product', 1641, 'celeste-sports-bra.html', 'catalog/product/view/id/1641', 0, 1, NULL, 1, NULL),
(482, 'product', 1561, 'women/tops-women/tees-women/radiant-tee.html', 'catalog/product/view/id/1561/category/25', 0, 1, NULL, 1, '{"category_id":"25"}'),
(483, 'product', 1561, 'promotions/tees-all/radiant-tee.html', 'catalog/product/view/id/1561/category/33', 0, 1, NULL, 1, '{"category_id":"33"}'),
(484, 'product', 1561, 'collections/performance-fabrics/radiant-tee.html', 'catalog/product/view/id/1561/category/35', 0, 1, NULL, 1, '{"category_id":"35"}'),
(485, 'product', 1577, 'women/tops-women/tees-women/gwyn-endurance-tee.html', 'catalog/product/view/id/1577/category/25', 0, 1, NULL, 1, '{"category_id":"25"}'),
(486, 'product', 1577, 'promotions/tees-all/gwyn-endurance-tee.html', 'catalog/product/view/id/1577/category/33', 0, 1, NULL, 1, '{"category_id":"33"}'),
(487, 'product', 1593, 'women/tops-women/tees-women/desiree-fitness-tee.html', 'catalog/product/view/id/1593/category/25', 0, 1, NULL, 1, '{"category_id":"25"}'),
(488, 'product', 1593, 'promotions/tees-all/desiree-fitness-tee.html', 'catalog/product/view/id/1593/category/33', 0, 1, NULL, 1, '{"category_id":"33"}'),
(489, 'product', 1609, 'women/tops-women/tanks-women/electra-bra-top.html', 'catalog/product/view/id/1609/category/26', 0, 1, NULL, 1, '{"category_id":"26"}'),
(490, 'product', 1609, 'promotions/women-sale/electra-bra-top.html', 'catalog/product/view/id/1609/category/30', 0, 1, NULL, 1, '{"category_id":"30"}'),
(491, 'product', 1609, 'collections/eco-friendly/electra-bra-top.html', 'catalog/product/view/id/1609/category/36', 0, 1, NULL, 1, '{"category_id":"36"}'),
(492, 'product', 1625, 'women/tops-women/tanks-women/erica-evercool-sports-bra.html', 'catalog/product/view/id/1625/category/26', 0, 1, NULL, 1, '{"category_id":"26"}'),
(493, 'product', 1641, 'women/tops-women/tanks-women/celeste-sports-bra.html', 'catalog/product/view/id/1641/category/26', 0, 1, NULL, 1, '{"category_id":"26"}'),
(494, 'product', 1641, 'collections/yoga-new/celeste-sports-bra.html', 'catalog/product/view/id/1641/category/8', 0, 1, NULL, 1, '{"category_id":"8"}'),
(495, 'product', 1641, 'collections/performance-fabrics/celeste-sports-bra.html', 'catalog/product/view/id/1641/category/35', 0, 1, NULL, 1, '{"category_id":"35"}'),
(496, 'product', 1657, 'prima-compete-bra-top.html', 'catalog/product/view/id/1657', 0, 1, NULL, 1, NULL),
(497, 'product', 1673, 'lucia-cross-fit-bra.html', 'catalog/product/view/id/1673', 0, 1, NULL, 1, NULL),
(498, 'product', 1689, 'bella-tank.html', 'catalog/product/view/id/1689', 0, 1, NULL, 1, NULL),
(499, 'product', 1705, 'zoe-tank.html', 'catalog/product/view/id/1705', 0, 1, NULL, 1, NULL),
(500, 'product', 1721, 'nora-practice-tank.html', 'catalog/product/view/id/1721', 0, 1, NULL, 1, NULL),
(501, 'product', 1737, 'nona-fitness-tank.html', 'catalog/product/view/id/1737', 0, 1, NULL, 1, NULL),
(502, 'product', 1657, 'women/tops-women/tanks-women/prima-compete-bra-top.html', 'catalog/product/view/id/1657/category/26', 0, 1, NULL, 1, '{"category_id":"26"}'),
(503, 'product', 1673, 'women/tops-women/tanks-women/lucia-cross-fit-bra.html', 'catalog/product/view/id/1673/category/26', 0, 1, NULL, 1, '{"category_id":"26"}'),
(504, 'product', 1689, 'women/tops-women/tanks-women/bella-tank.html', 'catalog/product/view/id/1689/category/26', 0, 1, NULL, 1, '{"category_id":"26"}'),
(505, 'product', 1689, 'collections/eco-friendly/bella-tank.html', 'catalog/product/view/id/1689/category/36', 0, 1, NULL, 1, '{"category_id":"36"}'),
(506, 'product', 1705, 'women/tops-women/tanks-women/zoe-tank.html', 'catalog/product/view/id/1705/category/26', 0, 1, NULL, 1, '{"category_id":"26"}'),
(507, 'product', 1721, 'women/tops-women/tanks-women/nora-practice-tank.html', 'catalog/product/view/id/1721/category/26', 0, 1, NULL, 1, '{"category_id":"26"}'),
(508, 'product', 1721, 'collections/yoga-new/nora-practice-tank.html', 'catalog/product/view/id/1721/category/8', 0, 1, NULL, 1, '{"category_id":"8"}'),
(509, 'product', 1721, 'collections/erin-recommends/nora-practice-tank.html', 'catalog/product/view/id/1721/category/34', 0, 1, NULL, 1, '{"category_id":"34"}'),
(510, 'product', 1737, 'women/tops-women/tanks-women/nona-fitness-tank.html', 'catalog/product/view/id/1737/category/26', 0, 1, NULL, 1, '{"category_id":"26"}'),
(511, 'product', 1737, 'collections/performance-fabrics/nona-fitness-tank.html', 'catalog/product/view/id/1737/category/35', 0, 1, NULL, 1, '{"category_id":"35"}'),
(512, 'product', 1753, 'leah-yoga-top.html', 'catalog/product/view/id/1753', 0, 1, NULL, 1, NULL),
(513, 'product', 1769, 'chloe-compete-tank.html', 'catalog/product/view/id/1769', 0, 1, NULL, 1, NULL),
(514, 'product', 1785, 'maya-tunic.html', 'catalog/product/view/id/1785', 0, 1, NULL, 1, NULL),
(515, 'product', 1801, 'antonia-racer-tank.html', 'catalog/product/view/id/1801', 0, 1, NULL, 1, NULL),
(516, 'product', 1817, 'breathe-easy-tank.html', 'catalog/product/view/id/1817', 0, 1, NULL, 1, NULL),
(517, 'product', 1824, 'karmen-yoga-pant.html', 'catalog/product/view/id/1824', 0, 1, NULL, 1, NULL),
(518, 'product', 1831, 'emma-leggings.html', 'catalog/product/view/id/1831', 0, 1, NULL, 1, NULL),
(519, 'product', 1838, 'ida-workout-parachute-pant.html', 'catalog/product/view/id/1838', 0, 1, NULL, 1, NULL),
(520, 'product', 1845, 'cora-parachute-pant.html', 'catalog/product/view/id/1845', 0, 1, NULL, 1, NULL),
(521, 'product', 1753, 'women/tops-women/tanks-women/leah-yoga-top.html', 'catalog/product/view/id/1753/category/26', 0, 1, NULL, 1, '{"category_id":"26"}'),
(522, 'product', 1753, 'collections/erin-recommends/leah-yoga-top.html', 'catalog/product/view/id/1753/category/34', 0, 1, NULL, 1, '{"category_id":"34"}'),
(523, 'product', 1769, 'women/tops-women/tanks-women/chloe-compete-tank.html', 'catalog/product/view/id/1769/category/26', 0, 1, NULL, 1, '{"category_id":"26"}'),
(524, 'product', 1785, 'women/tops-women/tanks-women/maya-tunic.html', 'catalog/product/view/id/1785/category/26', 0, 1, NULL, 1, '{"category_id":"26"}'),
(525, 'product', 1785, 'collections/performance-fabrics/maya-tunic.html', 'catalog/product/view/id/1785/category/35', 0, 1, NULL, 1, '{"category_id":"35"}'),
(526, 'product', 1801, 'women/tops-women/tanks-women/antonia-racer-tank.html', 'catalog/product/view/id/1801/category/26', 0, 1, NULL, 1, '{"category_id":"26"}'),
(527, 'product', 1801, 'promotions/women-sale/antonia-racer-tank.html', 'catalog/product/view/id/1801/category/30', 0, 1, NULL, 1, '{"category_id":"30"}'),
(528, 'product', 1801, 'collections/performance-fabrics/antonia-racer-tank.html', 'catalog/product/view/id/1801/category/35', 0, 1, NULL, 1, '{"category_id":"35"}'),
(529, 'product', 1817, 'women/tops-women/tanks-women/breathe-easy-tank.html', 'catalog/product/view/id/1817/category/26', 0, 1, NULL, 1, '{"category_id":"26"}'),
(530, 'product', 1817, 'promotions/women-sale/breathe-easy-tank.html', 'catalog/product/view/id/1817/category/30', 0, 1, NULL, 1, '{"category_id":"30"}'),
(531, 'product', 1817, 'collections/erin-recommends/breathe-easy-tank.html', 'catalog/product/view/id/1817/category/34', 0, 1, NULL, 1, '{"category_id":"34"}'),
(532, 'product', 1824, 'women/bottoms-women/pants-women/karmen-yoga-pant.html', 'catalog/product/view/id/1824/category/27', 0, 1, NULL, 1, '{"category_id":"27"}'),
(533, 'product', 1824, 'promotions/pants-all/karmen-yoga-pant.html', 'catalog/product/view/id/1824/category/32', 0, 1, NULL, 1, '{"category_id":"32"}'),
(534, 'product', 1824, 'collections/performance-fabrics/karmen-yoga-pant.html', 'catalog/product/view/id/1824/category/35', 0, 1, NULL, 1, '{"category_id":"35"}'),
(535, 'product', 1831, 'women/bottoms-women/pants-women/emma-leggings.html', 'catalog/product/view/id/1831/category/27', 0, 1, NULL, 1, '{"category_id":"27"}'),
(536, 'product', 1831, 'promotions/pants-all/emma-leggings.html', 'catalog/product/view/id/1831/category/32', 0, 1, NULL, 1, '{"category_id":"32"}'),
(537, 'product', 1831, 'collections/performance-fabrics/emma-leggings.html', 'catalog/product/view/id/1831/category/35', 0, 1, NULL, 1, '{"category_id":"35"}'),
(538, 'product', 1838, 'women/bottoms-women/pants-women/ida-workout-parachute-pant.html', 'catalog/product/view/id/1838/category/27', 0, 1, NULL, 1, '{"category_id":"27"}'),
(539, 'product', 1838, 'promotions/pants-all/ida-workout-parachute-pant.html', 'catalog/product/view/id/1838/category/32', 0, 1, NULL, 1, '{"category_id":"32"}'),
(540, 'product', 1838, 'collections/yoga-new/ida-workout-parachute-pant.html', 'catalog/product/view/id/1838/category/8', 0, 1, NULL, 1, '{"category_id":"8"}'),
(541, 'product', 1845, 'women/bottoms-women/pants-women/cora-parachute-pant.html', 'catalog/product/view/id/1845/category/27', 0, 1, NULL, 1, '{"category_id":"27"}'),
(542, 'product', 1845, 'promotions/pants-all/cora-parachute-pant.html', 'catalog/product/view/id/1845/category/32', 0, 1, NULL, 1, '{"category_id":"32"}'),
(543, 'product', 1852, 'sahara-leggings.html', 'catalog/product/view/id/1852', 0, 1, NULL, 1, NULL),
(544, 'product', 1859, 'diana-tights.html', 'catalog/product/view/id/1859', 0, 1, NULL, 1, NULL),
(545, 'product', 1866, 'aeon-capri.html', 'catalog/product/view/id/1866', 0, 1, NULL, 1, NULL),
(546, 'product', 1873, 'bardot-capri.html', 'catalog/product/view/id/1873', 0, 1, NULL, 1, NULL),
(547, 'product', 1880, 'carina-basic-capri.html', 'catalog/product/view/id/1880', 0, 1, NULL, 1, NULL),
(548, 'product', 1887, 'daria-bikram-pant.html', 'catalog/product/view/id/1887', 0, 1, NULL, 1, NULL),
(549, 'product', 1894, 'sylvia-capri.html', 'catalog/product/view/id/1894', 0, 1, NULL, 1, NULL),
(550, 'product', 1901, 'deirdre-relaxed-fit-capri.html', 'catalog/product/view/id/1901', 0, 1, NULL, 1, NULL),
(551, 'product', 1908, 'portia-capri.html', 'catalog/product/view/id/1908', 0, 1, NULL, 1, NULL),
(552, 'product', 1924, 'fiona-fitness-short.html', 'catalog/product/view/id/1924', 0, 1, NULL, 1, NULL),
(553, 'product', 1940, 'maxima-drawstring-short.html', 'catalog/product/view/id/1940', 0, 1, NULL, 1, NULL),
(554, 'product', 1852, 'women/bottoms-women/pants-women/sahara-leggings.html', 'catalog/product/view/id/1852/category/27', 0, 1, NULL, 1, '{"category_id":"27"}'),
(555, 'product', 1852, 'promotions/pants-all/sahara-leggings.html', 'catalog/product/view/id/1852/category/32', 0, 1, NULL, 1, '{"category_id":"32"}'),
(556, 'product', 1852, 'collections/erin-recommends/sahara-leggings.html', 'catalog/product/view/id/1852/category/34', 0, 1, NULL, 1, '{"category_id":"34"}'),
(557, 'product', 1859, 'women/bottoms-women/pants-women/diana-tights.html', 'catalog/product/view/id/1859/category/27', 0, 1, NULL, 1, '{"category_id":"27"}'),
(558, 'product', 1859, 'promotions/pants-all/diana-tights.html', 'catalog/product/view/id/1859/category/32', 0, 1, NULL, 1, '{"category_id":"32"}'),
(559, 'product', 1859, 'collections/erin-recommends/diana-tights.html', 'catalog/product/view/id/1859/category/34', 0, 1, NULL, 1, '{"category_id":"34"}'),
(560, 'product', 1866, 'women/bottoms-women/pants-women/aeon-capri.html', 'catalog/product/view/id/1866/category/27', 0, 1, NULL, 1, '{"category_id":"27"}'),
(561, 'product', 1866, 'promotions/pants-all/aeon-capri.html', 'catalog/product/view/id/1866/category/32', 0, 1, NULL, 1, '{"category_id":"32"}'),
(562, 'product', 1866, 'collections/performance-fabrics/aeon-capri.html', 'catalog/product/view/id/1866/category/35', 0, 1, NULL, 1, '{"category_id":"35"}'),
(563, 'product', 1873, 'women/bottoms-women/pants-women/bardot-capri.html', 'catalog/product/view/id/1873/category/27', 0, 1, NULL, 1, '{"category_id":"27"}'),
(564, 'product', 1873, 'promotions/pants-all/bardot-capri.html', 'catalog/product/view/id/1873/category/32', 0, 1, NULL, 1, '{"category_id":"32"}'),
(565, 'product', 1880, 'women/bottoms-women/pants-women/carina-basic-capri.html', 'catalog/product/view/id/1880/category/27', 0, 1, NULL, 1, '{"category_id":"27"}'),
(566, 'product', 1880, 'promotions/pants-all/carina-basic-capri.html', 'catalog/product/view/id/1880/category/32', 0, 1, NULL, 1, '{"category_id":"32"}'),
(567, 'product', 1887, 'women/bottoms-women/pants-women/daria-bikram-pant.html', 'catalog/product/view/id/1887/category/27', 0, 1, NULL, 1, '{"category_id":"27"}'),
(568, 'product', 1887, 'promotions/pants-all/daria-bikram-pant.html', 'catalog/product/view/id/1887/category/32', 0, 1, NULL, 1, '{"category_id":"32"}'),
(569, 'product', 1887, 'collections/eco-friendly/daria-bikram-pant.html', 'catalog/product/view/id/1887/category/36', 0, 1, NULL, 1, '{"category_id":"36"}'),
(570, 'product', 1894, 'women/bottoms-women/pants-women/sylvia-capri.html', 'catalog/product/view/id/1894/category/27', 0, 1, NULL, 1, '{"category_id":"27"}'),
(571, 'product', 1894, 'promotions/pants-all/sylvia-capri.html', 'catalog/product/view/id/1894/category/32', 0, 1, NULL, 1, '{"category_id":"32"}'),
(572, 'product', 1901, 'women/bottoms-women/pants-women/deirdre-relaxed-fit-capri.html', 'catalog/product/view/id/1901/category/27', 0, 1, NULL, 1, '{"category_id":"27"}'),
(573, 'product', 1901, 'promotions/pants-all/deirdre-relaxed-fit-capri.html', 'catalog/product/view/id/1901/category/32', 0, 1, NULL, 1, '{"category_id":"32"}'),
(574, 'product', 1901, 'collections/performance-fabrics/deirdre-relaxed-fit-capri.html', 'catalog/product/view/id/1901/category/35', 0, 1, NULL, 1, '{"category_id":"35"}'),
(575, 'product', 1908, 'women/bottoms-women/pants-women/portia-capri.html', 'catalog/product/view/id/1908/category/27', 0, 1, NULL, 1, '{"category_id":"27"}'),
(576, 'product', 1908, 'promotions/pants-all/portia-capri.html', 'catalog/product/view/id/1908/category/32', 0, 1, NULL, 1, '{"category_id":"32"}'),
(577, 'product', 1908, 'collections/erin-recommends/portia-capri.html', 'catalog/product/view/id/1908/category/34', 0, 1, NULL, 1, '{"category_id":"34"}'),
(578, 'product', 1924, 'women/bottoms-women/shorts-women/fiona-fitness-short.html', 'catalog/product/view/id/1924/category/28', 0, 1, NULL, 1, '{"category_id":"28"}'),
(579, 'product', 1924, 'collections/yoga-new/fiona-fitness-short.html', 'catalog/product/view/id/1924/category/8', 0, 1, NULL, 1, '{"category_id":"8"}'),
(580, 'product', 1924, 'collections/eco-friendly/fiona-fitness-short.html', 'catalog/product/view/id/1924/category/36', 0, 1, NULL, 1, '{"category_id":"36"}'),
(581, 'product', 1940, 'women/bottoms-women/shorts-women/maxima-drawstring-short.html', 'catalog/product/view/id/1940/category/28', 0, 1, NULL, 1, '{"category_id":"28"}'),
(582, 'product', 1940, 'promotions/women-sale/maxima-drawstring-short.html', 'catalog/product/view/id/1940/category/30', 0, 1, NULL, 1, '{"category_id":"30"}'),
(583, 'product', 1956, 'gwen-drawstring-bike-short.html', 'catalog/product/view/id/1956', 0, 1, NULL, 1, NULL),
(584, 'product', 1972, 'artemis-running-short.html', 'catalog/product/view/id/1972', 0, 1, NULL, 1, NULL),
(585, 'product', 1988, 'bess-yoga-short.html', 'catalog/product/view/id/1988', 0, 1, NULL, 1, NULL),
(586, 'product', 1995, 'angel-light-running-short.html', 'catalog/product/view/id/1995', 0, 1, NULL, 1, NULL),
(587, 'product', 2002, 'echo-fit-compression-short.html', 'catalog/product/view/id/2002', 0, 1, NULL, 1, NULL),
(588, 'product', 2008, 'sybil-running-short.html', 'catalog/product/view/id/2008', 0, 1, NULL, 1, NULL),
(589, 'product', 2015, 'mimi-all-purpose-short.html', 'catalog/product/view/id/2015', 0, 1, NULL, 1, NULL),
(590, 'product', 2022, 'ana-running-short.html', 'catalog/product/view/id/2022', 0, 1, NULL, 1, NULL),
(591, 'product', 2029, 'ina-compression-short.html', 'catalog/product/view/id/2029', 0, 1, NULL, 1, NULL),
(592, 'product', 2045, 'erika-running-short.html', 'catalog/product/view/id/2045', 0, 1, NULL, 1, NULL),
(593, 'product', 1956, 'women/bottoms-women/shorts-women/gwen-drawstring-bike-short.html', 'catalog/product/view/id/1956/category/28', 0, 1, NULL, 1, '{"category_id":"28"}'),
(594, 'product', 1956, 'collections/yoga-new/gwen-drawstring-bike-short.html', 'catalog/product/view/id/1956/category/8', 0, 1, NULL, 1, '{"category_id":"8"}'),
(595, 'product', 1956, 'collections/performance-fabrics/gwen-drawstring-bike-short.html', 'catalog/product/view/id/1956/category/35', 0, 1, NULL, 1, '{"category_id":"35"}'),
(596, 'product', 1972, 'women/bottoms-women/shorts-women/artemis-running-short.html', 'catalog/product/view/id/1972/category/28', 0, 1, NULL, 1, '{"category_id":"28"}'),
(597, 'product', 1988, 'women/bottoms-women/shorts-women/bess-yoga-short.html', 'catalog/product/view/id/1988/category/28', 0, 1, NULL, 1, '{"category_id":"28"}'),
(598, 'product', 1988, 'promotions/women-sale/bess-yoga-short.html', 'catalog/product/view/id/1988/category/30', 0, 1, NULL, 1, '{"category_id":"30"}'),
(599, 'product', 1995, 'women/bottoms-women/shorts-women/angel-light-running-short.html', 'catalog/product/view/id/1995/category/28', 0, 1, NULL, 1, '{"category_id":"28"}'),
(600, 'product', 2002, 'women/bottoms-women/shorts-women/echo-fit-compression-short.html', 'catalog/product/view/id/2002/category/28', 0, 1, NULL, 1, '{"category_id":"28"}'),
(601, 'product', 2002, 'collections/yoga-new/echo-fit-compression-short.html', 'catalog/product/view/id/2002/category/8', 0, 1, NULL, 1, '{"category_id":"8"}'),
(602, 'product', 2008, 'women/bottoms-women/shorts-women/sybil-running-short.html', 'catalog/product/view/id/2008/category/28', 0, 1, NULL, 1, '{"category_id":"28"}'),
(603, 'product', 2008, 'collections/performance-fabrics/sybil-running-short.html', 'catalog/product/view/id/2008/category/35', 0, 1, NULL, 1, '{"category_id":"35"}'),
(604, 'product', 2015, 'women/bottoms-women/shorts-women/mimi-all-purpose-short.html', 'catalog/product/view/id/2015/category/28', 0, 1, NULL, 1, '{"category_id":"28"}'),
(605, 'product', 2022, 'women/bottoms-women/shorts-women/ana-running-short.html', 'catalog/product/view/id/2022/category/28', 0, 1, NULL, 1, '{"category_id":"28"}'),
(606, 'product', 2022, 'collections/eco-friendly/ana-running-short.html', 'catalog/product/view/id/2022/category/36', 0, 1, NULL, 1, '{"category_id":"36"}'),
(607, 'product', 2029, 'women/bottoms-women/shorts-women/ina-compression-short.html', 'catalog/product/view/id/2029/category/28', 0, 1, NULL, 1, '{"category_id":"28"}'),
(608, 'product', 2029, 'collections/erin-recommends/ina-compression-short.html', 'catalog/product/view/id/2029/category/34', 0, 1, NULL, 1, '{"category_id":"34"}'),
(609, 'product', 2045, 'women/bottoms-women/shorts-women/erika-running-short.html', 'catalog/product/view/id/2045/category/28', 0, 1, NULL, 1, '{"category_id":"28"}'),
(610, 'product', 2045, 'collections/erin-recommends/erika-running-short.html', 'catalog/product/view/id/2045/category/34', 0, 1, NULL, 1, '{"category_id":"34"}'),
(611, 'category', 37, 'sale.html', 'catalog/category/view/id/37', 0, 1, NULL, 1, NULL),
(612, 'category', 38, 'what-is-new.html', 'catalog/category/view/id/38', 0, 1, NULL, 1, NULL),
(613, 'category', 39, 'collections/performance-new.html', 'catalog/category/view/id/39', 0, 1, NULL, 1, NULL),
(614, 'category', 40, 'collections/eco-new.html', 'catalog/category/view/id/40', 0, 1, NULL, 1, NULL),
(615, 'cms-page', 5, 'about-us', 'cms/page/view/page_id/5', 0, 1, NULL, 1, NULL),
(616, 'cms-page', 6, 'customer-service', 'cms/page/view/page_id/6', 0, 1, NULL, 1, NULL),
(617, 'product', 2046, 'set-of-sprite-yoga-straps.html', 'catalog/product/view/id/2046', 0, 1, NULL, 1, NULL),
(618, 'product', 2046, 'gear/set-of-sprite-yoga-straps.html', 'catalog/product/view/id/2046/category/3', 0, 1, NULL, 1, '{"category_id":"3"}'),
(619, 'product', 2046, 'gear/fitness-equipment/set-of-sprite-yoga-straps.html', 'catalog/product/view/id/2046/category/5', 0, 1, NULL, 1, '{"category_id":"5"}');

-- --------------------------------------------------------

--
-- Структура таблиці `variable`
--

CREATE TABLE `variable` (
  `variable_id` int(10) UNSIGNED NOT NULL COMMENT 'Variable Id',
  `code` varchar(255) DEFAULT NULL COMMENT 'Variable Code',
  `name` varchar(255) DEFAULT NULL COMMENT 'Variable Name'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Variables';

-- --------------------------------------------------------

--
-- Структура таблиці `variable_value`
--

CREATE TABLE `variable_value` (
  `value_id` int(10) UNSIGNED NOT NULL COMMENT 'Variable Value Id',
  `variable_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Variable Id',
  `store_id` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Store Id',
  `plain_value` text COMMENT 'Plain Text Value',
  `html_value` text COMMENT 'Html Value'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Variable Value';

-- --------------------------------------------------------

--
-- Структура таблиці `vault_payment_token`
--

CREATE TABLE `vault_payment_token` (
  `entity_id` int(10) UNSIGNED NOT NULL COMMENT 'Entity Id',
  `customer_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Customer Id',
  `public_hash` varchar(128) NOT NULL COMMENT 'Hash code for using on frontend',
  `payment_method_code` varchar(128) NOT NULL COMMENT 'Payment method code',
  `type` varchar(128) NOT NULL COMMENT 'Type',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
  `expires_at` timestamp NULL DEFAULT NULL COMMENT 'Expires At',
  `gateway_token` varchar(255) NOT NULL COMMENT 'Gateway Token',
  `details` text COMMENT 'Details',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_visible` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Vault tokens of payment';

-- --------------------------------------------------------

--
-- Структура таблиці `vault_payment_token_order_payment_link`
--

CREATE TABLE `vault_payment_token_order_payment_link` (
  `order_payment_id` int(10) UNSIGNED NOT NULL COMMENT 'Order payment Id',
  `payment_token_id` int(10) UNSIGNED NOT NULL COMMENT 'Payment token Id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Order payments to vault token';

-- --------------------------------------------------------

--
-- Структура таблиці `vertex_customer_code`
--

CREATE TABLE `vertex_customer_code` (
  `customer_id` int(10) UNSIGNED NOT NULL COMMENT 'Customer ID',
  `customer_code` text COMMENT 'Customer Code for Vertex'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='vertex_customer_code';

-- --------------------------------------------------------

--
-- Структура таблиці `vertex_invoice_sent`
--

CREATE TABLE `vertex_invoice_sent` (
  `invoice_id` int(10) UNSIGNED NOT NULL COMMENT 'Invoice ID',
  `sent_to_vertex` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Invoice has been logged in Vertex'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='vertex_invoice_sent';

-- --------------------------------------------------------

--
-- Структура таблиці `vertex_order_invoice_status`
--

CREATE TABLE `vertex_order_invoice_status` (
  `order_id` int(10) UNSIGNED NOT NULL COMMENT 'Order ID',
  `sent_to_vertex` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Invoice has been logged in Vertex'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='vertex_order_invoice_status';

-- --------------------------------------------------------

--
-- Структура таблиці `vertex_taxrequest`
--

CREATE TABLE `vertex_taxrequest` (
  `request_id` bigint(20) UNSIGNED NOT NULL,
  `request_type` varchar(255) NOT NULL COMMENT 'Request Type',
  `quote_id` bigint(20) DEFAULT NULL,
  `order_id` bigint(20) DEFAULT NULL,
  `total_tax` varchar(255) NOT NULL COMMENT 'Total Tax Amount',
  `source_path` varchar(255) NOT NULL COMMENT 'Source path controller_module_action',
  `tax_area_id` varchar(255) NOT NULL COMMENT 'Tax Jurisdictions Id',
  `sub_total` varchar(255) NOT NULL COMMENT 'Response Subtotal Amount',
  `total` varchar(255) NOT NULL COMMENT 'Response Total Amount',
  `lookup_result` varchar(255) NOT NULL COMMENT 'Tax Area Response Lookup Result',
  `request_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Request create date',
  `request_xml` text NOT NULL COMMENT 'Request XML',
  `response_xml` text NOT NULL COMMENT 'Response XML'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Log of requests to Vertex';

-- --------------------------------------------------------

--
-- Структура таблиці `weee_tax`
--

CREATE TABLE `weee_tax` (
  `value_id` int(11) NOT NULL COMMENT 'Value Id',
  `website_id` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Website Id',
  `entity_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Entity Id',
  `country` varchar(2) DEFAULT NULL COMMENT 'Country',
  `value` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Value',
  `state` int(11) NOT NULL DEFAULT '0' COMMENT 'State',
  `attribute_id` smallint(5) UNSIGNED NOT NULL COMMENT 'Attribute Id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Weee Tax';

-- --------------------------------------------------------

--
-- Структура таблиці `widget`
--

CREATE TABLE `widget` (
  `widget_id` int(10) UNSIGNED NOT NULL COMMENT 'Widget Id',
  `widget_code` varchar(255) DEFAULT NULL COMMENT 'Widget code for template directive',
  `widget_type` varchar(255) DEFAULT NULL COMMENT 'Widget Type',
  `parameters` text COMMENT 'Parameters'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Preconfigured Widgets';

-- --------------------------------------------------------

--
-- Структура таблиці `widget_instance`
--

CREATE TABLE `widget_instance` (
  `instance_id` int(10) UNSIGNED NOT NULL COMMENT 'Instance Id',
  `instance_type` varchar(255) DEFAULT NULL COMMENT 'Instance Type',
  `theme_id` int(10) UNSIGNED NOT NULL COMMENT 'Theme id',
  `title` varchar(255) DEFAULT NULL COMMENT 'Widget Title',
  `store_ids` varchar(255) NOT NULL DEFAULT '0' COMMENT 'Store ids',
  `widget_parameters` text COMMENT 'Widget parameters',
  `sort_order` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Sort order'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Instances of Widget for Package Theme';

--
-- Дамп даних таблиці `widget_instance`
--

INSERT INTO `widget_instance` (`instance_id`, `instance_type`, `theme_id`, `title`, `store_ids`, `widget_parameters`, `sort_order`) VALUES
(1, 'Magento\\Cms\\Block\\Widget\\Block', 2, 'Contact us info', '0', '{"block_id":"2"}', 0),
(2, 'Magento\\Cms\\Block\\Widget\\Block', 2, 'Footer Links', '0', '{"block_id":"20"}', 0),
(3, 'Magento\\Cms\\Block\\Widget\\Block', 2, 'Sale Left Menu', '0', '{"block_id":"3"}', 0),
(4, 'Magento\\Cms\\Block\\Widget\\Block', 2, 'Gear Left Menu', '0', '{"block_id":"4"}', 0),
(5, 'Magento\\Cms\\Block\\Widget\\Block', 2, 'Men\'s Left Menu', '0', '{"block_id":"5"}', 0),
(6, 'Magento\\Cms\\Block\\Widget\\Block', 2, 'Women\'s Left Menu', '0', '{"block_id":"6"}', 0),
(7, 'Magento\\Cms\\Block\\Widget\\Block', 2, 'What\'s New Left Menu', '0', '{"block_id":"7"}', 0),
(8, 'Magento\\Cms\\Block\\Widget\\Block', 2, 'Women Category Content', '0', '{"block_id":"8"}', 0),
(9, 'Magento\\Cms\\Block\\Widget\\Block', 2, 'Training Category Content', '0', '{"block_id":"9"}', 0),
(10, 'Magento\\Cms\\Block\\Widget\\Block', 2, 'Men Category Content', '0', '{"block_id":"10"}', 0),
(11, 'Magento\\Cms\\Block\\Widget\\Block', 2, 'Gear Category Content', '0', '{"block_id":"11"}', 0),
(12, 'Magento\\Cms\\Block\\Widget\\Block', 2, 'New Products Category Content', '0', '{"block_id":"13"}', 0),
(13, 'Magento\\Cms\\Block\\Widget\\Block', 2, 'Sale Category Content', '0', '{"block_id":"12"}', 0),
(14, 'Magento\\Cms\\Block\\Widget\\Block', 2, 'Home Page', '0', '{"block_id":"14"}', 0),
(15, 'Magento\\Cms\\Block\\Widget\\Block', 2, 'Performance Fabrics', '0', '{"block_id":"15"}', 0),
(16, 'Magento\\Cms\\Block\\Widget\\Block', 2, 'Eco Friendly', '0', '{"block_id":"16"}', 0),
(17, 'Magento\\Cms\\Block\\Widget\\Block', 2, 'Login Info', '0', '{"block_id":"18"}', 0),
(18, 'Magento\\Cms\\Block\\Widget\\Block', 2, 'Giftcard Category Content', '0', '{"block_id":"17"}', 0),
(19, 'Magento\\Cms\\Block\\Widget\\Block', 4, 'Main content block', '0', '{"block_id":"13"}', 0),
(21, 'Magento\\Cms\\Block\\Widget\\Block', 4, 'Sales', '0', '{"block_id":"19"}', 0),
(22, 'Magento\\Cms\\Block\\Widget\\Block', 4, 'My footer links', '0', '{"block_id":"1"}', 0);

-- --------------------------------------------------------

--
-- Структура таблиці `widget_instance_page`
--

CREATE TABLE `widget_instance_page` (
  `page_id` int(10) UNSIGNED NOT NULL COMMENT 'Page Id',
  `instance_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Instance Id',
  `page_group` varchar(25) DEFAULT NULL COMMENT 'Block Group Type',
  `layout_handle` varchar(255) DEFAULT NULL COMMENT 'Layout Handle',
  `block_reference` varchar(255) DEFAULT NULL COMMENT 'Container',
  `page_for` varchar(25) DEFAULT NULL COMMENT 'For instance entities',
  `entities` text COMMENT 'Catalog entities (comma separated)',
  `page_template` varchar(255) DEFAULT NULL COMMENT 'Path to widget template'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Instance of Widget on Page';

--
-- Дамп даних таблиці `widget_instance_page`
--

INSERT INTO `widget_instance_page` (`page_id`, `instance_id`, `page_group`, `layout_handle`, `block_reference`, `page_for`, `entities`, `page_template`) VALUES
(1, 1, 'pages', 'contact_index_index', 'content.top', 'all', '', 'widget/static_block/default.phtml'),
(2, 2, 'all_pages', 'default', 'footer', 'all', '', 'widget/static_block/default.phtml'),
(3, 3, 'anchor_categories', 'catalog_category_view_type_layered', 'sidebar.main', 'specific', '37', 'widget/static_block/default.phtml'),
(4, 4, 'anchor_categories', 'catalog_category_view_type_layered', 'sidebar.main', 'specific', '3', 'widget/static_block/default.phtml'),
(5, 5, 'anchor_categories', 'catalog_category_view_type_layered', 'sidebar.main', 'specific', '11', 'widget/static_block/default.phtml'),
(6, 6, 'anchor_categories', 'catalog_category_view_type_layered', 'sidebar.main', 'specific', '20', 'widget/static_block/default.phtml'),
(7, 7, 'anchor_categories', 'catalog_category_view_type_layered', 'sidebar.main', 'specific', '38', 'widget/static_block/default.phtml'),
(8, 8, 'anchor_categories', 'catalog_category_view_type_layered', 'content.top', 'specific', '20', 'widget/static_block/default.phtml'),
(9, 9, 'anchor_categories', 'catalog_category_view_type_layered', 'content.top', 'specific', '9', 'widget/static_block/default.phtml'),
(10, 10, 'anchor_categories', 'catalog_category_view_type_layered', 'content.top', 'specific', '11', 'widget/static_block/default.phtml'),
(11, 11, 'anchor_categories', 'catalog_category_view_type_layered', 'content.top', 'specific', '3', 'widget/static_block/default.phtml'),
(12, 12, 'anchor_categories', 'catalog_category_view_type_layered', 'content.top', 'specific', '38', 'widget/static_block/default.phtml'),
(13, 13, 'anchor_categories', 'catalog_category_view_type_layered', 'content.top', 'specific', '37', 'widget/static_block/default.phtml'),
(14, 14, 'pages', 'cms_index_index', 'content', 'all', '', 'widget/static_block/default.phtml'),
(15, 15, 'anchor_categories', 'catalog_category_view_type_layered', 'content.top', 'specific', '39', 'widget/static_block/default.phtml'),
(16, 16, 'anchor_categories', 'catalog_category_view_type_layered', 'content.top', 'specific', '40', 'widget/static_block/default.phtml'),
(17, 17, 'pages', 'customer_account_login', 'customer.login.container', 'all', '', 'widget/static_block/default.phtml'),
(18, 18, 'anchor_categories', 'catalog_category_view_type_layered', 'content.top', 'specific', NULL, 'widget/static_block/default.phtml'),
(19, 19, 'pages', 'cms_index_index', 'content.top', 'all', '', 'widget/static_block/default.phtml'),
(22, 22, 'all_pages', 'default', 'cms_footer_links_container', 'all', '', 'widget/static_block/default.phtml'),
(23, 21, 'pages', 'cms_index_index', 'content.top', 'all', '', 'widget/static_block/default.phtml');

-- --------------------------------------------------------

--
-- Структура таблиці `widget_instance_page_layout`
--

CREATE TABLE `widget_instance_page_layout` (
  `page_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Page Id',
  `layout_update_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Layout Update Id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Layout updates';

--
-- Дамп даних таблиці `widget_instance_page_layout`
--

INSERT INTO `widget_instance_page_layout` (`page_id`, `layout_update_id`) VALUES
(1, 1),
(2, 28),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8),
(9, 9),
(10, 10),
(11, 11),
(12, 12),
(13, 13),
(14, 14),
(15, 15),
(16, 16),
(17, 17),
(18, 18),
(19, 30),
(22, 27),
(23, 34);

-- --------------------------------------------------------

--
-- Структура таблиці `wishlist`
--

CREATE TABLE `wishlist` (
  `wishlist_id` int(10) UNSIGNED NOT NULL COMMENT 'Wishlist ID',
  `customer_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Customer ID',
  `shared` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Sharing flag (0 or 1)',
  `sharing_code` varchar(32) DEFAULT NULL COMMENT 'Sharing encrypted code',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Last updated date'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Wishlist main Table';

-- --------------------------------------------------------

--
-- Структура таблиці `wishlist_item`
--

CREATE TABLE `wishlist_item` (
  `wishlist_item_id` int(10) UNSIGNED NOT NULL COMMENT 'Wishlist item ID',
  `wishlist_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Wishlist ID',
  `product_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Product ID',
  `store_id` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Store ID',
  `added_at` timestamp NULL DEFAULT NULL COMMENT 'Add date and time',
  `description` text COMMENT 'Short description of wish list item',
  `qty` decimal(12,4) NOT NULL COMMENT 'Qty'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Wishlist items';

-- --------------------------------------------------------

--
-- Структура таблиці `wishlist_item_option`
--

CREATE TABLE `wishlist_item_option` (
  `option_id` int(10) UNSIGNED NOT NULL COMMENT 'Option Id',
  `wishlist_item_id` int(10) UNSIGNED NOT NULL COMMENT 'Wishlist Item Id',
  `product_id` int(10) UNSIGNED NOT NULL COMMENT 'Product Id',
  `code` varchar(255) NOT NULL COMMENT 'Code',
  `value` text COMMENT 'Value'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Wishlist Item Option Table';

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `review_entity`
--
ALTER TABLE `review_entity`
  ADD PRIMARY KEY (`entity_id`);

--
-- Індекси таблиці `review_entity_summary`
--
ALTER TABLE `review_entity_summary`
  ADD PRIMARY KEY (`primary_id`),
  ADD KEY `REVIEW_ENTITY_SUMMARY_STORE_ID` (`store_id`);

--
-- Індекси таблиці `review_status`
--
ALTER TABLE `review_status`
  ADD PRIMARY KEY (`status_id`);

--
-- Індекси таблиці `review_store`
--
ALTER TABLE `review_store`
  ADD PRIMARY KEY (`review_id`,`store_id`),
  ADD KEY `REVIEW_STORE_STORE_ID` (`store_id`);

--
-- Індекси таблиці `salesrule`
--
ALTER TABLE `salesrule`
  ADD PRIMARY KEY (`rule_id`),
  ADD KEY `SALESRULE_IS_ACTIVE_SORT_ORDER_TO_DATE_FROM_DATE` (`is_active`,`sort_order`,`to_date`,`from_date`);

--
-- Індекси таблиці `salesrule_coupon`
--
ALTER TABLE `salesrule_coupon`
  ADD PRIMARY KEY (`coupon_id`),
  ADD UNIQUE KEY `SALESRULE_COUPON_CODE` (`code`),
  ADD UNIQUE KEY `SALESRULE_COUPON_RULE_ID_IS_PRIMARY` (`rule_id`,`is_primary`),
  ADD KEY `SALESRULE_COUPON_RULE_ID` (`rule_id`);

--
-- Індекси таблиці `salesrule_coupon_aggregated`
--
ALTER TABLE `salesrule_coupon_aggregated`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `SALESRULE_COUPON_AGGRED_PERIOD_STORE_ID_ORDER_STS_COUPON_CODE` (`period`,`store_id`,`order_status`,`coupon_code`),
  ADD KEY `SALESRULE_COUPON_AGGREGATED_STORE_ID` (`store_id`),
  ADD KEY `SALESRULE_COUPON_AGGREGATED_RULE_NAME` (`rule_name`);

--
-- Індекси таблиці `salesrule_coupon_aggregated_order`
--
ALTER TABLE `salesrule_coupon_aggregated_order`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNQ_1094D1FBBCBB11704A29DEF3ACC37D2B` (`period`,`store_id`,`order_status`,`coupon_code`),
  ADD KEY `SALESRULE_COUPON_AGGREGATED_ORDER_STORE_ID` (`store_id`),
  ADD KEY `SALESRULE_COUPON_AGGREGATED_ORDER_RULE_NAME` (`rule_name`);

--
-- Індекси таблиці `salesrule_coupon_aggregated_updated`
--
ALTER TABLE `salesrule_coupon_aggregated_updated`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNQ_7196FA120A4F0F84E1B66605E87E213E` (`period`,`store_id`,`order_status`,`coupon_code`),
  ADD KEY `SALESRULE_COUPON_AGGREGATED_UPDATED_STORE_ID` (`store_id`),
  ADD KEY `SALESRULE_COUPON_AGGREGATED_UPDATED_RULE_NAME` (`rule_name`);

--
-- Індекси таблиці `salesrule_coupon_usage`
--
ALTER TABLE `salesrule_coupon_usage`
  ADD PRIMARY KEY (`coupon_id`,`customer_id`),
  ADD KEY `SALESRULE_COUPON_USAGE_CUSTOMER_ID` (`customer_id`);

--
-- Індекси таблиці `salesrule_customer`
--
ALTER TABLE `salesrule_customer`
  ADD PRIMARY KEY (`rule_customer_id`),
  ADD KEY `SALESRULE_CUSTOMER_RULE_ID_CUSTOMER_ID` (`rule_id`,`customer_id`),
  ADD KEY `SALESRULE_CUSTOMER_CUSTOMER_ID_RULE_ID` (`customer_id`,`rule_id`);

--
-- Індекси таблиці `salesrule_customer_group`
--
ALTER TABLE `salesrule_customer_group`
  ADD PRIMARY KEY (`rule_id`,`customer_group_id`),
  ADD KEY `SALESRULE_CUSTOMER_GROUP_CUSTOMER_GROUP_ID` (`customer_group_id`);

--
-- Індекси таблиці `salesrule_label`
--
ALTER TABLE `salesrule_label`
  ADD PRIMARY KEY (`label_id`),
  ADD UNIQUE KEY `SALESRULE_LABEL_RULE_ID_STORE_ID` (`rule_id`,`store_id`),
  ADD KEY `SALESRULE_LABEL_STORE_ID` (`store_id`);

--
-- Індекси таблиці `salesrule_product_attribute`
--
ALTER TABLE `salesrule_product_attribute`
  ADD PRIMARY KEY (`rule_id`,`website_id`,`customer_group_id`,`attribute_id`),
  ADD KEY `SALESRULE_PRODUCT_ATTRIBUTE_WEBSITE_ID` (`website_id`),
  ADD KEY `SALESRULE_PRODUCT_ATTRIBUTE_CUSTOMER_GROUP_ID` (`customer_group_id`),
  ADD KEY `SALESRULE_PRODUCT_ATTRIBUTE_ATTRIBUTE_ID` (`attribute_id`);

--
-- Індекси таблиці `salesrule_website`
--
ALTER TABLE `salesrule_website`
  ADD PRIMARY KEY (`rule_id`,`website_id`),
  ADD KEY `SALESRULE_WEBSITE_WEBSITE_ID` (`website_id`);

--
-- Індекси таблиці `sales_bestsellers_aggregated_daily`
--
ALTER TABLE `sales_bestsellers_aggregated_daily`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `SALES_BESTSELLERS_AGGREGATED_DAILY_PERIOD_STORE_ID_PRODUCT_ID` (`period`,`store_id`,`product_id`),
  ADD KEY `SALES_BESTSELLERS_AGGREGATED_DAILY_STORE_ID` (`store_id`),
  ADD KEY `SALES_BESTSELLERS_AGGREGATED_DAILY_PRODUCT_ID` (`product_id`);

--
-- Індекси таблиці `sales_bestsellers_aggregated_monthly`
--
ALTER TABLE `sales_bestsellers_aggregated_monthly`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `SALES_BESTSELLERS_AGGREGATED_MONTHLY_PERIOD_STORE_ID_PRODUCT_ID` (`period`,`store_id`,`product_id`),
  ADD KEY `SALES_BESTSELLERS_AGGREGATED_MONTHLY_STORE_ID` (`store_id`),
  ADD KEY `SALES_BESTSELLERS_AGGREGATED_MONTHLY_PRODUCT_ID` (`product_id`);

--
-- Індекси таблиці `sales_bestsellers_aggregated_yearly`
--
ALTER TABLE `sales_bestsellers_aggregated_yearly`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `SALES_BESTSELLERS_AGGREGATED_YEARLY_PERIOD_STORE_ID_PRODUCT_ID` (`period`,`store_id`,`product_id`),
  ADD KEY `SALES_BESTSELLERS_AGGREGATED_YEARLY_STORE_ID` (`store_id`),
  ADD KEY `SALES_BESTSELLERS_AGGREGATED_YEARLY_PRODUCT_ID` (`product_id`);

--
-- Індекси таблиці `sales_creditmemo`
--
ALTER TABLE `sales_creditmemo`
  ADD PRIMARY KEY (`entity_id`),
  ADD UNIQUE KEY `SALES_CREDITMEMO_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  ADD KEY `SALES_CREDITMEMO_STORE_ID` (`store_id`),
  ADD KEY `SALES_CREDITMEMO_ORDER_ID` (`order_id`),
  ADD KEY `SALES_CREDITMEMO_CREDITMEMO_STATUS` (`creditmemo_status`),
  ADD KEY `SALES_CREDITMEMO_STATE` (`state`),
  ADD KEY `SALES_CREDITMEMO_CREATED_AT` (`created_at`),
  ADD KEY `SALES_CREDITMEMO_UPDATED_AT` (`updated_at`),
  ADD KEY `SALES_CREDITMEMO_SEND_EMAIL` (`send_email`),
  ADD KEY `SALES_CREDITMEMO_EMAIL_SENT` (`email_sent`);

--
-- Індекси таблиці `sales_creditmemo_comment`
--
ALTER TABLE `sales_creditmemo_comment`
  ADD PRIMARY KEY (`entity_id`),
  ADD KEY `SALES_CREDITMEMO_COMMENT_CREATED_AT` (`created_at`),
  ADD KEY `SALES_CREDITMEMO_COMMENT_PARENT_ID` (`parent_id`);

--
-- Індекси таблиці `sales_creditmemo_grid`
--
ALTER TABLE `sales_creditmemo_grid`
  ADD PRIMARY KEY (`entity_id`),
  ADD UNIQUE KEY `SALES_CREDITMEMO_GRID_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  ADD KEY `SALES_CREDITMEMO_GRID_ORDER_INCREMENT_ID` (`order_increment_id`),
  ADD KEY `SALES_CREDITMEMO_GRID_CREATED_AT` (`created_at`),
  ADD KEY `SALES_CREDITMEMO_GRID_UPDATED_AT` (`updated_at`),
  ADD KEY `SALES_CREDITMEMO_GRID_ORDER_CREATED_AT` (`order_created_at`),
  ADD KEY `SALES_CREDITMEMO_GRID_STATE` (`state`),
  ADD KEY `SALES_CREDITMEMO_GRID_BILLING_NAME` (`billing_name`),
  ADD KEY `SALES_CREDITMEMO_GRID_ORDER_STATUS` (`order_status`),
  ADD KEY `SALES_CREDITMEMO_GRID_BASE_GRAND_TOTAL` (`base_grand_total`),
  ADD KEY `SALES_CREDITMEMO_GRID_STORE_ID` (`store_id`),
  ADD KEY `SALES_CREDITMEMO_GRID_ORDER_BASE_GRAND_TOTAL` (`order_base_grand_total`),
  ADD KEY `SALES_CREDITMEMO_GRID_ORDER_ID` (`order_id`);
ALTER TABLE `sales_creditmemo_grid` ADD FULLTEXT KEY `FTI_32B7BA885941A8254EE84AE650ABDC86` (`increment_id`,`order_increment_id`,`billing_name`,`billing_address`,`shipping_address`,`customer_name`,`customer_email`);

--
-- Індекси таблиці `sales_creditmemo_item`
--
ALTER TABLE `sales_creditmemo_item`
  ADD PRIMARY KEY (`entity_id`),
  ADD KEY `SALES_CREDITMEMO_ITEM_PARENT_ID` (`parent_id`);

--
-- Індекси таблиці `sales_invoice`
--
ALTER TABLE `sales_invoice`
  ADD PRIMARY KEY (`entity_id`),
  ADD UNIQUE KEY `SALES_INVOICE_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  ADD KEY `SALES_INVOICE_STORE_ID` (`store_id`),
  ADD KEY `SALES_INVOICE_GRAND_TOTAL` (`grand_total`),
  ADD KEY `SALES_INVOICE_ORDER_ID` (`order_id`),
  ADD KEY `SALES_INVOICE_STATE` (`state`),
  ADD KEY `SALES_INVOICE_CREATED_AT` (`created_at`),
  ADD KEY `SALES_INVOICE_UPDATED_AT` (`updated_at`),
  ADD KEY `SALES_INVOICE_SEND_EMAIL` (`send_email`),
  ADD KEY `SALES_INVOICE_EMAIL_SENT` (`email_sent`);

--
-- Індекси таблиці `sales_invoiced_aggregated`
--
ALTER TABLE `sales_invoiced_aggregated`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `SALES_INVOICED_AGGREGATED_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  ADD KEY `SALES_INVOICED_AGGREGATED_STORE_ID` (`store_id`);

--
-- Індекси таблиці `sales_invoiced_aggregated_order`
--
ALTER TABLE `sales_invoiced_aggregated_order`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `SALES_INVOICED_AGGREGATED_ORDER_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  ADD KEY `SALES_INVOICED_AGGREGATED_ORDER_STORE_ID` (`store_id`);

--
-- Індекси таблиці `sales_invoice_comment`
--
ALTER TABLE `sales_invoice_comment`
  ADD PRIMARY KEY (`entity_id`),
  ADD KEY `SALES_INVOICE_COMMENT_CREATED_AT` (`created_at`),
  ADD KEY `SALES_INVOICE_COMMENT_PARENT_ID` (`parent_id`);

--
-- Індекси таблиці `sales_invoice_grid`
--
ALTER TABLE `sales_invoice_grid`
  ADD PRIMARY KEY (`entity_id`),
  ADD UNIQUE KEY `SALES_INVOICE_GRID_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  ADD KEY `SALES_INVOICE_GRID_STORE_ID` (`store_id`),
  ADD KEY `SALES_INVOICE_GRID_GRAND_TOTAL` (`grand_total`),
  ADD KEY `SALES_INVOICE_GRID_ORDER_ID` (`order_id`),
  ADD KEY `SALES_INVOICE_GRID_STATE` (`state`),
  ADD KEY `SALES_INVOICE_GRID_ORDER_INCREMENT_ID` (`order_increment_id`),
  ADD KEY `SALES_INVOICE_GRID_CREATED_AT` (`created_at`),
  ADD KEY `SALES_INVOICE_GRID_UPDATED_AT` (`updated_at`),
  ADD KEY `SALES_INVOICE_GRID_ORDER_CREATED_AT` (`order_created_at`),
  ADD KEY `SALES_INVOICE_GRID_BILLING_NAME` (`billing_name`),
  ADD KEY `SALES_INVOICE_GRID_BASE_GRAND_TOTAL` (`base_grand_total`);
ALTER TABLE `sales_invoice_grid` ADD FULLTEXT KEY `FTI_95D9C924DD6A8734EB8B5D01D60F90DE` (`increment_id`,`order_increment_id`,`billing_name`,`billing_address`,`shipping_address`,`customer_name`,`customer_email`);

--
-- Індекси таблиці `sales_invoice_item`
--
ALTER TABLE `sales_invoice_item`
  ADD PRIMARY KEY (`entity_id`),
  ADD KEY `SALES_INVOICE_ITEM_PARENT_ID` (`parent_id`);

--
-- Індекси таблиці `sales_order`
--
ALTER TABLE `sales_order`
  ADD PRIMARY KEY (`entity_id`),
  ADD UNIQUE KEY `SALES_ORDER_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  ADD KEY `SALES_ORDER_STATUS` (`status`),
  ADD KEY `SALES_ORDER_STATE` (`state`),
  ADD KEY `SALES_ORDER_STORE_ID` (`store_id`),
  ADD KEY `SALES_ORDER_CREATED_AT` (`created_at`),
  ADD KEY `SALES_ORDER_CUSTOMER_ID` (`customer_id`),
  ADD KEY `SALES_ORDER_EXT_ORDER_ID` (`ext_order_id`),
  ADD KEY `SALES_ORDER_QUOTE_ID` (`quote_id`),
  ADD KEY `SALES_ORDER_UPDATED_AT` (`updated_at`),
  ADD KEY `SALES_ORDER_SEND_EMAIL` (`send_email`),
  ADD KEY `SALES_ORDER_EMAIL_SENT` (`email_sent`);

--
-- Індекси таблиці `sales_order_address`
--
ALTER TABLE `sales_order_address`
  ADD PRIMARY KEY (`entity_id`),
  ADD KEY `SALES_ORDER_ADDRESS_PARENT_ID` (`parent_id`);

--
-- Індекси таблиці `sales_order_aggregated_created`
--
ALTER TABLE `sales_order_aggregated_created`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `SALES_ORDER_AGGREGATED_CREATED_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  ADD KEY `SALES_ORDER_AGGREGATED_CREATED_STORE_ID` (`store_id`);

--
-- Індекси таблиці `sales_order_aggregated_updated`
--
ALTER TABLE `sales_order_aggregated_updated`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `SALES_ORDER_AGGREGATED_UPDATED_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  ADD KEY `SALES_ORDER_AGGREGATED_UPDATED_STORE_ID` (`store_id`);

--
-- Індекси таблиці `sales_order_grid`
--
ALTER TABLE `sales_order_grid`
  ADD PRIMARY KEY (`entity_id`),
  ADD UNIQUE KEY `SALES_ORDER_GRID_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  ADD KEY `SALES_ORDER_GRID_STATUS` (`status`),
  ADD KEY `SALES_ORDER_GRID_STORE_ID` (`store_id`),
  ADD KEY `SALES_ORDER_GRID_BASE_GRAND_TOTAL` (`base_grand_total`),
  ADD KEY `SALES_ORDER_GRID_BASE_TOTAL_PAID` (`base_total_paid`),
  ADD KEY `SALES_ORDER_GRID_GRAND_TOTAL` (`grand_total`),
  ADD KEY `SALES_ORDER_GRID_TOTAL_PAID` (`total_paid`),
  ADD KEY `SALES_ORDER_GRID_SHIPPING_NAME` (`shipping_name`),
  ADD KEY `SALES_ORDER_GRID_BILLING_NAME` (`billing_name`),
  ADD KEY `SALES_ORDER_GRID_CREATED_AT` (`created_at`),
  ADD KEY `SALES_ORDER_GRID_CUSTOMER_ID` (`customer_id`),
  ADD KEY `SALES_ORDER_GRID_UPDATED_AT` (`updated_at`);
ALTER TABLE `sales_order_grid` ADD FULLTEXT KEY `FTI_65B9E9925EC58F0C7C2E2F6379C233E7` (`increment_id`,`billing_name`,`shipping_name`,`shipping_address`,`billing_address`,`customer_name`,`customer_email`);

--
-- Індекси таблиці `sales_order_item`
--
ALTER TABLE `sales_order_item`
  ADD PRIMARY KEY (`item_id`),
  ADD KEY `SALES_ORDER_ITEM_ORDER_ID` (`order_id`),
  ADD KEY `SALES_ORDER_ITEM_STORE_ID` (`store_id`);

--
-- Індекси таблиці `sales_order_payment`
--
ALTER TABLE `sales_order_payment`
  ADD PRIMARY KEY (`entity_id`),
  ADD KEY `SALES_ORDER_PAYMENT_PARENT_ID` (`parent_id`);

--
-- Індекси таблиці `sales_order_status`
--
ALTER TABLE `sales_order_status`
  ADD PRIMARY KEY (`status`);

--
-- Індекси таблиці `sales_order_status_history`
--
ALTER TABLE `sales_order_status_history`
  ADD PRIMARY KEY (`entity_id`),
  ADD KEY `SALES_ORDER_STATUS_HISTORY_PARENT_ID` (`parent_id`),
  ADD KEY `SALES_ORDER_STATUS_HISTORY_CREATED_AT` (`created_at`);

--
-- Індекси таблиці `sales_order_status_label`
--
ALTER TABLE `sales_order_status_label`
  ADD PRIMARY KEY (`status`,`store_id`),
  ADD KEY `SALES_ORDER_STATUS_LABEL_STORE_ID` (`store_id`);

--
-- Індекси таблиці `sales_order_status_state`
--
ALTER TABLE `sales_order_status_state`
  ADD PRIMARY KEY (`status`,`state`);

--
-- Індекси таблиці `sales_order_tax`
--
ALTER TABLE `sales_order_tax`
  ADD PRIMARY KEY (`tax_id`),
  ADD KEY `SALES_ORDER_TAX_ORDER_ID_PRIORITY_POSITION` (`order_id`,`priority`,`position`);

--
-- Індекси таблиці `sales_order_tax_item`
--
ALTER TABLE `sales_order_tax_item`
  ADD PRIMARY KEY (`tax_item_id`),
  ADD UNIQUE KEY `SALES_ORDER_TAX_ITEM_TAX_ID_ITEM_ID` (`tax_id`,`item_id`),
  ADD KEY `SALES_ORDER_TAX_ITEM_ITEM_ID` (`item_id`),
  ADD KEY `SALES_ORDER_TAX_ITEM_ASSOCIATED_ITEM_ID_SALES_ORDER_ITEM_ITEM_ID` (`associated_item_id`);

--
-- Індекси таблиці `sales_payment_transaction`
--
ALTER TABLE `sales_payment_transaction`
  ADD PRIMARY KEY (`transaction_id`),
  ADD UNIQUE KEY `SALES_PAYMENT_TRANSACTION_ORDER_ID_PAYMENT_ID_TXN_ID` (`order_id`,`payment_id`,`txn_id`),
  ADD KEY `SALES_PAYMENT_TRANSACTION_PARENT_ID` (`parent_id`),
  ADD KEY `SALES_PAYMENT_TRANSACTION_PAYMENT_ID` (`payment_id`);

--
-- Індекси таблиці `sales_refunded_aggregated`
--
ALTER TABLE `sales_refunded_aggregated`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `SALES_REFUNDED_AGGREGATED_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  ADD KEY `SALES_REFUNDED_AGGREGATED_STORE_ID` (`store_id`);

--
-- Індекси таблиці `sales_refunded_aggregated_order`
--
ALTER TABLE `sales_refunded_aggregated_order`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `SALES_REFUNDED_AGGREGATED_ORDER_PERIOD_STORE_ID_ORDER_STATUS` (`period`,`store_id`,`order_status`),
  ADD KEY `SALES_REFUNDED_AGGREGATED_ORDER_STORE_ID` (`store_id`);

--
-- Індекси таблиці `sales_sequence_meta`
--
ALTER TABLE `sales_sequence_meta`
  ADD PRIMARY KEY (`meta_id`),
  ADD UNIQUE KEY `SALES_SEQUENCE_META_ENTITY_TYPE_STORE_ID` (`entity_type`,`store_id`);

--
-- Індекси таблиці `sales_sequence_profile`
--
ALTER TABLE `sales_sequence_profile`
  ADD PRIMARY KEY (`profile_id`),
  ADD UNIQUE KEY `SALES_SEQUENCE_PROFILE_META_ID_PREFIX_SUFFIX` (`meta_id`,`prefix`,`suffix`);

--
-- Індекси таблиці `sales_shipment`
--
ALTER TABLE `sales_shipment`
  ADD PRIMARY KEY (`entity_id`),
  ADD UNIQUE KEY `SALES_SHIPMENT_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  ADD KEY `SALES_SHIPMENT_STORE_ID` (`store_id`),
  ADD KEY `SALES_SHIPMENT_TOTAL_QTY` (`total_qty`),
  ADD KEY `SALES_SHIPMENT_ORDER_ID` (`order_id`),
  ADD KEY `SALES_SHIPMENT_CREATED_AT` (`created_at`),
  ADD KEY `SALES_SHIPMENT_UPDATED_AT` (`updated_at`),
  ADD KEY `SALES_SHIPMENT_SEND_EMAIL` (`send_email`),
  ADD KEY `SALES_SHIPMENT_EMAIL_SENT` (`email_sent`);

--
-- Індекси таблиці `sales_shipment_comment`
--
ALTER TABLE `sales_shipment_comment`
  ADD PRIMARY KEY (`entity_id`),
  ADD KEY `SALES_SHIPMENT_COMMENT_CREATED_AT` (`created_at`),
  ADD KEY `SALES_SHIPMENT_COMMENT_PARENT_ID` (`parent_id`);

--
-- Індекси таблиці `sales_shipment_grid`
--
ALTER TABLE `sales_shipment_grid`
  ADD PRIMARY KEY (`entity_id`),
  ADD UNIQUE KEY `SALES_SHIPMENT_GRID_INCREMENT_ID_STORE_ID` (`increment_id`,`store_id`),
  ADD KEY `SALES_SHIPMENT_GRID_STORE_ID` (`store_id`),
  ADD KEY `SALES_SHIPMENT_GRID_TOTAL_QTY` (`total_qty`),
  ADD KEY `SALES_SHIPMENT_GRID_ORDER_INCREMENT_ID` (`order_increment_id`),
  ADD KEY `SALES_SHIPMENT_GRID_SHIPMENT_STATUS` (`shipment_status`),
  ADD KEY `SALES_SHIPMENT_GRID_ORDER_STATUS` (`order_status`),
  ADD KEY `SALES_SHIPMENT_GRID_CREATED_AT` (`created_at`),
  ADD KEY `SALES_SHIPMENT_GRID_UPDATED_AT` (`updated_at`),
  ADD KEY `SALES_SHIPMENT_GRID_ORDER_CREATED_AT` (`order_created_at`),
  ADD KEY `SALES_SHIPMENT_GRID_SHIPPING_NAME` (`shipping_name`),
  ADD KEY `SALES_SHIPMENT_GRID_BILLING_NAME` (`billing_name`);
ALTER TABLE `sales_shipment_grid` ADD FULLTEXT KEY `FTI_086B40C8955F167B8EA76653437879B4` (`increment_id`,`order_increment_id`,`shipping_name`,`customer_name`,`customer_email`,`billing_address`,`shipping_address`);

--
-- Індекси таблиці `sales_shipment_item`
--
ALTER TABLE `sales_shipment_item`
  ADD PRIMARY KEY (`entity_id`),
  ADD KEY `SALES_SHIPMENT_ITEM_PARENT_ID` (`parent_id`);

--
-- Індекси таблиці `sales_shipment_track`
--
ALTER TABLE `sales_shipment_track`
  ADD PRIMARY KEY (`entity_id`),
  ADD KEY `SALES_SHIPMENT_TRACK_PARENT_ID` (`parent_id`),
  ADD KEY `SALES_SHIPMENT_TRACK_ORDER_ID` (`order_id`),
  ADD KEY `SALES_SHIPMENT_TRACK_CREATED_AT` (`created_at`);

--
-- Індекси таблиці `sales_shipping_aggregated`
--
ALTER TABLE `sales_shipping_aggregated`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `SALES_SHPP_AGGRED_PERIOD_STORE_ID_ORDER_STS_SHPP_DESCRIPTION` (`period`,`store_id`,`order_status`,`shipping_description`),
  ADD KEY `SALES_SHIPPING_AGGREGATED_STORE_ID` (`store_id`);

--
-- Індекси таблиці `sales_shipping_aggregated_order`
--
ALTER TABLE `sales_shipping_aggregated_order`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNQ_C05FAE47282EEA68654D0924E946761F` (`period`,`store_id`,`order_status`,`shipping_description`),
  ADD KEY `SALES_SHIPPING_AGGREGATED_ORDER_STORE_ID` (`store_id`);

--
-- Індекси таблиці `search_query`
--
ALTER TABLE `search_query`
  ADD PRIMARY KEY (`query_id`),
  ADD UNIQUE KEY `SEARCH_QUERY_QUERY_TEXT_STORE_ID` (`query_text`,`store_id`),
  ADD KEY `SEARCH_QUERY_QUERY_TEXT_STORE_ID_POPULARITY` (`query_text`,`store_id`,`popularity`),
  ADD KEY `SEARCH_QUERY_STORE_ID` (`store_id`),
  ADD KEY `SEARCH_QUERY_IS_PROCESSED` (`is_processed`);

--
-- Індекси таблиці `search_synonyms`
--
ALTER TABLE `search_synonyms`
  ADD PRIMARY KEY (`group_id`),
  ADD KEY `SEARCH_SYNONYMS_STORE_ID` (`store_id`),
  ADD KEY `SEARCH_SYNONYMS_WEBSITE_ID` (`website_id`);
ALTER TABLE `search_synonyms` ADD FULLTEXT KEY `SEARCH_SYNONYMS_SYNONYMS` (`synonyms`);

--
-- Індекси таблиці `sendfriend_log`
--
ALTER TABLE `sendfriend_log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `SENDFRIEND_LOG_IP` (`ip`),
  ADD KEY `SENDFRIEND_LOG_TIME` (`time`);

--
-- Індекси таблиці `sequence_creditmemo_0`
--
ALTER TABLE `sequence_creditmemo_0`
  ADD PRIMARY KEY (`sequence_value`);

--
-- Індекси таблиці `sequence_creditmemo_1`
--
ALTER TABLE `sequence_creditmemo_1`
  ADD PRIMARY KEY (`sequence_value`);

--
-- Індекси таблиці `sequence_invoice_0`
--
ALTER TABLE `sequence_invoice_0`
  ADD PRIMARY KEY (`sequence_value`);

--
-- Індекси таблиці `sequence_invoice_1`
--
ALTER TABLE `sequence_invoice_1`
  ADD PRIMARY KEY (`sequence_value`);

--
-- Індекси таблиці `sequence_order_0`
--
ALTER TABLE `sequence_order_0`
  ADD PRIMARY KEY (`sequence_value`);

--
-- Індекси таблиці `sequence_order_1`
--
ALTER TABLE `sequence_order_1`
  ADD PRIMARY KEY (`sequence_value`);

--
-- Індекси таблиці `sequence_shipment_0`
--
ALTER TABLE `sequence_shipment_0`
  ADD PRIMARY KEY (`sequence_value`);

--
-- Індекси таблиці `sequence_shipment_1`
--
ALTER TABLE `sequence_shipment_1`
  ADD PRIMARY KEY (`sequence_value`);

--
-- Індекси таблиці `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`session_id`);

--
-- Індекси таблиці `setup_module`
--
ALTER TABLE `setup_module`
  ADD PRIMARY KEY (`module`);

--
-- Індекси таблиці `shipping_tablerate`
--
ALTER TABLE `shipping_tablerate`
  ADD PRIMARY KEY (`pk`),
  ADD UNIQUE KEY `UNQ_D60821CDB2AFACEE1566CFC02D0D4CAA` (`website_id`,`dest_country_id`,`dest_region_id`,`dest_zip`,`condition_name`,`condition_value`);

--
-- Індекси таблиці `signifyd_case`
--
ALTER TABLE `signifyd_case`
  ADD PRIMARY KEY (`entity_id`),
  ADD UNIQUE KEY `SIGNIFYD_CASE_ORDER_ID` (`order_id`),
  ADD UNIQUE KEY `SIGNIFYD_CASE_CASE_ID` (`case_id`);

--
-- Індекси таблиці `sitemap`
--
ALTER TABLE `sitemap`
  ADD PRIMARY KEY (`sitemap_id`),
  ADD KEY `SITEMAP_STORE_ID` (`store_id`);

--
-- Індекси таблиці `store`
--
ALTER TABLE `store`
  ADD PRIMARY KEY (`store_id`),
  ADD UNIQUE KEY `STORE_CODE` (`code`),
  ADD KEY `STORE_WEBSITE_ID` (`website_id`),
  ADD KEY `STORE_IS_ACTIVE_SORT_ORDER` (`is_active`,`sort_order`),
  ADD KEY `STORE_GROUP_ID` (`group_id`);

--
-- Індекси таблиці `store_group`
--
ALTER TABLE `store_group`
  ADD PRIMARY KEY (`group_id`),
  ADD UNIQUE KEY `STORE_GROUP_CODE` (`code`),
  ADD KEY `STORE_GROUP_WEBSITE_ID` (`website_id`),
  ADD KEY `STORE_GROUP_DEFAULT_STORE_ID` (`default_store_id`);

--
-- Індекси таблиці `store_website`
--
ALTER TABLE `store_website`
  ADD PRIMARY KEY (`website_id`),
  ADD UNIQUE KEY `STORE_WEBSITE_CODE` (`code`),
  ADD KEY `STORE_WEBSITE_SORT_ORDER` (`sort_order`),
  ADD KEY `STORE_WEBSITE_DEFAULT_GROUP_ID` (`default_group_id`);

--
-- Індекси таблиці `tax_calculation`
--
ALTER TABLE `tax_calculation`
  ADD PRIMARY KEY (`tax_calculation_id`),
  ADD KEY `TAX_CALCULATION_TAX_CALCULATION_RULE_ID` (`tax_calculation_rule_id`),
  ADD KEY `TAX_CALCULATION_CUSTOMER_TAX_CLASS_ID` (`customer_tax_class_id`),
  ADD KEY `TAX_CALCULATION_PRODUCT_TAX_CLASS_ID` (`product_tax_class_id`),
  ADD KEY `TAX_CALC_TAX_CALC_RATE_ID_CSTR_TAX_CLASS_ID_PRD_TAX_CLASS_ID` (`tax_calculation_rate_id`,`customer_tax_class_id`,`product_tax_class_id`);

--
-- Індекси таблиці `tax_calculation_rate`
--
ALTER TABLE `tax_calculation_rate`
  ADD PRIMARY KEY (`tax_calculation_rate_id`),
  ADD KEY `TAX_CALCULATION_RATE_TAX_COUNTRY_ID_TAX_REGION_ID_TAX_POSTCODE` (`tax_country_id`,`tax_region_id`,`tax_postcode`),
  ADD KEY `TAX_CALCULATION_RATE_CODE` (`code`),
  ADD KEY `IDX_CA799F1E2CB843495F601E56C84A626D` (`tax_calculation_rate_id`,`tax_country_id`,`tax_region_id`,`zip_is_range`,`tax_postcode`);

--
-- Індекси таблиці `tax_calculation_rate_title`
--
ALTER TABLE `tax_calculation_rate_title`
  ADD PRIMARY KEY (`tax_calculation_rate_title_id`),
  ADD KEY `TAX_CALCULATION_RATE_TITLE_TAX_CALCULATION_RATE_ID_STORE_ID` (`tax_calculation_rate_id`,`store_id`),
  ADD KEY `TAX_CALCULATION_RATE_TITLE_STORE_ID` (`store_id`);

--
-- Індекси таблиці `tax_calculation_rule`
--
ALTER TABLE `tax_calculation_rule`
  ADD PRIMARY KEY (`tax_calculation_rule_id`),
  ADD KEY `TAX_CALCULATION_RULE_PRIORITY_POSITION` (`priority`,`position`),
  ADD KEY `TAX_CALCULATION_RULE_CODE` (`code`);

--
-- Індекси таблиці `tax_class`
--
ALTER TABLE `tax_class`
  ADD PRIMARY KEY (`class_id`);

--
-- Індекси таблиці `tax_order_aggregated_created`
--
ALTER TABLE `tax_order_aggregated_created`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `TAX_ORDER_AGGRED_CREATED_PERIOD_STORE_ID_CODE_PERCENT_ORDER_STS` (`period`,`store_id`,`code`,`percent`,`order_status`),
  ADD KEY `TAX_ORDER_AGGREGATED_CREATED_STORE_ID` (`store_id`);

--
-- Індекси таблиці `tax_order_aggregated_updated`
--
ALTER TABLE `tax_order_aggregated_updated`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `TAX_ORDER_AGGRED_UPDATED_PERIOD_STORE_ID_CODE_PERCENT_ORDER_STS` (`period`,`store_id`,`code`,`percent`,`order_status`),
  ADD KEY `TAX_ORDER_AGGREGATED_UPDATED_STORE_ID` (`store_id`);

--
-- Індекси таблиці `temando_checkout_address`
--
ALTER TABLE `temando_checkout_address`
  ADD PRIMARY KEY (`entity_id`),
  ADD KEY `TEMANDO_CHKT_ADDR_SHPP_ADDR_ID_QUOTE_ADDR_ADDR_ID` (`shipping_address_id`);

--
-- Індекси таблиці `temando_collection_point_search`
--
ALTER TABLE `temando_collection_point_search`
  ADD PRIMARY KEY (`shipping_address_id`),
  ADD KEY `TEMANDO_COLLECTION_POINT_SRCH_COUNTRY_ID_DIR_COUNTRY_COUNTRY_ID` (`country_id`);

--
-- Індекси таблиці `temando_order`
--
ALTER TABLE `temando_order`
  ADD PRIMARY KEY (`entity_id`),
  ADD KEY `TEMANDO_ORDER_ORDER_ID_SALES_ORDER_ENTITY_ID` (`order_id`);

--
-- Індекси таблиці `temando_order_collection_point`
--
ALTER TABLE `temando_order_collection_point`
  ADD PRIMARY KEY (`recipient_address_id`);

--
-- Індекси таблиці `temando_quote_collection_point`
--
ALTER TABLE `temando_quote_collection_point`
  ADD PRIMARY KEY (`entity_id`),
  ADD KEY `FK_4ABED96BBA0BAF57A5EC10E47B5A0F12` (`recipient_address_id`);

--
-- Індекси таблиці `temando_shipment`
--
ALTER TABLE `temando_shipment`
  ADD PRIMARY KEY (`entity_id`),
  ADD UNIQUE KEY `TEMANDO_SHIPMENT_SHIPMENT_ID_EXT_SHIPMENT_ID` (`shipment_id`,`ext_shipment_id`);

--
-- Індекси таблиці `theme`
--
ALTER TABLE `theme`
  ADD PRIMARY KEY (`theme_id`);

--
-- Індекси таблиці `theme_file`
--
ALTER TABLE `theme_file`
  ADD PRIMARY KEY (`theme_files_id`),
  ADD KEY `THEME_FILE_THEME_ID_THEME_THEME_ID` (`theme_id`);

--
-- Індекси таблиці `translation`
--
ALTER TABLE `translation`
  ADD PRIMARY KEY (`key_id`),
  ADD UNIQUE KEY `TRANSLATION_STORE_ID_LOCALE_CRC_STRING_STRING` (`store_id`,`locale`,`crc_string`,`string`);

--
-- Індекси таблиці `trive_revo`
--
ALTER TABLE `trive_revo`
  ADD PRIMARY KEY (`slider_id`),
  ADD KEY `TRIVE_REVO_SLIDER_ID` (`slider_id`);

--
-- Індекси таблиці `trive_revo_products`
--
ALTER TABLE `trive_revo_products`
  ADD PRIMARY KEY (`slider_id`,`product_id`),
  ADD KEY `TRIVE_REVO_PRODUCTS_PRODUCT_ID` (`product_id`);

--
-- Індекси таблиці `trive_revo_stores`
--
ALTER TABLE `trive_revo_stores`
  ADD PRIMARY KEY (`slider_id`,`store_id`),
  ADD KEY `TRIVE_REVO_STORES_STORE_ID` (`store_id`);

--
-- Індекси таблиці `ui_bookmark`
--
ALTER TABLE `ui_bookmark`
  ADD PRIMARY KEY (`bookmark_id`),
  ADD KEY `UI_BOOKMARK_USER_ID_NAMESPACE_IDENTIFIER` (`user_id`,`namespace`,`identifier`);

--
-- Індекси таблиці `url_rewrite`
--
ALTER TABLE `url_rewrite`
  ADD PRIMARY KEY (`url_rewrite_id`),
  ADD UNIQUE KEY `URL_REWRITE_REQUEST_PATH_STORE_ID` (`request_path`,`store_id`),
  ADD KEY `URL_REWRITE_TARGET_PATH` (`target_path`),
  ADD KEY `URL_REWRITE_STORE_ID_ENTITY_ID` (`store_id`,`entity_id`);

--
-- Індекси таблиці `variable`
--
ALTER TABLE `variable`
  ADD PRIMARY KEY (`variable_id`),
  ADD UNIQUE KEY `VARIABLE_CODE` (`code`);

--
-- Індекси таблиці `variable_value`
--
ALTER TABLE `variable_value`
  ADD PRIMARY KEY (`value_id`),
  ADD UNIQUE KEY `VARIABLE_VALUE_VARIABLE_ID_STORE_ID` (`variable_id`,`store_id`),
  ADD KEY `VARIABLE_VALUE_STORE_ID` (`store_id`);

--
-- Індекси таблиці `vault_payment_token`
--
ALTER TABLE `vault_payment_token`
  ADD PRIMARY KEY (`entity_id`),
  ADD UNIQUE KEY `VAULT_PAYMENT_TOKEN_HASH_UNIQUE_INDEX_PUBLIC_HASH` (`public_hash`),
  ADD UNIQUE KEY `UNQ_54DCE14AEAEA03B587F9EF723EB10A10` (`payment_method_code`,`customer_id`,`gateway_token`),
  ADD KEY `VAULT_PAYMENT_TOKEN_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` (`customer_id`);

--
-- Індекси таблиці `vault_payment_token_order_payment_link`
--
ALTER TABLE `vault_payment_token_order_payment_link`
  ADD PRIMARY KEY (`order_payment_id`,`payment_token_id`),
  ADD KEY `FK_4ED894655446D385894580BECA993862` (`payment_token_id`);

--
-- Індекси таблиці `vertex_customer_code`
--
ALTER TABLE `vertex_customer_code`
  ADD PRIMARY KEY (`customer_id`);

--
-- Індекси таблиці `vertex_invoice_sent`
--
ALTER TABLE `vertex_invoice_sent`
  ADD PRIMARY KEY (`invoice_id`);

--
-- Індекси таблиці `vertex_order_invoice_status`
--
ALTER TABLE `vertex_order_invoice_status`
  ADD PRIMARY KEY (`order_id`);

--
-- Індекси таблиці `vertex_taxrequest`
--
ALTER TABLE `vertex_taxrequest`
  ADD PRIMARY KEY (`request_id`),
  ADD UNIQUE KEY `VERTEX_TAXREQUEST_REQUEST_ID` (`request_id`),
  ADD KEY `VERTEX_TAXREQUEST_REQUEST_TYPE` (`request_type`),
  ADD KEY `VERTEX_TAXREQUEST_ORDER_ID` (`order_id`);

--
-- Індекси таблиці `weee_tax`
--
ALTER TABLE `weee_tax`
  ADD PRIMARY KEY (`value_id`),
  ADD KEY `WEEE_TAX_WEBSITE_ID` (`website_id`),
  ADD KEY `WEEE_TAX_ENTITY_ID` (`entity_id`),
  ADD KEY `WEEE_TAX_COUNTRY` (`country`),
  ADD KEY `WEEE_TAX_ATTRIBUTE_ID` (`attribute_id`);

--
-- Індекси таблиці `widget`
--
ALTER TABLE `widget`
  ADD PRIMARY KEY (`widget_id`),
  ADD KEY `WIDGET_WIDGET_CODE` (`widget_code`);

--
-- Індекси таблиці `widget_instance`
--
ALTER TABLE `widget_instance`
  ADD PRIMARY KEY (`instance_id`),
  ADD KEY `WIDGET_INSTANCE_THEME_ID_THEME_THEME_ID` (`theme_id`);

--
-- Індекси таблиці `widget_instance_page`
--
ALTER TABLE `widget_instance_page`
  ADD PRIMARY KEY (`page_id`),
  ADD KEY `WIDGET_INSTANCE_PAGE_INSTANCE_ID` (`instance_id`);

--
-- Індекси таблиці `widget_instance_page_layout`
--
ALTER TABLE `widget_instance_page_layout`
  ADD UNIQUE KEY `WIDGET_INSTANCE_PAGE_LAYOUT_LAYOUT_UPDATE_ID_PAGE_ID` (`layout_update_id`,`page_id`),
  ADD KEY `WIDGET_INSTANCE_PAGE_LAYOUT_PAGE_ID` (`page_id`);

--
-- Індекси таблиці `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`wishlist_id`),
  ADD UNIQUE KEY `WISHLIST_CUSTOMER_ID` (`customer_id`),
  ADD KEY `WISHLIST_SHARED` (`shared`);

--
-- Індекси таблиці `wishlist_item`
--
ALTER TABLE `wishlist_item`
  ADD PRIMARY KEY (`wishlist_item_id`),
  ADD KEY `WISHLIST_ITEM_WISHLIST_ID` (`wishlist_id`),
  ADD KEY `WISHLIST_ITEM_PRODUCT_ID` (`product_id`),
  ADD KEY `WISHLIST_ITEM_STORE_ID` (`store_id`);

--
-- Індекси таблиці `wishlist_item_option`
--
ALTER TABLE `wishlist_item_option`
  ADD PRIMARY KEY (`option_id`),
  ADD KEY `FK_A014B30B04B72DD0EAB3EECD779728D6` (`wishlist_item_id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `review_entity`
--
ALTER TABLE `review_entity`
  MODIFY `entity_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Review entity id', AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблиці `review_entity_summary`
--
ALTER TABLE `review_entity_summary`
  MODIFY `primary_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Summary review entity id', AUTO_INCREMENT=251;
--
-- AUTO_INCREMENT для таблиці `review_status`
--
ALTER TABLE `review_status`
  MODIFY `status_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Status id', AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблиці `salesrule`
--
ALTER TABLE `salesrule`
  MODIFY `rule_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Rule Id', AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблиці `salesrule_coupon`
--
ALTER TABLE `salesrule_coupon`
  MODIFY `coupon_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Coupon Id', AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `salesrule_coupon_aggregated`
--
ALTER TABLE `salesrule_coupon_aggregated`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Id';
--
-- AUTO_INCREMENT для таблиці `salesrule_coupon_aggregated_order`
--
ALTER TABLE `salesrule_coupon_aggregated_order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Id';
--
-- AUTO_INCREMENT для таблиці `salesrule_coupon_aggregated_updated`
--
ALTER TABLE `salesrule_coupon_aggregated_updated`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Id';
--
-- AUTO_INCREMENT для таблиці `salesrule_customer`
--
ALTER TABLE `salesrule_customer`
  MODIFY `rule_customer_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Rule Customer Id', AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `salesrule_label`
--
ALTER TABLE `salesrule_label`
  MODIFY `label_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Label Id';
--
-- AUTO_INCREMENT для таблиці `sales_bestsellers_aggregated_daily`
--
ALTER TABLE `sales_bestsellers_aggregated_daily`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Id';
--
-- AUTO_INCREMENT для таблиці `sales_bestsellers_aggregated_monthly`
--
ALTER TABLE `sales_bestsellers_aggregated_monthly`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Id';
--
-- AUTO_INCREMENT для таблиці `sales_bestsellers_aggregated_yearly`
--
ALTER TABLE `sales_bestsellers_aggregated_yearly`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Id';
--
-- AUTO_INCREMENT для таблиці `sales_creditmemo`
--
ALTER TABLE `sales_creditmemo`
  MODIFY `entity_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Entity Id', AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `sales_creditmemo_comment`
--
ALTER TABLE `sales_creditmemo_comment`
  MODIFY `entity_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Entity Id';
--
-- AUTO_INCREMENT для таблиці `sales_creditmemo_item`
--
ALTER TABLE `sales_creditmemo_item`
  MODIFY `entity_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Entity Id', AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `sales_invoice`
--
ALTER TABLE `sales_invoice`
  MODIFY `entity_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Entity Id', AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблиці `sales_invoiced_aggregated`
--
ALTER TABLE `sales_invoiced_aggregated`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Id';
--
-- AUTO_INCREMENT для таблиці `sales_invoiced_aggregated_order`
--
ALTER TABLE `sales_invoiced_aggregated_order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Id';
--
-- AUTO_INCREMENT для таблиці `sales_invoice_comment`
--
ALTER TABLE `sales_invoice_comment`
  MODIFY `entity_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Entity Id';
--
-- AUTO_INCREMENT для таблиці `sales_invoice_item`
--
ALTER TABLE `sales_invoice_item`
  MODIFY `entity_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Entity Id', AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблиці `sales_order`
--
ALTER TABLE `sales_order`
  MODIFY `entity_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Entity Id', AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблиці `sales_order_address`
--
ALTER TABLE `sales_order_address`
  MODIFY `entity_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Entity Id', AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблиці `sales_order_aggregated_created`
--
ALTER TABLE `sales_order_aggregated_created`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Id';
--
-- AUTO_INCREMENT для таблиці `sales_order_aggregated_updated`
--
ALTER TABLE `sales_order_aggregated_updated`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Id';
--
-- AUTO_INCREMENT для таблиці `sales_order_item`
--
ALTER TABLE `sales_order_item`
  MODIFY `item_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Item Id', AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблиці `sales_order_payment`
--
ALTER TABLE `sales_order_payment`
  MODIFY `entity_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Entity Id', AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблиці `sales_order_status_history`
--
ALTER TABLE `sales_order_status_history`
  MODIFY `entity_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Entity Id', AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `sales_order_tax`
--
ALTER TABLE `sales_order_tax`
  MODIFY `tax_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Tax Id', AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблиці `sales_order_tax_item`
--
ALTER TABLE `sales_order_tax_item`
  MODIFY `tax_item_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Tax Item Id', AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблиці `sales_payment_transaction`
--
ALTER TABLE `sales_payment_transaction`
  MODIFY `transaction_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Transaction Id';
--
-- AUTO_INCREMENT для таблиці `sales_refunded_aggregated`
--
ALTER TABLE `sales_refunded_aggregated`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Id';
--
-- AUTO_INCREMENT для таблиці `sales_refunded_aggregated_order`
--
ALTER TABLE `sales_refunded_aggregated_order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Id';
--
-- AUTO_INCREMENT для таблиці `sales_sequence_meta`
--
ALTER TABLE `sales_sequence_meta`
  MODIFY `meta_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Id', AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблиці `sales_sequence_profile`
--
ALTER TABLE `sales_sequence_profile`
  MODIFY `profile_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Id', AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблиці `sales_shipment`
--
ALTER TABLE `sales_shipment`
  MODIFY `entity_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Entity Id', AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблиці `sales_shipment_comment`
--
ALTER TABLE `sales_shipment_comment`
  MODIFY `entity_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Entity Id';
--
-- AUTO_INCREMENT для таблиці `sales_shipment_item`
--
ALTER TABLE `sales_shipment_item`
  MODIFY `entity_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Entity Id', AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблиці `sales_shipment_track`
--
ALTER TABLE `sales_shipment_track`
  MODIFY `entity_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Entity Id';
--
-- AUTO_INCREMENT для таблиці `sales_shipping_aggregated`
--
ALTER TABLE `sales_shipping_aggregated`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Id';
--
-- AUTO_INCREMENT для таблиці `sales_shipping_aggregated_order`
--
ALTER TABLE `sales_shipping_aggregated_order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Id';
--
-- AUTO_INCREMENT для таблиці `search_query`
--
ALTER TABLE `search_query`
  MODIFY `query_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Query ID';
--
-- AUTO_INCREMENT для таблиці `search_synonyms`
--
ALTER TABLE `search_synonyms`
  MODIFY `group_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Synonyms Group Id';
--
-- AUTO_INCREMENT для таблиці `sendfriend_log`
--
ALTER TABLE `sendfriend_log`
  MODIFY `log_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Log ID';
--
-- AUTO_INCREMENT для таблиці `sequence_creditmemo_0`
--
ALTER TABLE `sequence_creditmemo_0`
  MODIFY `sequence_value` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `sequence_creditmemo_1`
--
ALTER TABLE `sequence_creditmemo_1`
  MODIFY `sequence_value` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `sequence_invoice_0`
--
ALTER TABLE `sequence_invoice_0`
  MODIFY `sequence_value` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `sequence_invoice_1`
--
ALTER TABLE `sequence_invoice_1`
  MODIFY `sequence_value` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблиці `sequence_order_0`
--
ALTER TABLE `sequence_order_0`
  MODIFY `sequence_value` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `sequence_order_1`
--
ALTER TABLE `sequence_order_1`
  MODIFY `sequence_value` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблиці `sequence_shipment_0`
--
ALTER TABLE `sequence_shipment_0`
  MODIFY `sequence_value` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `sequence_shipment_1`
--
ALTER TABLE `sequence_shipment_1`
  MODIFY `sequence_value` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблиці `shipping_tablerate`
--
ALTER TABLE `shipping_tablerate`
  MODIFY `pk` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary key', AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблиці `signifyd_case`
--
ALTER TABLE `signifyd_case`
  MODIFY `entity_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Entity_id';
--
-- AUTO_INCREMENT для таблиці `sitemap`
--
ALTER TABLE `sitemap`
  MODIFY `sitemap_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Sitemap Id';
--
-- AUTO_INCREMENT для таблиці `store`
--
ALTER TABLE `store`
  MODIFY `store_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Store Id', AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `store_group`
--
ALTER TABLE `store_group`
  MODIFY `group_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Group Id', AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `store_website`
--
ALTER TABLE `store_website`
  MODIFY `website_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Website Id', AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `tax_calculation`
--
ALTER TABLE `tax_calculation`
  MODIFY `tax_calculation_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Tax Calculation Id', AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `tax_calculation_rate`
--
ALTER TABLE `tax_calculation_rate`
  MODIFY `tax_calculation_rate_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Tax Calculation Rate Id', AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблиці `tax_calculation_rate_title`
--
ALTER TABLE `tax_calculation_rate_title`
  MODIFY `tax_calculation_rate_title_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Tax Calculation Rate Title Id';
--
-- AUTO_INCREMENT для таблиці `tax_calculation_rule`
--
ALTER TABLE `tax_calculation_rule`
  MODIFY `tax_calculation_rule_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Tax Calculation Rule Id', AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `tax_class`
--
ALTER TABLE `tax_class`
  MODIFY `class_id` smallint(6) NOT NULL AUTO_INCREMENT COMMENT 'Class Id', AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблиці `tax_order_aggregated_created`
--
ALTER TABLE `tax_order_aggregated_created`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Id';
--
-- AUTO_INCREMENT для таблиці `tax_order_aggregated_updated`
--
ALTER TABLE `tax_order_aggregated_updated`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Id';
--
-- AUTO_INCREMENT для таблиці `temando_checkout_address`
--
ALTER TABLE `temando_checkout_address`
  MODIFY `entity_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Entity Id';
--
-- AUTO_INCREMENT для таблиці `temando_order`
--
ALTER TABLE `temando_order`
  MODIFY `entity_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Entity Id';
--
-- AUTO_INCREMENT для таблиці `temando_quote_collection_point`
--
ALTER TABLE `temando_quote_collection_point`
  MODIFY `entity_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Entity Id';
--
-- AUTO_INCREMENT для таблиці `temando_shipment`
--
ALTER TABLE `temando_shipment`
  MODIFY `entity_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Entity Id';
--
-- AUTO_INCREMENT для таблиці `theme`
--
ALTER TABLE `theme`
  MODIFY `theme_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Theme identifier', AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблиці `theme_file`
--
ALTER TABLE `theme_file`
  MODIFY `theme_files_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Theme files identifier';
--
-- AUTO_INCREMENT для таблиці `translation`
--
ALTER TABLE `translation`
  MODIFY `key_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Key Id of Translation';
--
-- AUTO_INCREMENT для таблиці `trive_revo`
--
ALTER TABLE `trive_revo`
  MODIFY `slider_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Slider ID', AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблиці `ui_bookmark`
--
ALTER TABLE `ui_bookmark`
  MODIFY `bookmark_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Bookmark identifier', AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблиці `url_rewrite`
--
ALTER TABLE `url_rewrite`
  MODIFY `url_rewrite_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Rewrite Id', AUTO_INCREMENT=620;
--
-- AUTO_INCREMENT для таблиці `variable`
--
ALTER TABLE `variable`
  MODIFY `variable_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Variable Id';
--
-- AUTO_INCREMENT для таблиці `variable_value`
--
ALTER TABLE `variable_value`
  MODIFY `value_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Variable Value Id';
--
-- AUTO_INCREMENT для таблиці `vault_payment_token`
--
ALTER TABLE `vault_payment_token`
  MODIFY `entity_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Entity Id';
--
-- AUTO_INCREMENT для таблиці `vertex_taxrequest`
--
ALTER TABLE `vertex_taxrequest`
  MODIFY `request_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `weee_tax`
--
ALTER TABLE `weee_tax`
  MODIFY `value_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Value Id';
--
-- AUTO_INCREMENT для таблиці `widget`
--
ALTER TABLE `widget`
  MODIFY `widget_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Widget Id';
--
-- AUTO_INCREMENT для таблиці `widget_instance`
--
ALTER TABLE `widget_instance`
  MODIFY `instance_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Instance Id', AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT для таблиці `widget_instance_page`
--
ALTER TABLE `widget_instance_page`
  MODIFY `page_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Page Id', AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT для таблиці `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `wishlist_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Wishlist ID';
--
-- AUTO_INCREMENT для таблиці `wishlist_item`
--
ALTER TABLE `wishlist_item`
  MODIFY `wishlist_item_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Wishlist item ID';
--
-- AUTO_INCREMENT для таблиці `wishlist_item_option`
--
ALTER TABLE `wishlist_item_option`
  MODIFY `option_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Option Id';
--
-- Обмеження зовнішнього ключа збережених таблиць
--

--
-- Обмеження зовнішнього ключа таблиці `review_entity_summary`
--
ALTER TABLE `review_entity_summary`
  ADD CONSTRAINT `REVIEW_ENTITY_SUMMARY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `review_store`
--
ALTER TABLE `review_store`
  ADD CONSTRAINT `REVIEW_STORE_REVIEW_ID_REVIEW_REVIEW_ID` FOREIGN KEY (`review_id`) REFERENCES `review` (`review_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `REVIEW_STORE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `salesrule_coupon`
--
ALTER TABLE `salesrule_coupon`
  ADD CONSTRAINT `SALESRULE_COUPON_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `salesrule_coupon_aggregated`
--
ALTER TABLE `salesrule_coupon_aggregated`
  ADD CONSTRAINT `SALESRULE_COUPON_AGGREGATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `salesrule_coupon_aggregated_order`
--
ALTER TABLE `salesrule_coupon_aggregated_order`
  ADD CONSTRAINT `SALESRULE_COUPON_AGGREGATED_ORDER_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `salesrule_coupon_aggregated_updated`
--
ALTER TABLE `salesrule_coupon_aggregated_updated`
  ADD CONSTRAINT `SALESRULE_COUPON_AGGREGATED_UPDATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `salesrule_coupon_usage`
--
ALTER TABLE `salesrule_coupon_usage`
  ADD CONSTRAINT `SALESRULE_COUPON_USAGE_COUPON_ID_SALESRULE_COUPON_COUPON_ID` FOREIGN KEY (`coupon_id`) REFERENCES `salesrule_coupon` (`coupon_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALESRULE_COUPON_USAGE_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `salesrule_customer`
--
ALTER TABLE `salesrule_customer`
  ADD CONSTRAINT `SALESRULE_CUSTOMER_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALESRULE_CUSTOMER_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `salesrule_customer_group`
--
ALTER TABLE `salesrule_customer_group`
  ADD CONSTRAINT `SALESRULE_CSTR_GROUP_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALESRULE_CUSTOMER_GROUP_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `salesrule_label`
--
ALTER TABLE `salesrule_label`
  ADD CONSTRAINT `SALESRULE_LABEL_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALESRULE_LABEL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `salesrule_product_attribute`
--
ALTER TABLE `salesrule_product_attribute`
  ADD CONSTRAINT `SALESRULE_PRD_ATTR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALESRULE_PRD_ATTR_CSTR_GROUP_ID_CSTR_GROUP_CSTR_GROUP_ID` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALESRULE_PRODUCT_ATTRIBUTE_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALESRULE_PRODUCT_ATTRIBUTE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `salesrule_website`
--
ALTER TABLE `salesrule_website`
  ADD CONSTRAINT `SALESRULE_WEBSITE_RULE_ID_SALESRULE_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `salesrule` (`rule_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALESRULE_WEBSITE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `sales_bestsellers_aggregated_daily`
--
ALTER TABLE `sales_bestsellers_aggregated_daily`
  ADD CONSTRAINT `SALES_BESTSELLERS_AGGREGATED_DAILY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `sales_bestsellers_aggregated_monthly`
--
ALTER TABLE `sales_bestsellers_aggregated_monthly`
  ADD CONSTRAINT `SALES_BESTSELLERS_AGGREGATED_MONTHLY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `sales_bestsellers_aggregated_yearly`
--
ALTER TABLE `sales_bestsellers_aggregated_yearly`
  ADD CONSTRAINT `SALES_BESTSELLERS_AGGREGATED_YEARLY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `sales_creditmemo`
--
ALTER TABLE `sales_creditmemo`
  ADD CONSTRAINT `SALES_CREDITMEMO_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALES_CREDITMEMO_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

--
-- Обмеження зовнішнього ключа таблиці `sales_creditmemo_comment`
--
ALTER TABLE `sales_creditmemo_comment`
  ADD CONSTRAINT `SALES_CREDITMEMO_COMMENT_PARENT_ID_SALES_CREDITMEMO_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_creditmemo` (`entity_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `sales_creditmemo_item`
--
ALTER TABLE `sales_creditmemo_item`
  ADD CONSTRAINT `SALES_CREDITMEMO_ITEM_PARENT_ID_SALES_CREDITMEMO_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_creditmemo` (`entity_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `sales_invoice`
--
ALTER TABLE `sales_invoice`
  ADD CONSTRAINT `SALES_INVOICE_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALES_INVOICE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

--
-- Обмеження зовнішнього ключа таблиці `sales_invoiced_aggregated`
--
ALTER TABLE `sales_invoiced_aggregated`
  ADD CONSTRAINT `SALES_INVOICED_AGGREGATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

--
-- Обмеження зовнішнього ключа таблиці `sales_invoiced_aggregated_order`
--
ALTER TABLE `sales_invoiced_aggregated_order`
  ADD CONSTRAINT `SALES_INVOICED_AGGREGATED_ORDER_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

--
-- Обмеження зовнішнього ключа таблиці `sales_invoice_comment`
--
ALTER TABLE `sales_invoice_comment`
  ADD CONSTRAINT `SALES_INVOICE_COMMENT_PARENT_ID_SALES_INVOICE_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_invoice` (`entity_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `sales_invoice_item`
--
ALTER TABLE `sales_invoice_item`
  ADD CONSTRAINT `SALES_INVOICE_ITEM_PARENT_ID_SALES_INVOICE_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_invoice` (`entity_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `sales_order`
--
ALTER TABLE `sales_order`
  ADD CONSTRAINT `SALES_ORDER_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE SET NULL,
  ADD CONSTRAINT `SALES_ORDER_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

--
-- Обмеження зовнішнього ключа таблиці `sales_order_address`
--
ALTER TABLE `sales_order_address`
  ADD CONSTRAINT `SALES_ORDER_ADDRESS_PARENT_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `sales_order_aggregated_created`
--
ALTER TABLE `sales_order_aggregated_created`
  ADD CONSTRAINT `SALES_ORDER_AGGREGATED_CREATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

--
-- Обмеження зовнішнього ключа таблиці `sales_order_aggregated_updated`
--
ALTER TABLE `sales_order_aggregated_updated`
  ADD CONSTRAINT `SALES_ORDER_AGGREGATED_UPDATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

--
-- Обмеження зовнішнього ключа таблиці `sales_order_item`
--
ALTER TABLE `sales_order_item`
  ADD CONSTRAINT `SALES_ORDER_ITEM_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALES_ORDER_ITEM_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

--
-- Обмеження зовнішнього ключа таблиці `sales_order_payment`
--
ALTER TABLE `sales_order_payment`
  ADD CONSTRAINT `SALES_ORDER_PAYMENT_PARENT_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `sales_order_status_history`
--
ALTER TABLE `sales_order_status_history`
  ADD CONSTRAINT `SALES_ORDER_STATUS_HISTORY_PARENT_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `sales_order_status_label`
--
ALTER TABLE `sales_order_status_label`
  ADD CONSTRAINT `SALES_ORDER_STATUS_LABEL_STATUS_SALES_ORDER_STATUS_STATUS` FOREIGN KEY (`status`) REFERENCES `sales_order_status` (`status`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALES_ORDER_STATUS_LABEL_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `sales_order_status_state`
--
ALTER TABLE `sales_order_status_state`
  ADD CONSTRAINT `SALES_ORDER_STATUS_STATE_STATUS_SALES_ORDER_STATUS_STATUS` FOREIGN KEY (`status`) REFERENCES `sales_order_status` (`status`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `sales_order_tax_item`
--
ALTER TABLE `sales_order_tax_item`
  ADD CONSTRAINT `SALES_ORDER_TAX_ITEM_ASSOCIATED_ITEM_ID_SALES_ORDER_ITEM_ITEM_ID` FOREIGN KEY (`associated_item_id`) REFERENCES `sales_order_item` (`item_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALES_ORDER_TAX_ITEM_ITEM_ID_SALES_ORDER_ITEM_ITEM_ID` FOREIGN KEY (`item_id`) REFERENCES `sales_order_item` (`item_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALES_ORDER_TAX_ITEM_TAX_ID_SALES_ORDER_TAX_TAX_ID` FOREIGN KEY (`tax_id`) REFERENCES `sales_order_tax` (`tax_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `sales_payment_transaction`
--
ALTER TABLE `sales_payment_transaction`
  ADD CONSTRAINT `FK_B99FF1A06402D725EBDB0F3A7ECD47A2` FOREIGN KEY (`parent_id`) REFERENCES `sales_payment_transaction` (`transaction_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALES_PAYMENT_TRANSACTION_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALES_PAYMENT_TRANSACTION_PAYMENT_ID_SALES_ORDER_PAYMENT_ENTT_ID` FOREIGN KEY (`payment_id`) REFERENCES `sales_order_payment` (`entity_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `sales_refunded_aggregated`
--
ALTER TABLE `sales_refunded_aggregated`
  ADD CONSTRAINT `SALES_REFUNDED_AGGREGATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

--
-- Обмеження зовнішнього ключа таблиці `sales_refunded_aggregated_order`
--
ALTER TABLE `sales_refunded_aggregated_order`
  ADD CONSTRAINT `SALES_REFUNDED_AGGREGATED_ORDER_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

--
-- Обмеження зовнішнього ключа таблиці `sales_sequence_profile`
--
ALTER TABLE `sales_sequence_profile`
  ADD CONSTRAINT `SALES_SEQUENCE_PROFILE_META_ID_SALES_SEQUENCE_META_META_ID` FOREIGN KEY (`meta_id`) REFERENCES `sales_sequence_meta` (`meta_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `sales_shipment`
--
ALTER TABLE `sales_shipment`
  ADD CONSTRAINT `SALES_SHIPMENT_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SALES_SHIPMENT_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

--
-- Обмеження зовнішнього ключа таблиці `sales_shipment_comment`
--
ALTER TABLE `sales_shipment_comment`
  ADD CONSTRAINT `SALES_SHIPMENT_COMMENT_PARENT_ID_SALES_SHIPMENT_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_shipment` (`entity_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `sales_shipment_item`
--
ALTER TABLE `sales_shipment_item`
  ADD CONSTRAINT `SALES_SHIPMENT_ITEM_PARENT_ID_SALES_SHIPMENT_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_shipment` (`entity_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `sales_shipment_track`
--
ALTER TABLE `sales_shipment_track`
  ADD CONSTRAINT `SALES_SHIPMENT_TRACK_PARENT_ID_SALES_SHIPMENT_ENTITY_ID` FOREIGN KEY (`parent_id`) REFERENCES `sales_shipment` (`entity_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `sales_shipping_aggregated`
--
ALTER TABLE `sales_shipping_aggregated`
  ADD CONSTRAINT `SALES_SHIPPING_AGGREGATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

--
-- Обмеження зовнішнього ключа таблиці `sales_shipping_aggregated_order`
--
ALTER TABLE `sales_shipping_aggregated_order`
  ADD CONSTRAINT `SALES_SHIPPING_AGGREGATED_ORDER_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL;

--
-- Обмеження зовнішнього ключа таблиці `search_query`
--
ALTER TABLE `search_query`
  ADD CONSTRAINT `SEARCH_QUERY_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `search_synonyms`
--
ALTER TABLE `search_synonyms`
  ADD CONSTRAINT `SEARCH_SYNONYMS_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `SEARCH_SYNONYMS_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `signifyd_case`
--
ALTER TABLE `signifyd_case`
  ADD CONSTRAINT `SIGNIFYD_CASE_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE SET NULL;

--
-- Обмеження зовнішнього ключа таблиці `sitemap`
--
ALTER TABLE `sitemap`
  ADD CONSTRAINT `SITEMAP_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `store`
--
ALTER TABLE `store`
  ADD CONSTRAINT `STORE_GROUP_ID_STORE_GROUP_GROUP_ID` FOREIGN KEY (`group_id`) REFERENCES `store_group` (`group_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `STORE_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `store_group`
--
ALTER TABLE `store_group`
  ADD CONSTRAINT `STORE_GROUP_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `tax_calculation`
--
ALTER TABLE `tax_calculation`
  ADD CONSTRAINT `TAX_CALCULATION_CUSTOMER_TAX_CLASS_ID_TAX_CLASS_CLASS_ID` FOREIGN KEY (`customer_tax_class_id`) REFERENCES `tax_class` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `TAX_CALCULATION_PRODUCT_TAX_CLASS_ID_TAX_CLASS_CLASS_ID` FOREIGN KEY (`product_tax_class_id`) REFERENCES `tax_class` (`class_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `TAX_CALC_TAX_CALC_RATE_ID_TAX_CALC_RATE_TAX_CALC_RATE_ID` FOREIGN KEY (`tax_calculation_rate_id`) REFERENCES `tax_calculation_rate` (`tax_calculation_rate_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `TAX_CALC_TAX_CALC_RULE_ID_TAX_CALC_RULE_TAX_CALC_RULE_ID` FOREIGN KEY (`tax_calculation_rule_id`) REFERENCES `tax_calculation_rule` (`tax_calculation_rule_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `tax_calculation_rate_title`
--
ALTER TABLE `tax_calculation_rate_title`
  ADD CONSTRAINT `FK_37FB965F786AD5897BB3AE90470C42AB` FOREIGN KEY (`tax_calculation_rate_id`) REFERENCES `tax_calculation_rate` (`tax_calculation_rate_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `TAX_CALCULATION_RATE_TITLE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `tax_order_aggregated_created`
--
ALTER TABLE `tax_order_aggregated_created`
  ADD CONSTRAINT `TAX_ORDER_AGGREGATED_CREATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `tax_order_aggregated_updated`
--
ALTER TABLE `tax_order_aggregated_updated`
  ADD CONSTRAINT `TAX_ORDER_AGGREGATED_UPDATED_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `temando_checkout_address`
--
ALTER TABLE `temando_checkout_address`
  ADD CONSTRAINT `TEMANDO_CHKT_ADDR_SHPP_ADDR_ID_QUOTE_ADDR_ADDR_ID` FOREIGN KEY (`shipping_address_id`) REFERENCES `quote_address` (`address_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `temando_collection_point_search`
--
ALTER TABLE `temando_collection_point_search`
  ADD CONSTRAINT `TEMANDO_COLLECTION_POINT_SRCH_COUNTRY_ID_DIR_COUNTRY_COUNTRY_ID` FOREIGN KEY (`country_id`) REFERENCES `directory_country` (`country_id`) ON DELETE NO ACTION,
  ADD CONSTRAINT `TEMANDO_COLLECTION_POINT_SRCH_SHPP_ADDR_ID_QUOTE_ADDR_ADDR_ID` FOREIGN KEY (`shipping_address_id`) REFERENCES `quote_address` (`address_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `temando_order`
--
ALTER TABLE `temando_order`
  ADD CONSTRAINT `TEMANDO_ORDER_ORDER_ID_SALES_ORDER_ENTITY_ID` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`entity_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `temando_order_collection_point`
--
ALTER TABLE `temando_order_collection_point`
  ADD CONSTRAINT `FK_EEB9F0F035B969ECA24160257CFC6F6B` FOREIGN KEY (`recipient_address_id`) REFERENCES `sales_order_address` (`entity_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `temando_quote_collection_point`
--
ALTER TABLE `temando_quote_collection_point`
  ADD CONSTRAINT `FK_4ABED96BBA0BAF57A5EC10E47B5A0F12` FOREIGN KEY (`recipient_address_id`) REFERENCES `temando_collection_point_search` (`shipping_address_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `temando_shipment`
--
ALTER TABLE `temando_shipment`
  ADD CONSTRAINT `TEMANDO_SHIPMENT_SHIPMENT_ID_SALES_SHIPMENT_ENTITY_ID` FOREIGN KEY (`shipment_id`) REFERENCES `sales_shipment` (`entity_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `theme_file`
--
ALTER TABLE `theme_file`
  ADD CONSTRAINT `THEME_FILE_THEME_ID_THEME_THEME_ID` FOREIGN KEY (`theme_id`) REFERENCES `theme` (`theme_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `translation`
--
ALTER TABLE `translation`
  ADD CONSTRAINT `TRANSLATION_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `trive_revo_products`
--
ALTER TABLE `trive_revo_products`
  ADD CONSTRAINT `TRIVE_REVO_PRODUCTS_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `TRIVE_REVO_PRODUCTS_SLIDER_ID_TRIVE_REVO_SLIDER_ID` FOREIGN KEY (`slider_id`) REFERENCES `trive_revo` (`slider_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `trive_revo_stores`
--
ALTER TABLE `trive_revo_stores`
  ADD CONSTRAINT `TRIVE_REVO_STORES_SLIDER_ID_TRIVE_REVO_SLIDER_ID` FOREIGN KEY (`slider_id`) REFERENCES `trive_revo` (`slider_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `TRIVE_REVO_STORES_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `ui_bookmark`
--
ALTER TABLE `ui_bookmark`
  ADD CONSTRAINT `UI_BOOKMARK_USER_ID_ADMIN_USER_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `admin_user` (`user_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `variable_value`
--
ALTER TABLE `variable_value`
  ADD CONSTRAINT `VARIABLE_VALUE_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `VARIABLE_VALUE_VARIABLE_ID_VARIABLE_VARIABLE_ID` FOREIGN KEY (`variable_id`) REFERENCES `variable` (`variable_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `vault_payment_token`
--
ALTER TABLE `vault_payment_token`
  ADD CONSTRAINT `VAULT_PAYMENT_TOKEN_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `vault_payment_token_order_payment_link`
--
ALTER TABLE `vault_payment_token_order_payment_link`
  ADD CONSTRAINT `FK_4ED894655446D385894580BECA993862` FOREIGN KEY (`payment_token_id`) REFERENCES `vault_payment_token` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_CF37B9D854256534BE23C818F6291CA2` FOREIGN KEY (`order_payment_id`) REFERENCES `sales_order_payment` (`entity_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `weee_tax`
--
ALTER TABLE `weee_tax`
  ADD CONSTRAINT `WEEE_TAX_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `WEEE_TAX_COUNTRY_DIRECTORY_COUNTRY_COUNTRY_ID` FOREIGN KEY (`country`) REFERENCES `directory_country` (`country_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `WEEE_TAX_ENTITY_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`entity_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `WEEE_TAX_WEBSITE_ID_STORE_WEBSITE_WEBSITE_ID` FOREIGN KEY (`website_id`) REFERENCES `store_website` (`website_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `widget_instance`
--
ALTER TABLE `widget_instance`
  ADD CONSTRAINT `WIDGET_INSTANCE_THEME_ID_THEME_THEME_ID` FOREIGN KEY (`theme_id`) REFERENCES `theme` (`theme_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `widget_instance_page`
--
ALTER TABLE `widget_instance_page`
  ADD CONSTRAINT `WIDGET_INSTANCE_PAGE_INSTANCE_ID_WIDGET_INSTANCE_INSTANCE_ID` FOREIGN KEY (`instance_id`) REFERENCES `widget_instance` (`instance_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `widget_instance_page_layout`
--
ALTER TABLE `widget_instance_page_layout`
  ADD CONSTRAINT `WIDGET_INSTANCE_PAGE_LAYOUT_PAGE_ID_WIDGET_INSTANCE_PAGE_PAGE_ID` FOREIGN KEY (`page_id`) REFERENCES `widget_instance_page` (`page_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `WIDGET_INSTANCE_PAGE_LYT_LYT_UPDATE_ID_LYT_UPDATE_LYT_UPDATE_ID` FOREIGN KEY (`layout_update_id`) REFERENCES `layout_update` (`layout_update_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `wishlist`
--
ALTER TABLE `wishlist`
  ADD CONSTRAINT `WISHLIST_CUSTOMER_ID_CUSTOMER_ENTITY_ENTITY_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `wishlist_item`
--
ALTER TABLE `wishlist_item`
  ADD CONSTRAINT `WISHLIST_ITEM_PRODUCT_ID_CATALOG_PRODUCT_ENTITY_ENTITY_ID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `WISHLIST_ITEM_STORE_ID_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE SET NULL,
  ADD CONSTRAINT `WISHLIST_ITEM_WISHLIST_ID_WISHLIST_WISHLIST_ID` FOREIGN KEY (`wishlist_id`) REFERENCES `wishlist` (`wishlist_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `wishlist_item_option`
--
ALTER TABLE `wishlist_item_option`
  ADD CONSTRAINT `FK_A014B30B04B72DD0EAB3EECD779728D6` FOREIGN KEY (`wishlist_item_id`) REFERENCES `wishlist_item` (`wishlist_item_id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
