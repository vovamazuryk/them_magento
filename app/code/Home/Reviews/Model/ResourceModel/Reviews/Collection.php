<?php

namespace Home\Reviews\Model\ResourceModel\Reviews;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Framework\Search\AggregationInterface;
use Magento\Framework\Api\Search\SearchResultInterface;
use Magento\Framework\View\Element\UiComponent\DataProvider\Document;
use Magento\Framework\Api\SearchCriteriaInterface;
use \Home\Reviews\Model\ResourceModel\Reviews;

class Collection extends AbstractCollection //implements SearchResultInterface
{
    /**
     * Initialize resource collection
     *
     * @return void
     */
    protected $aggregations;
    protected $_idFieldName = 'id';

    public function _construct()
    {
        $this->_init('Home\Reviews\Model\Reviews', 'Home\Reviews\Model\ResourceModel\Reviews');
       // $this->_init(Document::class, Reviews::class);
    }



}
