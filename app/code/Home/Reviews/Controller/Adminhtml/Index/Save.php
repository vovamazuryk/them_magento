<?php
/**
 * Created by Volodymyr Mazuryk.
 * User: vovan
 * Date: 05.11.18
 * Time: 14:17
 */

namespace Home\Reviews\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;


class Save extends Action
{
    public function execute()
    {

        $data = $this->getRequest()->getParams();
        if ($data) {
            $model = $this->_objectManager->create('Home\Reviews\Model\Reviews');

            $id = $this->getRequest()->getParam('id');
            if ($id) {
                $model->load($id);
            }

            $model->setData($data);

            try {
                $model->save();
                $this->messageManager->addSuccess(__('The Comment Has been Saved.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                $this->_redirect('*/*/');
                return;
            } catch (\Magento\Framework\Model\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the banner.'));
            }
            return;
        }
        $this->_redirect('*/*/');
    }
}