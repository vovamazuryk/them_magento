<?php
/**
 * Created by Volodymyr Mazuryk.
 * User: vovan
 * Date: 19.11.18
 * Time: 8:36
 */

namespace Home\Reviews\Controller\Adminhtml\Index;

use Magento\Framework\Controller\ResultFactory;

class MassStatus extends \Magento\Backend\App\Action
{
    protected $_filter;

    protected $_collectionFactory;

    public function __construct(
        \Magento\Ui\Component\MassAction\Filter $filter,
        \Home\Reviews\Model\ResourceModel\Reviews\CollectionFactory $collectionFactory,
        \Magento\Backend\App\Action\Context $context
    )
    {
        $this->_filter = $filter;
        $this->_collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $collection = $this->_filter->getCollection($this->_collectionFactory->create());
        $collectionSize = $collection->getSize();
        $status = (int) $this->getRequest()->getParam('status');

        foreach ($collection as $item) {
            if ($status == 1) {
                $item->setIsActive(true);
                $item->save();
            }
            if ($status == 2) {
                $item->setIsActive(false);
                $item->save();
            }
        }

        $this->messageManager->addSuccess(__('A total of %1 record(s) have been update.', $collection->getSize()));

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}