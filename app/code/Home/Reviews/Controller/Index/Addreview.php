<?php

namespace Home\Reviews\Controller\Index;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Mail\Template\TransportBuilder;

class Addreview extends \Magento\Framework\App\Action\Action
{

    const XML_PATH_EMAIL_RECIPIENT = 'reviews/email/send_email';

    protected $_pageFactory;
    protected $_reviewsFactory;
    protected $_scopeConfig;
    protected $_transportBuilder;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Home\Reviews\Model\ReviewsFactory $reviewsFactory,
        ScopeConfigInterface $scopeConfig,
        TransportBuilder $transportBuilder
    )
    {
        $this->_pageFactory = $pageFactory;
        $this->_reviewsFactory = $reviewsFactory;
        $this->_scopeConfig = $scopeConfig;
        $this->_transportBuilder=$transportBuilder;
        return parent::__construct($context);
    }

    public function execute()
    {

        $post = (array) $this->getRequest()->getPost();

        if (!empty($post)) {
            $userid=$post['userid'];
            $username=$post['username'];
            $title=$post['title'];
            $comment=$post['comment'];

            $model = $this->_reviewsFactory->create();
            $model->addData([
                "userid" => $userid,
                "username" => $username,
                "title" => $title,
                "comment" => $comment
            ]);
            $saveData = $model->save();
            if($saveData){
                //$this->messageManager->addSuccessMessage('Review add and will be visible after moderate !!!');
            }

            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setUrl('/reviews/index');

            //return $resultRedirect;
             $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);
             $result->setData('Review add and will be visible after moderate !!!');
             $this->sendAdminEmail();
             return $result;
        }

        return $this->_pageFactory->create();
    }

    public function sendAdminEmail() {

        $email = $this->_scopeConfig->getValue('trans_email/ident_support/email',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        //$adminname = $this->_scopeConfig->getValue('trans_email/ident_support/name',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        $post = $this->getRequest()->getPostValue();


            $postObject = new \Magento\Framework\DataObject();
            $postObject->setData($post);


            $sender = [
                'name' => $post['username'],
                'email' => $post['useremail'],
            ];

            $transport = $this->_transportBuilder
                ->setTemplateIdentifier('send_admin_email_template')
                ->setTemplateOptions(['area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID])
                ->setTemplateVars(['data' => $postObject])
                ->setFrom($sender)
                ->addTo([trim($email)])
                ->getTransport();
            $transport->sendMessage();

    }
}